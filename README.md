Overview
========

Multi-GPU iterative solver code for an elliptic model equation arising in atmospheric modelling in flat domains. CUDA-C implementation works across multiple GPUs and uses the GCL library [1] for halo-exchanges.
Both a preconditioned CG solver and a multigrid solver based on the tensor-product multigrid idea in [2] is implemented.

Usage
=====

To compile the code, modify the Makefile (and Makefile.options) in the Source code directory. For detailled instructions on compilation and installation, see the README.txt file in the source directory.
The subdirectory 'Jobsubmission' contains some utility scripts for submitting jobs on parallel machines.

Eike Mueller, Bath (UK), May 2014

[This code was developed with contributions by Eero Vainikko (University of Tartu) and Sinan Shi (EPCC, Edinburgh)]

Copyright and Licensing statement
=================================

(c) The copyright relating to this work is owned jointly by the Crown, Met Office and NERC [2014]. However, it has been created with the help of the GungHo Consortium, whose members are identified at https://puma.nerc.ac.uk/trac/GungHo/wiki
.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License, version 3.0, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License, version 3.0, for more details.

You should have received a copy of the GNU Lesser General Public License along with this program (see files COPYING and COPYING.LESSER in the main directory).  If not, see <http://www.gnu.org/licenses/>.

The EllipticSolverGPU code uses (i.e. links to and includes header files from) the Generic Communication Library (GCL). The GCL has been developed by  Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING CENTRE (CSCS) and it is distributed under the Copyright statement in the file COPYING.GCL.

[1] Mauro Bianco: "An Interface for Halo Exchange Pattern." http://www.prace-project.eu/IMG/pdf/wp86.pdf

[2] S. Boerm and R. Hiptmair: "Analysis of tensor product multigrid", Numer. Algorithms, 26/3 (2001), pp. 219-234,
