/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "kernel_transpose.hh"

/* ********************************************************************* *
 * Device function for transposing a field [ LINIDX_Z -> LINIDX_X ]
 * (see Mark Harris' blogpost at
 * devblogs.nvidia.com/parallelforall/efficient-matrix-transpose-cuda-cc/)
 * ********************************************************************* */
__global__ void gpu_transposeZtoX(int NX,
                                  int NY,
                                  real* phi,
                                  real* phiT) {
  __shared__ real tmp[TILE_SIZE][TILE_SIZE];
  int ix = threadIdx.y + TILE_SIZE * blockIdx.y;
  int iy = threadIdx.z + blockIdx.z * blockDim.z;
  int iz = threadIdx.x + TILE_SIZE * blockIdx.x;
  for (int j=0;j<TILE_SIZE;j+=BLOCK_ROWS) {
    tmp[threadIdx.y+j][threadIdx.x] = phi[LINIDX_Z(NX,NY,ix+j+1,iy+1,iz)];
  }
  __syncthreads();
  ix = threadIdx.x + TILE_SIZE * blockIdx.y;
  iz = threadIdx.y + TILE_SIZE * blockIdx.x;

  for (int j=0;j<TILE_SIZE;j+=BLOCK_ROWS) {
    phiT[LINIDX_X(NX,NY,ix+1,iy+1,iz+j)] = tmp[threadIdx.x][threadIdx.y+j];
  }
}

/* ********************************************************************* *
 * Device function for transposing a field [ LINIDX_Z -> LINIDX_X ]
 * (see Mark Harris' blogpost at
 * devblogs.nvidia.com/parallelforall/efficient-matrix-transpose-cuda-cc/)
 * ********************************************************************* */
__global__ void gpu_transposeXtoZ(int NX,
                                  int NY,
                                  real* phi,
                                  real* phiT) {
  __shared__ real tmp[TILE_SIZE][TILE_SIZE];
  int ix = threadIdx.x + TILE_SIZE * blockIdx.x;
  int iy = threadIdx.z + blockIdx.z * blockDim.z;
  int iz = threadIdx.y + TILE_SIZE * blockIdx.y;
  for (int j=0;j<TILE_SIZE;j+=BLOCK_ROWS) {
    tmp[threadIdx.y+j][threadIdx.x] = phi[LINIDX_X(NX,NY,ix+1,iy+1,iz+j)];
  }
  ix = threadIdx.y + TILE_SIZE * blockIdx.x;
  iz = threadIdx.x + TILE_SIZE * blockIdx.y;
  __syncthreads();
  for (int j=0;j<TILE_SIZE;j+=BLOCK_ROWS) {
    phiT[LINIDX_Z(NX,NY,ix+j+1,iy+1,iz)] = tmp[threadIdx.x][threadIdx.y+j];
  }
}

/* ********************************************************************* *
 * Constructor
 * ********************************************************************* */
KernelTranspose::KernelTranspose(const int NX_,
                                 const int NY_) :
    NX(NX_), NY(NY_) {
  int pid;
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  if (NX % TILE_SIZE) {
    if (pid == 0) {
      std::cout << "ERROR initialising transpose kernel:" << std::endl;
      std::cout << "NX has to be a multiple of " << TILE_SIZE << std::endl;
    }
    exit(-1);
  }
  if (NZ % TILE_SIZE) {
    if (pid == 0) {
      std::cout << "ERROR initialising transpose kernel:" << std::endl;
      std::cout << "NZ has to be a multiple of " << TILE_SIZE << std::endl;
    }
    exit(-1);
  }
  dimBlock.x = TILE_SIZE;
  dimBlock.y = BLOCK_ROWS;
  dimBlock.z = 1;
  // Grid dimensions for Z -> X transpose
  dimGrid_ZtoX.x = NZ/TILE_SIZE;
  dimGrid_ZtoX.y = NX/TILE_SIZE;
  dimGrid_ZtoX.z = NY;
  // Grid dimensions for X -> Z transpose
  dimGrid_XtoZ.x = NX/TILE_SIZE;
  dimGrid_XtoZ.y = NZ/TILE_SIZE;
  dimGrid_XtoZ.z = NY;
}

/* ********************************************************************* *
 * Invoke kernel
 * ********************************************************************* */
void KernelTranspose::invoke(real *phi,
                             real *phiT,
                             TransDirType direction) {
  if (direction == ZtoX) {
    gpu_transposeZtoX<<<dimGrid_ZtoX,dimBlock>>>(NX,NY,phi,phiT);
  } else {
    gpu_transposeXtoZ<<<dimGrid_XtoZ,dimBlock>>>(NX,NY,phi,phiT);
  }
  cuda_checkError();
}
