/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "timer.hh"

/* ********************************************************************* *
 * Timer class implementation
 * ********************************************************************* */


/* ********************************************************************* *
 * print out warning message
 * ********************************************************************* */
void Timer::timerWarning(const std::string msg) {
  std::cout << "WARNING: Timer " << label << " : " << msg << std::endl;
}

/* ********************************************************************* *
 * Output operator for timer class
 * ********************************************************************* */
std::ostream& operator<<(std::ostream& out,
                         const Timer& timer) {
#ifdef USE_TIMERS
  out << std::left << std::setw(32) << timer.getLabel() << " : ";
  out << std::scientific << std::setprecision(3) << timer.tElapsed() << " s ";
  out << " " << std::right << std::setw(8) << timer.nCall();
  out << " " << std::scientific << std::setprecision(3) << timer.tCall() << " s";
#else
  out << "Code compiled without timer support. Recompile with -DUSE_TIMERS.";
#endif // USE_TIMERS
}
