/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<cuda.h>
#include<assert.h>
#include<mpi.h>
#include"definitions.hh"
#include"auxilliary.hh"
#include"apply_host.hh"
#include"kernel_apply.hh"
#include"prec_host.hh"
#include"pcg_host.hh"
#include"solver.hh"
#include"solver_cg.hh"
#include"solver_multigrid.hh"
#include"multi_gpu.hh"
#include"functions.hh"
#include"function_mapper.hh"
#include"cuda_errorcheck.hh"

/* ********************************************************************* *
 * Measure time for one halo exchange and network BW
 * ********************************************************************* */
void measure_bandwidth(int nx, int ny,
                       DomainLayout& threadlayout,
                       int pid) {
  const int NX=threadlayout.all.NX;
  const int NY=threadlayout.all.NY;
  int n_haloexchange=1000;
  real *dev_halo;
  const unsigned long NXYZ=(NX+2*OL_X)*(NY+2*OL_Y)*NZ;
  cuda_safeCall(cudaMalloc((void**)&dev_halo,NXYZ*sizeof(real)));
  MultiGPULauncher multi_gpu_launcher(nx,ny,threadlayout);
  MPI_Barrier(MPI_COMM_WORLD);
  double tStart = MPI_Wtime();
  for (int i=0;i<n_haloexchange;++i) {
    multi_gpu_launcher.halo_exchange(dev_halo);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  double tFinish = MPI_Wtime();
  if (pid == 0) {
    double t_exchange = (tFinish - tStart)/double(n_haloexchange);
    std::cout << " Time per halo exchange " << 1000.0*t_exchange << " ms";
    std::cout << std::endl;
    double bandwidth = 2.*HALOSIZE*(NX+NY)*NZ*sizeof(real)/t_exchange;
    std::cout << " Network bandwidth = " << 1.e-9*bandwidth << " GB/s";
    std::cout << std::endl;
    std::cout << std::endl;
  }
  cuda_safeCall(cudaFree(dev_halo));
}

/* ********************************************************************* *
 * SOLVER MAIN PROGRAM
 * ********************************************************************* *
 *
 * Solve the equation
 *   A.u = b
 * using the CUDA multiGPU solver. The equation can be solved both
 * for a RHS which is constructed from an exact solution u_{exact} as
 * b = A.u_{exact} or from a RHS that is non-zero only in part of the
 * domain. In addition, the GPU solution can be compared both to a
 * sequential CPU solution and to the exact solution.
 *
 * ********************************************************************* */
int main(int argc, char* argv[]){

  int NX,NY;

  real omega2;
  real lambda2;

  double t_start, t_finish;

  // Required residual reduction
  real resreduction = 1.0E-12;
  // Maximal number of iterations
  int maxiter=100;

  int i;
  int verbose=1;
  int Nlevel=0;
  int ncoarsesmooth=2;
  SolverType solver_type;

  // Measure MPI BW?
  bool measure_BW=true;

  // call host solvers and test GCL by comparing?
  bool compare_to_host=false;
  // compare to exact solution?
  bool compare_to_exact=true;
  // Save fields to disk?
  static const bool savefields=false;

  // MPI parameters
  int nprocs, pid, nx, ny, nxyp, xp, yp;

  // Counters
  int j,k;

  // Initialise CUDA, MPI and GCL
  device_binding();
  MPI_Init(&argc, &argv);
  GCL::GCL_Init(argc, argv);

  // Setup processor layout
  MPI_Comm CartComm;
  setup_processor_layout(&pid,&nprocs,&nxyp,&xp,&yp,&CartComm);

  // Print out information
  if (argc != 3) {
    if (pid == 0) {
      printf("Usage: %s <parameterfile> <domainfile>\n",argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (pid == 0) {
    printf("********************************************\n");
    printf("********************************************\n");
    printf("***       Parallel CUDA solver           ***\n");
    printf("********************************************\n");
    printf("********************************************\n");
    printf("\n");
    printf("Running on %d GPUs\n",nprocs);
    printf("\n");

    printf(" compiler flags\n");
    printf(" ==============\n");

#ifdef DOUBLE__PRECISION
    printf(" precision = DOUBLE\n");
#else
    printf(" precision = SINGLE\n");
#endif
    printf(" sizeof(real) = %d\n",sizeof(real));

#ifdef CARTESIAN_GEOMETRY
    printf(" geometry = CARTESIAN\n");
#else
    printf(" geometry = CUBED SPHERE\n");
#endif

#ifdef Z_GRADED
    printf(" z-graded = TRUE\n");
#else
    printf(" z-graded = FALSE\n");
#endif

#ifdef HAVE_BLAS
  #ifdef ATLAS_BLAS
    printf(" Using ATLAS BLAS.\n");
  #endif
  #ifdef GOTO_BLAS
    printf(" Using GOTO BLAS.\n");
  #endif
  #ifdef GOTO_BLAS2
    printf(" Using GOTO BLAS2.\n");
  #endif
  #ifdef OPEN_BLAS
    printf(" Using OPEN BLAS.\n");
  #endif
#else
    printf(" Using dummy BLAS implementation.\n");
#endif

#ifdef HAVE_LAPACK
    printf(" Using LAPACK\n");
#endif // HAVE_LAPACK
    printf("\n");
  }
  if (pid == 0) {
    printf(" MPI timer resolution = %8.4e\n",MPI_Wtick());
    printf("\n");
  }

  char filename_param[1024];
  char filename_domain[1024];
  strcpy(filename_param,argv[1]);
  strcpy(filename_domain,argv[2]);
  read_parameters(filename_param,
                  &solver_type,
                  &Nlevel,
                  &ncoarsesmooth,
                  &omega2,
                  &lambda2,
                  &resreduction,
                  &maxiter,
                  &verbose,
                  &compare_to_host,
                  &compare_to_exact);
  DomainLayout threadlayout(filename_domain);

  NX=threadlayout.all.NX;
  NY=threadlayout.all.NY;

  // Work out global problem size
  nx=NX*nxyp;
  ny=NY*nxyp;

  // Allocate memory
  const unsigned long nxyz = (nx+2*OL_X)*(ny+2*OL_Y)*NZ;
  const unsigned long NXYZ=(NX+2*OL_X)*(NY+2*OL_Y)*NZ;
  real *b,*b_global;
  real *u_exact;
  real *u_exact_global;
  real *u_host_global,*u_host,*u_device,*u_error;
  if (compare_to_host) {
    u_exact_global = (real*) malloc(nxyz*sizeof(real));
    b_global = (real*) malloc(nxyz*sizeof(real));
    u_host_global = (real*) malloc(nxyz*sizeof(real));
    u_host = (real*) malloc(NXYZ*sizeof(real));
    u_error = (real*) malloc(nxyz*sizeof(real));
  }
  if (compare_to_exact) {
    u_exact = (real*) malloc(NXYZ*sizeof(real));
  }
  u_device = (real*) malloc(NXYZ*sizeof(real));
  b = (real*) malloc(NXYZ*sizeof(real));

  // Construct RHS and exact solution, if necessary
  real sigma = 0.5;
  FunctionExact f_exact(sigma);
  FunctionIndicator f_ind;
  FunctionZero f_zero;
  FunctionMapper f_map(NX,NY);

  if (compare_to_host) {
    if (compare_to_exact) {
      // Calculate RHS = A.u_{exact} on CPU
      /* Set values of sequential vector (exact solution) */
      f_map.mapGlobal(f_exact,u_exact_global);
      // Calculate the RHS by applying the matrix to A
      VerticalDiscretisation vert_disc(omega2, lambda2);
      real* a_vert = vert_disc.get_a_vert();
      real* b_vert = vert_disc.get_b_vert();
      real* c_vert = vert_disc.get_c_vert();
      real* d_vert = vert_disc.get_d_vert();
      apply(nx,ny,
            a_vert,
            b_vert,
            c_vert,
            d_vert,
            u_exact_global,b_global);
    } else {
      f_map.mapGlobal(f_ind,b_global,true);
    }
  }

  if (measure_BW) {
    measure_bandwidth(nx,ny,threadlayout,pid);
  }

  if ( compare_to_exact ) {
    // Calculate RHS = A.u_{exact} on GPU
    f_map.mapLocal(f_exact,u_exact);
    real *dev_u,*dev_b;
    real* phiT = (real*) malloc(NXYZ*sizeof(real));
    real* phiZero = (real*) malloc(NXYZ*sizeof(real));
    for (unsigned int i=0;i<NXYZ;++i)
      phiZero[i] = 0.0;
    transposeZtoX(NX,NY,u_exact,phiT);
    cuda_safeCall(cudaMalloc((void**)&dev_u,NXYZ*sizeof(real)));
    cuda_safeCall(cudaMalloc((void**)&dev_b,NXYZ*sizeof(real)));
    cuda_safeCall(cudaMemcpy(dev_b,phiZero,NXYZ*sizeof(real),
                  cudaMemcpyHostToDevice));
    cuda_safeCall(cudaMemcpy(dev_u,phiT,NXYZ*sizeof(real),
                  cudaMemcpyHostToDevice));
    VerticalDiscretisation vert_disc(omega2, lambda2);
    MultiGPULauncher multi_gpu_launcher(nx,ny,threadlayout);
    multi_gpu_launcher.halo_exchange(dev_u);
    ApplyKernel kernel_apply(&vert_disc,dev_u,dev_b);
    multi_gpu_launcher.invoke(kernel_apply);
    cuda_safeCall(cudaMemcpy(phiT,dev_b,NXYZ*sizeof(real),
                  cudaMemcpyDeviceToHost));
    transposeXtoZ(NX,NY,phiT,b);
    cuda_safeCall(cudaFree(dev_u));
    cuda_safeCall(cudaFree(dev_b));
    free(phiT);
    free(phiZero);
  } else {
    // Just initialise RHS to the indicator function
    f_map.mapLocal(f_ind,b,true);
  }

  // Set initial solution to zero
  f_map.mapLocal(f_zero,u_device,true);

  if (compare_to_host) {
    /*Apply on host*/
    f_map.mapLocal(f_zero,u_host_global);
    pcg_solve(nx,ny,
              omega2,lambda2,resreduction,maxiter,verbose,
              b_global,u_host_global);
    for(i=1-OL_X;i<=NX+OL_X;i++) {
      for(j=1-OL_Y;j<=NY+OL_Y;j++) {
        for(k=0;k<NZ;k++) {
          u_host[LINIDX_Z(NX,NY,i,j,k)]
            = u_host_global[LINIDX_Z(nx,ny,NX*xp+i,NY*yp+j,k)];
        }
      }
    }
  }

  Solver *solver;

  if (solver_type == SolverTypeCG) {
    /* Initialise solver */
    solver = new SolverCG(&threadlayout,
                          omega2,
                          lambda2,
                          maxiter,
                          resreduction,
                          verbose);
  } else if (solver_type == SolverTypeMultigrid) {
    bool testConvergence = true;
    real rho_relax = 2./3.;
    solver = new SolverMultigrid(&threadlayout,
                                 Nlevel,
                                 ncoarsesmooth,
                                 testConvergence,
                                 rho_relax,
                                 omega2,
                                 lambda2,
                                 maxiter,
                                 resreduction,
                                 verbose);
  }
  /* Solve on device*/
  MPI_Barrier(MPI_COMM_WORLD);
  t_start = MPI_Wtime();
  solver->solve(b,u_device);
  MPI_Barrier(MPI_COMM_WORLD);
  t_finish = MPI_Wtime();

  solver->showResults();

  delete solver;

  if (pid == 0) {
    printf("  Solve time              = %6.3e ms\n",(t_finish-t_start)*1000.);
    printf("\n");
  }
  real norm_diff;
  real norm_1;
  real norm_2;
  real max_rel_diff;
  if (compare_to_host) {
    if (compare_to_exact) {
      dnorm_loc_glo(NX, NY,
                    nx, ny,
                    xp, yp,
                    u_host,
                    u_exact_global,
                    norm_diff,
                    norm_1,
                    norm_2,
                    max_rel_diff);
      if (pid == 0) {
        printf("Comparison host / exact\n");
        printf (" ||y_[C]||              = %12.8e\n",norm_1);
        printf (" ||y_exact||            = %12.8e\n",norm_2);
        printf (" ||y_[C]-y_exact||      = %12.8e\n",norm_diff);
        printf (" maximal rel. diff.     = %12.8e\n",max_rel_diff);
      }
    }
    dnorm_loc_glo(NX, NY,
                  nx, ny,
                  xp, yp,
                  u_device,
                  u_host_global,
                  norm_diff,
                  norm_1,
                  norm_2,
                  max_rel_diff);
    if (pid == 0) {
      printf("Comparison CUDAC / host\n");
      printf (" ||y_[CUDA]||           = %12.8e\n",norm_1);
      printf (" ||y_[C]||              = %12.8e\n",norm_2);
      printf (" ||y_[C]-y_[CUDAC]||    = %12.8e\n",norm_diff);
      printf (" maximal rel. diff.     = %12.8e\n",max_rel_diff);
    }
    if (savefields) {
      savetovtk(NX,NY,"solution_host",u_host,false);
      for (unsigned int i=0;i<NXYZ;++i) {
        u_host[i] -= u_exact[i];
      }
      savetovtk(NX,NY,"error_host",u_host,false);
    }
  }

  if (compare_to_exact) {
    dnorm_loc_loc(NX, NY,
                  u_device,
                  u_exact,
                  norm_diff,
                  norm_1,
                  norm_2,
                  max_rel_diff);
    if (pid == 0) {
      printf("Comparison CUDAC / exact\n");
      printf (" ||y_[CUDA]||           = %12.8e\n",norm_1);
      printf (" ||y_exact              = %12.8e\n",norm_2);
      printf (" ||y_exact-y_[CUDAC]||  = %12.8e\n",norm_diff);
      printf (" maximal rel. diff.     = %12.8e\n",max_rel_diff);
    }
  }

  if ( savefields ) {
    savetovtk(NX,NY,"solution_device",u_device,false);
    if (compare_to_exact) {
      for (unsigned int i=0;i<NXYZ;++i) {
        u_device[i] -= u_exact[i];
      }
      savetovtk(NX,NY,"error_device",u_device,false);
    }
  }

  if (compare_to_host) {
    free(u_host_global);
    free(u_error);
    free(u_exact_global);
    free(u_host);
    free(b_global);
  }
  if (compare_to_exact) {
    free(u_exact);
  }
  free(u_device);
  free(b);

  MPI_Finalize();

  return(0);
}
