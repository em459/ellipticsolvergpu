#include "math.h"
#include "definitions.hh"
#include "blas_wrapper.hh"

/* ************************************************************ *
 * Copy
 * ************************************************************ */
void dummyblas_copy(const int n,
                    const real* x,
                    const int incx,
                    real* y,
                    const int incy) {
  int i,ix,iy;
  for (ix=0,iy=0,i=0;i<n;++i,ix+=incx,iy+=incy) {
    y[iy] = x[ix];
  }
}

/* ************************************************************ *
 * axpy
 * ************************************************************ */
void dummyblas_axpy(const int n,
                    const real alpha,
                    const real* x,
                    const int incx,
                    real* y,
                    const int incy) {
  int i,ix,iy;
  for (ix=0,iy=0,i=0;i<n;++i,ix+=incx,iy+=incy) {
    y[iy] += alpha*x[ix];
  }
}

/* ************************************************************ *
 * scal
 * ************************************************************ */
void dummyblas_scal(const int n,
                    const real alpha,
                    real* x,
                    const int incx) {
  int i,ix;
  for (ix=0,i=0;i<n;++i,ix+=incx) {
    x[ix]*=alpha;
  }
}

/* ************************************************************ *
 * dot
 * ************************************************************ */
real dummyblas_dot(const int n,
                   real* x,
                   const int incx,
                   real* y,
                   const int incy) {
  int i,ix,iy;
  real tmp = 0.0;
  for (ix=0,iy=0,i=0;i<n;++i,ix+=incx,iy+=incy) {
    tmp += y[iy]*x[ix];
  }
  return tmp;
}

/* ************************************************************ *
 * L2 onrm
 * ************************************************************ */
real dummyblas_nrm2(const int n,
                    real* x,
                    const int incx) {
  int i,ix;
  real tmp = 0.0;
  for (ix=0,i=0;i<n;++i,ix+=incx) {
    tmp += x[ix]*x[ix];
  }
  return sqrt(tmp);
}
