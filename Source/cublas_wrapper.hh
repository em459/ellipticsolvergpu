/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef CUBLAS_WRAPPER_HH
#define CUBLAS_WRAPPER_HH CUBLAS_WRAPPER_HH
/* ************************************************************ *
 * If HAVE_BLAS is defined, this module ensures that, depending
 * on whether we use double precision or not, the correct
 * CBLAS routines are called. If HAVE_BLAS is not set, the
 * dummy implementation below is used instead of the system
 * BLAS implementation. This should only be used for testing.
 * ************************************************************ */


#include<cuda_runtime_api.h>
#include<cusparse_v2.h>
#include<cublas_v2.h>


  #ifdef DOUBLE__PRECISION
    #define CUBLAS_COPY cublasDcopy
    #define CUBLAS_AXPY cublasDaxpy
    #define CUBLAS_SCAL cublasDscal
    #define CUBLAS_DOT  cublasDdot
    #define CUBLAS_NRM2 cublasDnrm2
  #else // DOUBLE__PRECISION
    #define CUBLAS_COPY cublasScopy
    #define CUBLAS_AXPY cublasSaxpy
    #define CUBLAS_DOT  cublasSdot
    #define CUBLAS_SCAL cublasSscal
    #define CUBLAS_NRM2 cublasSnrm2
  #endif // DOUBLE__PRECISION




#endif // BLAS_WRAPPER_HH
