/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef KERNEL_HH
#define KERNEL_HH KERNEL_HH
#include <vector>
#include "definitions.hh"
#include "discretisation.hh"

/* ********************************************************************* *
 * Base class for kernel wrapper
 * ********************************************************************* */
class Kernel {
 public:
  // Constructor
  Kernel(const VerticalDiscretisation* vert_disc) :
   halo_fields(0) {
    a_vert = vert_disc->get_dev_a_vert();
    b_vert = vert_disc->get_dev_b_vert();
    c_vert = vert_disc->get_dev_c_vert();
    d_vert = vert_disc->get_dev_d_vert();
  }
  // Launch kernel on a particular subdomain
  void invoke(dim3 dimGrid,
              dim3 dimBlock,
              cudaStream_t stream,
              int NX,
              int NY,
              int nx,
              int ny,
              int xp,
              int yp,
              int offset_x,
              int offset_y) {}
  // Return vector with fields that require a halo exchange
  std::vector<real*> get_halofields() { return halo_fields; }
 protected:
  // Vertical discretisation
  real* a_vert;
  real* b_vert;
  real* c_vert;
  real* d_vert;
  // Pointer to fields that are halo-exchanged
  std::vector<real*> halo_fields;
};

#endif // KERNEL_HH
