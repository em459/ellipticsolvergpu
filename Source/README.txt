##############################
# Elliptic solver code
##############################

  Eike Mueller (e.mueller@bath.ac.uk)
  Department of Mathematical Sciences
  University of Bath
  Mar 2014

*** Summary ***

This code is the CUDA-C implementation of two iterative solvers for a 3-dimensional elliptic PDE on a regular grid for one panel of the cubed shere. Further details on the mathematical formulation and implementation can be found in the following two publications:

[1] Eike H. Müller, X. Guo, R. Scheichl, S. Shi:
"Matrix-free GPU implementation of a preconditioned conjugate gradient solver for anisotropic elliptic PDEs" Computation and Visualization in Science [special issue EMG 2012] (2013) http://arxiv.org/abs/1302.7193

[2] Eike H. Müller, Robert Scheichl, Benson Muite, Eero Vainikko:
"Petascale elliptic solvers for anisotropic PDEs on GPU clusters"
Submitted to Parallel Computing (2014) http://arxiv.org/abs/1402.3545

The code has been tested on several multi-GPU clusters (most importantly EMERALD and TITAN) on up to 16384 GPUs. As described in detail in the publications above, the code is memory bound, so its performance is mainly limited by the global memory bandwidth of the device, NOT by the raw FLOP rate of the GPUs. Numerical tests on EMERALD and TITAN have shown that the implementation can usually utilise at the order of 25%-50% of the peak global memory bandwidth.

For the multigrid solver a sizeable fraction (10%-20% of the solver time) is spent on copying fields between the host and the device. In addition, halo exchanges require inter-GPU communication so the code also exercises intra- and internode communication between GPU cards. As described below the timing results in the output are broken down such that the relevant bottlenecks can be identified for each individual run.

All code code has been implemented using double precision arithmetic.

The code is made available subject to the following copyright and license agreement:
#############################################################
(c) The copyright relating to this work is owned jointly
by the Crown, Met Office and NERC [2014]. However, it
has been created with the help of the GungHo Consortium,
whose members are identified at
https://puma.nerc.ac.uk/trac/GungHo/wiki

Main Developer: Eike Mueller, University of Bath

Contributors:
  Eero Vainikko (University of Tartu)
  Sinan Shi (EPCC Edinburgh)

EllipticSolverGPU is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

EllipticSolverGPU is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the
main directory).  If not, see <http://www.gnu.org/licenses/>.
#############################################################

Further distribution of the code (or parts of it) is prohibited.

This document contains instructions for installing and running the code. It also describes the relevant output and how it can be interpreted.

*** Installation ***

--- Prerequisites ---

To compile the code both a CUDA- and an MPI installation are required. The CUBLAS library needs to be installed as well.

Inter-GPU communications, in particular halo-exchanges, are implemented in the Generic Communication Library developed by Mauro Bianco at CSCS (Switzerland). Further details on the library can be found here in the following document:

[3] Mauro Bianco: "An interface for halo exchange pattern. http://www.prace-ri.eu/IMG/pdf/wp86.pdf" 2013.

The library can be obtained from the trunk of the subversion repository via

svn checkout svn://scm.hpcforge.org/scmrepos/svn/gcl/trunk

and is installed via the CMake build tools, see also the README file in the GCL main directory for further instructions. The GCL requires a headers-only installation of the Boost library (We have tested it with Boost version 1.53 and 1.55). GCL requires OpenMPI 1.7 (or higher) but might also work with different MPI libraries. Building the GCL library is straightforward. Set CMAKE_BUILD_TYPE=Release and make sure that the GCL_MPI and GCL_GPU flags are set to ON. If the library is build with GCL_ONLY=OFF, also a set of test programs in the subdirectory L2/test is built. On some machines it might also be necessary to set the flag USE_MPI_COMPILER=ON. It is probably worth testing some of these (see source code for command line parameters) to ensure that the library works correctly before proceeding with the installation of ther iterative solver code. As all halo exchanges are handled by the library, tuning the GCL installation might improve the performance of the solvers.

On systems which do not support device pointers in MPI calls, uncomment the line

//#define HOSTWORKAROUND

in L3/include/GCL.h. Note, however, that this will have a negative impact on performance as data will be copied explicitly between device and host before/after exchanging it via MPI.

--- Compilation ---

The iterative solver code is compiled with the Makefile in the source directory. The flags on a specific machine need to be adjusted and the correct flags have to be passed to properly link against the GCL library (to figure out the correct flags for the GCL library, compile the GCL verbosely and read of the corresponding flags at the compilation and linking stage). For examples of compiler flags see the relevant sections for the "aquila", "EMERALD" and "Titan" machines in the Makefile. Compilation might produce lots of warning messages about missing return statements from the GCL library, these can be ignored.

Compilation with 'make all' produces the two main executables

main_apply_prec-CUDAC.x [code for testing the SpMV operation and preconditioner (block-tridiagonal solver) in isolation, this is not needed for the tests described below]
main_solver-CUDAC.x [the main solver code]

To get optimal performance make sure that the correct compute capability is used for the available hardware.

*** Running the code ***

to run the code use

mpirun -n <nprocs> main_solver-CUDAC.x <parameterfile> <domainfile>

The code can only run on a square number of GPUs, here we provide input files for runs on 1, 4 and 16 GPUs. The horizontal domain sizes are 256x256 and 512x512 per GPU, and one thread is assigned to each horizontal grid cell.

To test the code, the input files should be used in the following combinations. They differ in the solver which is used (cg = preconditioned Conjugate Gradient or multigrid) and in the domain size.

1 GPU:
parameters_cg_256x256x128_1GPUs.in            domain_256x256x128.in
parameters_multigrid_256x256x128_1GPUs.in     domain_256x256x128.in
parameters_cg_512x512x128_1GPUs.in            domain_512x512x128.in
parameters_multigrid_512x512x128_1GPUs.in     domain_512x512x128.in

4 GPUs:
parameters_cg_256x256x128_4GPUs.in            domain_256x256x128.in
parameters_multigrid_256x256x128_4GPUs.in     domain_256x256x128.in
parameters_cg_512x512x128_4GPUs.in            domain_512x512x128.in
parameters_multigrid_512x512x128_4GPUs.in     domain_512x512x128.in

16 GPUs:
parameters_cg_256x256x128_16GPUs.in           domain_256x256x128.in
parameters_multigrid_256x256x128_16GPUs.in    domain_256x256x128.in
parameters_cg_512x512x128_16GPUs.in           domain_512x512x128.in
parameters_multigrid_512x512x128_16GPUs.in    domain_512x512x128.in

the parameters*.in files should not be changed. However, in the domain*.in files, which control the size of the domain, the number of threads per block can be adjusted by changing

BLOCKSIZE_NX_all      = 64
BLOCKSIZE_NY_all      = 2

This might improve the performance, in all our tests we found that 64 x 2 gave good results. All other lines in these files should not be touched.

*** Interpreting the results ***

The code writes all results to standard output, so when running the code through a jobscript output has to be redirected accordingly. Two sample output files are provided with this document.

In the following we look at some relevant sections of an output file:

(1) Correctness
The iterative solver algorithm continues until the residual has been reduced by at least five orders of magnitude. The initial residual, number of iterations and final residual reduction is printed out. The multigrid solver should converge in less than 10 iterations and the final residual reduction has to be less than 1.0E-5. The CG solver should converge in less than 60 iterations. If the solver completed successfully the line "Solver converged = YES" is printed out.

Here are two example outputs from a multigrid solve and a CG solve on the 512x512 grid on 4 GPUs:

[...]
 *** Multigrid solve **

  Initial residual = 6.830954e-07
  Solver converged in    7 iterations.
  Final residual reduction = 9.071e-06

  ----------------------------------------
  Solver converged        = YES
[...]

[...]
*** CG solve **

  Initial residual = 6.774821e-07
  Solver converged in   58 iterations.
  Final residual reduction = 8.367e-06

  ----------------------------------------
  Solver converged        = YES
[...]

(2) Performance measures

(3.A) - total solution time and memory copy time
Ultimately we are interested in the total solution time, so this is the most important number shich should be minimised and tells us something about the performance of the code on a particular machine. The time is printed out as follows:

[...]
  Solve time              = 3.255e+03 ms
[...]

The total time is broken down into Preloop time, Loop time and Memcpy & transpose time. The third of these number is the time it takes to copy the initial right hand side onto the device, transpose it in GPU memory and copy the final solution back to the host. Since the transposition in GPU memory is very fast, this number is a very good indicator of the host-device memory transfer performance. As the number below show, it can make up 10%-20% of the total solution time.

[...]
  Preloop time            = 6.464e+01 ms
  Loop time               = 2.835e+03 ms
  Memcpy & transpose time = 3.548e+02 ms
[...]

(3.B) - time per iteration
The time per iteration is the time spent in one call to the main loop body. As the entire calculation is carried out on the GPU and no data is moved to the host inside the loop, this number provides a good indicator of the GPU performance itself. As the code is memory bound, it is mainly bounded by the global memory speed. The time does, however, include the inter-GPU halo transfer. This halo transfer time is more significant for the multigrid solver and on the smaller grid (256x256) than for the CG solver and on the larger grid (512x512).

[...]
  Time per iteration      = 4.888e+01 ms
[...]

(3.C) - network bandwidth.
The code carries out a number of halo exchanges and calculates the resulting network bandwidth. The time per halo exchange is reported:

[...]
 Time per halo exchange 1.41451 ms
 Network bandwidth = 1.4826 GB/s
[...]
