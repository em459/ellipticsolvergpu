/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "kernel_prolongate.hh"

/* *********************************************************** *
 * Smoother kernel
 * *********************************************************** */
__global__ void gpu_prolongate(
                         const int NX, const int NY,
                         const int nx, const int ny,
                         const int xp, const int yp,
                         const real* a_vert,
                         const real* b_vert,
                         const real* c_vert,
                         const real* d_vert,
                         const real* u_c,
      	                 real* u,
                         const int offset_x,
                         const int offset_y
                        ) {
  int ix, iy, iz;
  ix=blockIdx.x*blockDim.x+threadIdx.x+offset_x;
  iy=blockIdx.y*blockDim.y+threadIdx.y+offset_y;
  // Global coordinates for boundary checking
  int ix_c_A = (ix-1)/2+1;
  int iy_c_A = (iy-1)/2+1;
  int dix = 2*((ix-1) % 2)-1;
  int diy = 2*((iy-1) % 2)-1;
  real rho_A;
  real rho_B;
  real rho_C;
  int gix = (xp)*(NX/2)+(ix_c_A+dix);
  int giy = (yp)*(NY/2)+(iy_c_A+diy);
  if ( (gix == 0) || (gix == nx/2+1) ) {
    rho_B = 0.5;
  } else {
    rho_B = 0.25;
  }
  if ( (giy == 0) || (giy == ny/2+1) ) {
    rho_C = 0.5;
  } else {
    rho_C = 0.25;
  }
  rho_A = 1.-rho_B-rho_C;
  for (iz=0;iz<NZ;++iz) {
    real tmp_u = u[LINIDX_X(NX,NY,ix,iy,iz)];
    tmp_u += rho_A*u_c[LINIDX_X(NX/2,NY/2,ix_c_A    ,iy_c_A    ,iz)];
    tmp_u += rho_B*u_c[LINIDX_X(NX/2,NY/2,ix_c_A+dix,iy_c_A    ,iz)];
    tmp_u += rho_C*u_c[LINIDX_X(NX/2,NY/2,ix_c_A    ,iy_c_A+diy,iz)];
    u[LINIDX_X(NX,NY,ix,iy,iz)] = tmp_u;
  }
}
