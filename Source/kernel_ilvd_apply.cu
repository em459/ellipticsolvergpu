/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include"kernel_ilvd_apply.hh"

/* *********************************************************** *
 * Interleaved apply y = A.x kernel [device version]
 * *********************************************************** */
__global__ void gpu_ilvd_apply(
                               const int NX, const int NY,
                               const int nx, const int ny,
                               const int xp, const int yp,
                               const real* a_vert,
                               const real* b_vert,
                               const real* c_vert,
                               const real* d_vert,
                               const real* z,
                               const real alpha_prime,
                               const real beta,
                               real* u,
      	                       real* p,
                               real* q,
                               real* pq,
                               const int offset_x,
                               const int offset_y
                              ) {
  int ix,iy,iz;
  real tmp_pq=0.0;
  DECLARE_DISCRETISATION_VARIABLES
  ix=blockIdx.x*blockDim.x+threadIdx.x+offset_x;
  iy=blockIdx.y*blockDim.y+threadIdx.y+offset_y;
  CALCULATE_PAR_ALPHA_TIJ(NX,NY,nx,ny,xp,yp,ix,iy)
  for (iz=0;iz<NZ;++iz) {
    real tmp_p = p[LINIDX_X(NX,NY,ix,iy,iz)];
    real tmp_q = q[LINIDX_X(NX,NY,ix,iy,iz)];
    real tmp_z = z[LINIDX_X(NX,NY,ix,iy,iz)];
    u[LINIDX_X(NX,NY,ix,iy,iz)] += alpha_prime*tmp_p;
    tmp_p *= beta;
    tmp_p += tmp_z;
    p[LINIDX_X(NX,NY,ix,iy,iz)] = tmp_p;
    // Diagonal and vertical couplings
    tmp_q *= beta;
    real b_tmp = b_vert[iz];
    real c_tmp = c_vert[iz];
    real d_tmp = d_vert[iz];
    real delta_q = ((a_vert[iz]-b_tmp-c_tmp)*Tij-alpha*d_tmp)*tmp_z;
    if (iz<NZ-1) {
      delta_q+= b_tmp*Tij*z[LINIDX_X(NX,NY,ix,iy,iz+1)];
    }
    if (iz>0) {
      delta_q+= c_tmp*Tij*z[LINIDX_X(NX,NY,ix,iy,iz-1)];
    }
    // Horizontal couplings
    delta_q+= z[LINIDX_X(NX,NY,ix-1,iy,iz)] * alpha_im1_j*d_tmp;
    delta_q+= z[LINIDX_X(NX,NY,ix+1,iy,iz)] * alpha_ip1_j*d_tmp;
    delta_q+= z[LINIDX_X(NX,NY,ix,iy-1,iz)] * alpha_i_jm1*d_tmp;
    delta_q+= z[LINIDX_X(NX,NY,ix,iy+1,iz)] * alpha_i_jp1*d_tmp;
    tmp_q += delta_q;
    q[LINIDX_X(NX,NY,ix,iy,iz)] = tmp_q;
    tmp_pq += tmp_p*tmp_q;
  }
  pq[LINIDX_2D(NX,NY,ix,iy)] = tmp_pq;
}
