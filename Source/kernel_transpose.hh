/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef KERNEL_TRANSPOSE_HH
#define KERNEL_TRANSPOSE_HH KERNEL_TRANSPOSE_HH
#include <iostream>
#include <mpi.h>
#include <cuda.h>
#include "definitions.hh"
#include "cuda_errorcheck.hh"

#define TILE_SIZE 32
#define BLOCK_ROWS 8

/* ********************************************************************* *
 * Device functions for transposing a field
 * ********************************************************************* */
// LINIDX_Z -> LINIDX_X
__global__ void gpu_transposeZtoX(int NX,
                                  int NY,
                                  real* phi,
                                  real* phiT);

// LINIDX_X -> LINIDX_Z
__global__ void gpu_transposeXtoZ(int NX,
                                  int NY,
                                  real* phi,
                                  real* phiT);

/* ********************************************************************* *
 * enum for direction
 * ********************************************************************* */
enum TransDirType {XtoZ, ZtoX};

/* ********************************************************************* *
 * Wrapper class for transpose-kernel
 * ********************************************************************* */
class KernelTranspose {
 public:
  // Constructor
  KernelTranspose(const int NX_,
                  const int NY_);
  void invoke(real *phi,
              real* phiT,
              const TransDirType direction);
 private:
  const int NX;
  const int NY;
  int grid_dim;
  int block_dim;
  dim3 dimGrid_ZtoX;
  dim3 dimGrid_XtoZ;
  dim3 dimBlock;
};

#endif // KERNEL_TRANSPOSE_HH
