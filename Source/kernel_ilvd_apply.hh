/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef KERNEL_ILVD_APPLY_HH
#define KERNEL_ILVD_APPLY_HH KERNEL_ILVD_APPLY_HH
#include<vector>
#include<cuda.h>
#include <GCL.h>
#include"definitions.hh"
#include"discretisation.hh"
#include"kernel.hh"
#include"cuda_errorcheck.hh"

/* *********************************************************** *
 * Interleaved apply y = A.x kernel [device version]
 * *********************************************************** */
__global__ void gpu_ilvd_apply(const int NX, const int NY,
                               const int nx, const int ny,
                               const int xp, const int yp,
                               const real* a_vert,
                               const real* b_vert,
                               const real* c_vert,
                               const real* d_vert,
                               const real* z,
                               const real alpha_prime,
                               const real beta,
                               real* u,
    	                         real* p,
                               real* q,
                               real* pq,
                               const int offset_x,
                               const int offset_y
                              );
/* ********************************************************************* *
 * Wrapper class for interleaved apply kernel
 * ********************************************************************* */
class IlvdApplyKernel : public Kernel {
 public:
  // Constructor
  IlvdApplyKernel (const VerticalDiscretisation* vert_disc,
                   real* z_,
                   real* alpha_prime_,
                   real* beta_,
                   real* u_,
      	           real* p_,
                   real* q_,
                   real* pq_) :
  Kernel(vert_disc),
  z(z_), alpha_prime(alpha_prime_), beta(beta_),
  u(u_), p(p_), q(q_), pq(pq_) {}
  // Launch kernel on a particular subdomain
  inline void invoke(dim3 dimGrid,
              dim3 dimBlock,
              cudaStream_t stream,
              int NX,
              int NY,
              int nx,
              int ny,
              int xp,
              int yp,
              int offset_x,
              int offset_y) const {
    gpu_ilvd_apply<<<dimGrid,dimBlock,0,stream>>>
      (NX,NY,nx,ny,
      xp,yp,
      a_vert,b_vert,c_vert,d_vert,
      z,*alpha_prime,*beta,u,p, q,pq,
      offset_x,offset_y);
    cuda_checkError();
  }
 private:
  real* z;
  real* alpha_prime;
  real* beta;
  real* u;
  real* p;
  real* q;
  real* pq;
};

#endif // KERNEL_ILVD_APPLY_HH
