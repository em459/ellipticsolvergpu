/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "pcg_host.hh"

/* *********************************************************** *
 * preconditioned CG solver, solve A.x = b for x,
 * given an initial guess for x
 * *********************************************************** */
void pcg_solve(const int nx,
               const int ny,
               const real omega2,
               const real lambda2,
               real resreduction,
               unsigned int maxiter,
               int verbose,
               real* b,
    	         real* x) {
  // Loop variables, counts iterations
  unsigned int k;
  const int nxyz=(nx+2*OL_X)*(ny+2*OL_Y)*NZ;
  // Allocate temporary variables
  real* r = (real*) malloc(nxyz*sizeof(real));
  real* p = (real*) malloc(nxyz*sizeof(real));
  real* q = (real*) malloc(nxyz*sizeof(real));

  real alpha, beta;

  // Norms
  real rnorm, rnorm0, rnorm_old,rz, rznew;

  // Solver converged
  bool solverconverged = false;

  int pid, nprocs,nxyp,xp,yp;

  // Timers
  clock_t start_matrixsetup,start_init,start_iter;
  clock_t end_matrixsetup,end_init,end_iter;
  double time_matrixsetup,time_init,time_iter;
  time_iter=0;

  MPI_Comm CartComm;
  setup_processor_layout(&pid,&nprocs,&nxyp,&xp,&yp,&CartComm);

  if (verbose >= 1) {
    if (pid == 0) {
      printf("****************************************\n");
      printf("* C matrixfree                         *\n");
      printf("****************************************\n");
      printf("\n");
    }
  }
  clock_t tc_start = clock();

  start_matrixsetup =clock();
  // Set up vertical matrix coefficients
  VerticalDiscretisation vert_disc(omega2,lambda2);
  real* a_vert = vert_disc.get_a_vert();
  real* b_vert = vert_disc.get_b_vert();
  real* c_vert = vert_disc.get_c_vert();
  real* d_vert = vert_disc.get_d_vert();

  end_matrixsetup=clock();
  time_matrixsetup=(((double)(end_matrixsetup-start_matrixsetup))/CLOCKS_PER_SEC);

  // Initialise
  start_init = clock();
  // *** b -> r_0
  BLAS_COPY(nxyz,b,1,r,1);
  // *** A.x_0 -> q_0
  apply(nx,ny,
        a_vert,
        b_vert,
        c_vert,
        d_vert,
        x,q);
  // set r and q to zero on the outer halo, to ensure they are treated
  // correctly in the BLAS calls
  int i,j;
  for(i=1-OL_X;i<=nx+OL_X;i++) {
    for(j=1-OL_Y;j<=ny+OL_Y;j++) {
      for(k=0;k<NZ;k++) {
        if ( (i < 1) || (i > nx) || (j < 1) || (j > ny) ) {
          r[LINIDX_Z(nx,ny,i,j,k)] = 0.0;
          q[LINIDX_Z(nx,ny,i,j,k)] = 0.0;
        }
      }
    }
  }
  // *** r_0 - q_0 = r_0 - A.x_0 -> r_0
  BLAS_AXPY(nxyz,-1.0,q,1,r,1);
  // *** M^{-1}.r_0 -> z_0
  prec(nx,ny,a_vert,b_vert,c_vert,d_vert,r,q);
  // *** z_0 -> p_0
  BLAS_COPY(nxyz,q,1,p,1);
  // *** rz -> <x,z>
  rz = BLAS_DOT(nxyz,r,1,q,1);
  // *** ||r_{0}||^2
  rnorm0 = BLAS_NRM2(nxyz,r,1);
  rnorm_old = rnorm0;
  end_init = clock();
  time_init=(((double)(end_init-start_init))/CLOCKS_PER_SEC);

  if (verbose >= 2) {
    printf("  Initial residual = %8.4e\n",rnorm0);
  }

  start_iter = clock();

  // Main loop over iterations
  for (k=1; k<=maxiter; k++) {

    // *** A.p_k -> q_k
    apply(nx,ny,
          a_vert,
          b_vert,
          c_vert,
          d_vert,
          p,q);
    // *** pq = <p_k,q_k> = <p_k, A.p_k>
    real pq = BLAS_DOT(nxyz,p,1,q,1);
    // *** alpha_k = <r_k,z_k> / <p_k,A.p_k>
    alpha = rz/pq;

    // *** x_k + alpha_k * p_k -> x_{k+1}
    BLAS_AXPY(nxyz,alpha,p,1,x,1);
    // *** r_k - alpha_k * A.p_k -> r_{k+1}
    BLAS_AXPY(nxyz,-alpha,q,1,r,1);
    // *** ||r_{k+1}||^2 rnorm
    rnorm = BLAS_NRM2(nxyz,r,1);

    if (verbose >= 2){
      printf("    iteration %d ||r|| = %8.3e rho_r = %6.3f\n",k,rnorm,rnorm/rnorm_old);
    }
    if (rnorm/rnorm0 < resreduction) {
      solverconverged = true;
      break;
    }

    // *** z_k -> M^{-1} r
  	prec(nx,ny,a_vert,b_vert,c_vert,d_vert,r,q);

    // *** <r_{{k+1},z_{k+1}> -> r2new
    rznew = BLAS_DOT(nxyz,r,1,q,1);

    // beta_k = <r_{k+1},z_{k+1}> / <r_k,z_k>
    beta = rznew/rz;

    // beta_k * p_k -> p_{k+1}
    BLAS_SCAL(nxyz,beta,p,1);

    // p_{k+1} + z_k -> p_{k+1}
    BLAS_AXPY(nxyz,1.0,q,1,p,1);

    rz = rznew;                 // update <r_k, z_k> -> <r_{k+1},z_{k+1}>
    rnorm_old = rnorm;
  }
  clock_t tc_end = clock();
  end_iter = clock();//timer:end of whole solver
  if (verbose >= 1) {
    if (pid == 0) {
      printf("  ----------------------------------------\n");
      if (solverconverged) {
        printf("  Solver converged in %4d iterations.\n",k);
      } else {
        printf("  Solver failed to converge after %4d iterations.\n",k);
      }
      time_iter=((double)(end_iter-start_iter))/CLOCKS_PER_SEC;
      printf("  Number of iterations = %4d\n",k);
      printf("  ||r||/||r_0||        = %9.4e\n",rnorm/rnorm0);
      printf("  rho_{avg}            = %6.3f\n",pow(rnorm/rnorm0,(real)1./k));
      printf("  Initialisation time  = %12.4f s\n",time_init);
      printf("  Matrix setup time    = %12.4f s\n",time_matrixsetup);
      printf("  Main loop time       = %12.4f s\n",time_iter);
      printf("  Time per iteration   = %12.8f s\n",
        time_iter/k);
      printf("  Solve time [clock]      = %12.4f s\n",((double)(tc_end-tc_start))/CLOCKS_PER_SEC);
      printf("  ----------------------------------------\n");
      printf("\n");
    }
  }
  // Tidy up
  free(r);
  free(p);
  free(q);
}
