/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef SOLVER_HH
#define SOLVER_HH SOLVER_HH
#include <mpi.h>
#include "definitions.hh"
#include "discretisation.hh"
#include "multi_gpu.hh"
#include "timer.hh"
#include <fstream>
#include <iostream>

/* *********************************************************** *
 * Virtual base class for solver
 * *********************************************************** */

class Solver {
 public:
  // Constructor
  Solver(DomainLayout* threadlayout_,
         const real omega2_,
         const real lambda2_,
         const int maxiter_=20,
         const real tolerance_=1.0E-5,
         const int verbose_=1) :
   threadlayout(threadlayout_),
   omega2(omega2_),
   lambda2(lambda2_),
   maxiter(maxiter_),
   tolerance(tolerance_),
   verbose(verbose_) {
   // Setup processor layout
   setup_processor_layout(&pid,&nprocs,&nxyp,&xp,&yp,&cart_comm);
   if (pid != 0) verbose = 0;
   // Create vertical discretisation
   vert_disc = new VerticalDiscretisation(omega2,lambda2);
  }
  // Destructor
  ~Solver() {
    delete vert_disc;
  }
  // Solve method
  virtual void solve(real* b,
                     real* u) = 0;
  // Print out timing- and convergence results
  void showResults();
 protected:
  // MPI variables
  int pid, nprocs, xp, yp, nxyp, nx, ny, nxyz;
  MPI_Comm cart_comm;
  // Model parameters
  real omega2;
  real lambda2;
  // Maximal number of iterations
  int maxiter;
  // Required residual reduction
  real tolerance;
  // Thread layout
  DomainLayout *threadlayout;
  // Verbosity level (0,1 or 2)
  int verbose;
  // Vertical discretisation
  VerticalDiscretisation* vert_disc;
  // Solver results
  // Solver converged?
  bool solverconverged;
  // Number of iteration
  int nIter;
  // time (preloop, CG only)
  double t_preloop;
  // time in main loop
  double t_loop;
  // Time for memory copy and transpose
  double t_memcpy_transpose;
  // Initial...
  real rnorm0;
  // ... and final residual norm
  real rnorm;
  // Kernel Timers
  std::vector<Timer*> timers;
};

#endif // SOLVER_HH
