/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef KERNEL_RESTRICT_SMOOTH_HH
#define KERNEL_RESTRICT_SMOOTH_HH KERNEL_RESTRICT_SMOOTH_HH
/* *********************************************************** *
 * GPU Implementation of the restrict-and-smoother kernel
 * *********************************************************** */
#include<cuda.h>
#include"definitions.hh"
#include"discretisation.hh"
#include"kernel.hh"
#include"cuda_errorcheck.hh"

/* *********************************************************** *
* Kernel for Restriction + Block-Jacobi smoother
* u <- u + rho_relax*M^{-1}.(R.r-A.u)
* where r is given on the next-finer level
* *********************************************************** */
__global__ void gpu_restrict_smooth(const int NX, const int NY,
                           const int nx, const int ny,
                           const int xp, const int yp,
                           const real* a_vert,
                           const real* b_vert,
                           const real* c_vert,
                           const real* d_vert,
                           real* dev_c,
                           const real* r,
    	                     real* b,
    	                     real* u,
                           real rho_relax,
                           const int offset_x,
                           const int offset_y
                          );

/* ********************************************************************* *
 * Wrapper class for smoother kernel
 * ********************************************************************* */
class RestrictSmoothKernel : public Kernel {
 public:
  // Constructor
  RestrictSmoothKernel(const VerticalDiscretisation* vert_disc,
                       real* dev_c_,
                       real* r_,
                       real* b_,
                       real* u_,
                       real rho_relax_) :
  Kernel(vert_disc),
  dev_c(dev_c_), r(r_), b(b_), u(u_), rho_relax(rho_relax_) {
    halo_fields.push_back(u);
  }
  // Launch kernel on a particular subdomain
  inline void invoke(dim3 dimGrid,
              dim3 dimBlock,
              cudaStream_t stream,
              int NX,
              int NY,
              int nx,
              int ny,
              int xp,
              int yp,
              int offset_x,
              int offset_y) const {
    gpu_restrict_smooth<<<dimGrid,dimBlock,0,stream>>>
      (NX,NY,nx,ny,
       xp,yp,
       a_vert,b_vert,c_vert,d_vert,
       dev_c,
       r,b,u,
       rho_relax,
       offset_x,
       offset_y);
    cuda_checkError();
  }
 private:
  real* r;
  real* u;
  real* b;
  // temporary scratch space for c'
  real* dev_c;
  // Overrelaxation factor
  real rho_relax;
};

#endif // KERNEL_RESTRICT_SMOOTH_HH
