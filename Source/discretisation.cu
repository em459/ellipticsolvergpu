/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "discretisation.hh"

/* ********************************************************************* *
 * Constructor
 * ********************************************************************* */
VerticalDiscretisation::VerticalDiscretisation(const real omega2_,
                                               const real lambda2_) :
    omega2(omega2_), lambda2(lambda2_) {
  a_vert = (real*) malloc(NZ*sizeof(real));
  b_vert = (real*) malloc(NZ*sizeof(real));
  c_vert = (real*) malloc(NZ*sizeof(real));
  d_vert = (real*) malloc(NZ*sizeof(real));
  cuda_safeCall(cudaMalloc((void**)&dev_a_vert,NZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_b_vert,NZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_c_vert,NZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_d_vert,NZ*sizeof(real)));
  // Create matrix with vertical discretisation ...
  create_vertical_matrix();
  // ... and copy to device
  cuda_safeCall(cudaMemcpy(dev_a_vert,a_vert,NZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_b_vert,b_vert,NZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_c_vert,c_vert,NZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_d_vert,d_vert,NZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cudaDeviceSynchronize();
}

/* ********************************************************************* *
 * Destructor
 * ********************************************************************* */
VerticalDiscretisation::~VerticalDiscretisation() {
  free(a_vert);
  free(b_vert);
  free(c_vert);
  free(d_vert);
  cuda_safeCall(cudaFree(dev_a_vert));
  cuda_safeCall(cudaFree(dev_b_vert));
  cuda_safeCall(cudaFree(dev_c_vert));
  cuda_safeCall(cudaFree(dev_d_vert));
}

/* ********************************************************************* *
 * Calculate vertical matrix coefficients
 * ********************************************************************* */
void VerticalDiscretisation::create_vertical_matrix() {
  int ix, iy, iz;
  real a_k, b_k, c_k, d_k;
  for (iz = 0; iz<NZ; iz++) {
    calculate_abcd(omega2,lambda2,iz,
                   &a_k,&b_k,&c_k,&d_k);
    d_vert[iz] = d_k;
    a_vert[iz] = a_k;
    b_vert[iz] = b_k;
    c_vert[iz] = c_k;
  }
}
