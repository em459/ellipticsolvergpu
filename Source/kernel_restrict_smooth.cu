/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "kernel_restrict_smooth.hh"

/* *********************************************************** *
 * Smoother kernel
 * *********************************************************** */
__global__ void gpu_restrict_smooth(
                         const int NX, const int NY,
                         const int nx, const int ny,
                         const int xp, const int yp,
                         const real* a_vert,
                         const real* b_vert,
                         const real* c_vert,
                         const real* d_vert,
                         real* dev_c,
                         const real* r,
                         real* b,
      	                 real* u,
                         real rho_relax,
                         const int offset_x,
                         const int offset_y
                        ) {
  DECLARE_DISCRETISATION_VARIABLES
  int ix, iy, iz;
  ix=blockIdx.x*blockDim.x+threadIdx.x+offset_x;
  iy=blockIdx.y*blockDim.y+threadIdx.y+offset_y;
  // Cell coordinates on fine grid
  int ix_f = 2*(ix-1)+1;
  int iy_f = 2*(iy-1)+1;
  CALCULATE_PAR_ALPHA_TIJ(NX,NY,nx,ny,xp,yp,ix,iy)
  // Do a tridiagonal solve in the vertical direction
  // STEP 1: Calculate modified coefficients
  real a_tmp = a_vert[0];
  real b_tmp = b_vert[0];
  real c_tmp = c_vert[0];
  real d_tmp = d_vert[0];
  real diag_tmp = ((a_vert[0]-b_tmp-c_tmp)*Tij-alpha*d_tmp);
  dev_c[LINIDX_X(NX,NY,ix,iy,0)] = b_tmp*Tij/diag_tmp;
  real b_coarse = r[LINIDX_X(2*NX,2*NY,ix_f  ,iy_f  ,0)]
                + r[LINIDX_X(2*NX,2*NY,ix_f+1,iy_f  ,0)]
                + r[LINIDX_X(2*NX,2*NY,ix_f  ,iy_f+1,0)]
                + r[LINIDX_X(2*NX,2*NY,ix_f+1,iy_f+1,0)];
  u[LINIDX_X(NX,NY,ix,iy,0)] = rho_relax * b_coarse / diag_tmp;
  b[LINIDX_X(NX,NY,ix,iy,0)] = b_coarse;
  for (iz = 1; iz<NZ; iz++) {
    real a_tmp = a_vert[iz];
    real b_tmp = b_vert[iz];
    real c_tmp = c_vert[iz];
    real d_tmp = d_vert[iz];
    real tmp = ((a_tmp-b_tmp-c_tmp)*Tij-alpha*d_tmp)
             - dev_c[LINIDX_X(NX,NY,ix,iy,iz-1)]*c_tmp*Tij;
    dev_c[LINIDX_X(NX,NY,ix,iy,iz)] = b_tmp*Tij / tmp;
    b_coarse = r[LINIDX_X(2*NX,2*NY,ix_f  ,iy_f  ,iz)]
             + r[LINIDX_X(2*NX,2*NY,ix_f+1,iy_f  ,iz)]
             + r[LINIDX_X(2*NX,2*NY,ix_f  ,iy_f+1,iz)]
             + r[LINIDX_X(2*NX,2*NY,ix_f+1,iy_f+1,iz)];
    b[LINIDX_X(NX,NY,ix,iy,iz)] = b_coarse;
    u[LINIDX_X(NX,NY,ix,iy,iz)] =
      (rho_relax * b_coarse - u[LINIDX_X(NX,NY,ix,iy,iz-1)] * c_tmp*Tij)/tmp;
  }
  // STEP 2: Back-substitution.
  for (iz = NZ-2; iz>=0; iz--) {
    u[LINIDX_X(NX,NY,ix,iy,iz)] -= dev_c[LINIDX_X(NX,NY,ix,iy,iz)]*u[LINIDX_X(NX,NY,ix,iy,iz+1)];
  }
}
