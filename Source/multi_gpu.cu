/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include"multi_gpu.hh"

/* ********************************************************************* *
 * Use MPI environment variable to bind to correct device.
 * ********************************************************************* */
void device_binding ()
{
  const int verbose = 0;
  int local_rank=0/*, num_local_procs*/;
  int dev_count, use_dev_count, my_dev_id;
  char *str;

  if ((str = getenv ("OMPI_COMM_WORLD_LOCAL_RANK")) != NULL)
    {
      local_rank = atoi (str);
      if (verbose) {
        printf ("OMPI_COMM_WORLD_LOCAL_RANK %s\n", str);
      }
    }

  if ((str = getenv ("MV2_COMM_WORLD_LOCAL_RANK")) != NULL)
    {
      local_rank = atoi (str);
      if (verbose) {
        printf ("MV2_COMM_WORLD_LOCAL_RANK %s\n", str);
      }
    }

  if ((str = getenv ("MPISPAWN_LOCAL_NPROCS")) != NULL)
    {
      //num_local_procs = atoi (str);
      if (verbose) {
        printf ("MPISPAWN_LOCAL_NPROCS %s\n", str);
      }
    }

  cudaGetDeviceCount (&dev_count);
  if ((str = getenv ("NUM_GPU_DEVICES")) != NULL)
    {
      use_dev_count = atoi (str);
      if (verbose) {
        printf ("NUM_GPU_DEVICES %s\n", str);
      }
    }
  else
    {
      use_dev_count = dev_count;
    }

  my_dev_id = local_rank % use_dev_count;
  cuda_safeCall(cudaSetDevice (my_dev_id));
}

/* ********************************************************************* *
 * Constructor I:
 * Set up all five thread subdomains explicitly
 * ********************************************************************* */
DomainLayout::DomainLayout(const int NX,
                           const int NX_center,
                           const int NY_center,
                           const int BLOCKSIZE_X_ALL,
                           const int BLOCKSIZE_Y_ALL,
                           const int BLOCKSIZE_X_CENTER,
                           const int BLOCKSIZE_Y_CENTER,
                           const int BLOCKSIZE_X_BOUND_EW,
                           const int BLOCKSIZE_Y_BOUND_EW,
                           const int BLOCKSIZE_X_BOUND_NS,
                           const int BLOCKSIZE_Y_BOUND_NS,
                           const bool overlap_comms_,
                           const bool verbose_) : verbose(verbose_) {
  setup_all(NX,
            NX_center,
            NY_center,
            BLOCKSIZE_X_ALL,
            BLOCKSIZE_Y_ALL,
            BLOCKSIZE_X_CENTER,
            BLOCKSIZE_Y_CENTER,
            BLOCKSIZE_X_BOUND_EW,
            BLOCKSIZE_Y_BOUND_EW,
            BLOCKSIZE_X_BOUND_NS,
            BLOCKSIZE_Y_BOUND_NS,
            overlap_comms_);
}

/* ********************************************************************* *
 * Constructor II:
 * Set up all five thread subdomains by reading from file
 * ********************************************************************* */
DomainLayout::DomainLayout(const char* filename,
                           const bool verbose_) : verbose(verbose_) {
  /* Read layout from file */
  FILE *file;
  int rank,nprocs;
  int NX;
  int NY;
  int NX_center;
  int NY_center;
  int BLOCKSIZE_X_ALL;
  int BLOCKSIZE_Y_ALL;
  int BLOCKSIZE_X_CENTER;
  int BLOCKSIZE_Y_CENTER;
  int BLOCKSIZE_X_BOUND_EW;
  int BLOCKSIZE_Y_BOUND_EW;
  int BLOCKSIZE_X_BOUND_NS;
  int BLOCKSIZE_Y_BOUND_NS;
  char s[128];
  int nxyp;
  bool overlap_comms_ = false;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  if (rank == 0) {
    file = fopen(filename,"r");
    char overlap_comms_s[128];
    fscanf(file,"%s = %d\n",&s,&NX);
    fscanf(file,"%s = %d\n",&s,&NX_center);
    fscanf(file,"%s = %d\n",&s,&NY_center);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_X_ALL);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_Y_ALL);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_X_CENTER);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_Y_CENTER);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_X_BOUND_EW);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_Y_BOUND_EW);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_X_BOUND_NS);
    fscanf(file,"%s = %d\n",&s,&BLOCKSIZE_Y_BOUND_NS);
    fscanf(file,"%s = %s\n",&s,&overlap_comms_s);
    if ( (!strncmp(overlap_comms_s,"t",1)) ||
         (!strncmp(overlap_comms_s,"T",1)) )
      overlap_comms_=true;
    else
      overlap_comms_=false;
    fclose(file);
  }
  MPI_Bcast(&NX,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&NX_center,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&NY_center,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_X_ALL,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_Y_ALL,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_X_CENTER,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_Y_CENTER,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_X_BOUND_EW,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_Y_BOUND_EW,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_X_BOUND_NS,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&BLOCKSIZE_Y_BOUND_NS,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&overlap_comms_,1,MPI_LOGICAL,0,MPI_COMM_WORLD);
  setup_all(NX,
            NX_center,
            NY_center,
            BLOCKSIZE_X_ALL,
            BLOCKSIZE_Y_ALL,
            BLOCKSIZE_X_CENTER,
            BLOCKSIZE_Y_CENTER,
            BLOCKSIZE_X_BOUND_EW,
            BLOCKSIZE_Y_BOUND_EW,
            BLOCKSIZE_X_BOUND_NS,
            BLOCKSIZE_Y_BOUND_NS,
            overlap_comms_);
}

/* ********************************************************************* *
 * Set up thread layout in all subdomains
 * ********************************************************************* */
void DomainLayout::setup_all(const int NX,
                             const int NX_center,
                             const int NY_center,
                             const int BLOCKSIZE_X_ALL,
                             const int BLOCKSIZE_Y_ALL,
                             const int BLOCKSIZE_X_CENTER,
                             const int BLOCKSIZE_Y_CENTER,
                             const int BLOCKSIZE_X_BOUND_EW,
                             const int BLOCKSIZE_Y_BOUND_EW,
                             const int BLOCKSIZE_X_BOUND_NS,
                             const int BLOCKSIZE_Y_BOUND_NS,
                             const bool overlap_comms_) {
  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  int nxyp = sqrt(nprocs);
  int NY=NX;
  overlap_comms = overlap_comms_;
  int NX_bound_EW = (NX-NX_center)/2;
  int NY_bound_EW = NY_center;
  int NX_bound_NS = NX;
  int NY_bound_NS = (NY-NY_center)/2;
  double r_center = 100.0*(NX_center*NX_center)/(1.0*NX*NY);
  double r_bound_EW = 2.*100.0*(NX_bound_EW)*(NY_bound_EW)/(1.0*NX*NY);
  double r_bound_NS = 2.*100.0*(NX_bound_NS)*(NY_bound_NS)/(1.0*NX*NY);
  int nx = NX*nxyp;
  int ny = NY*nxyp;
  if ( (rank == 0)  && (verbose) ) {
    printf(" interior domain layout\n");
    printf(" ======================\n");
    printf(" Subdomain sizes [interior]\n");
    printf("        NX          x NY            = %d x %d = %d\n",
      NX,NY,NX*NY);
    printf("        NX_center   x NY_center     = %d x %d = %d [%6.2f %]\n",
      NX_center,NY_center,NX_center*NY_center,r_center);
    printf("    2 x NX_bound_EW x NY_bound_EW   = 2 x %d x %d = %d [%6.2f %]\n",
      NX_bound_EW,NY_bound_EW,2*(NX_bound_EW)*(NY_bound_EW),r_bound_EW);
    printf("    2 x NX_bound_NS x NY_bound_NS   = 2 x %d x %d = %d [%6.2f %]\n",
      NX_bound_NS,NY_bound_NS,2*(NX_bound_NS)*(NY_bound_NS),r_bound_NS);
    printf(" Overlap X , Y = %d , %d\n",OL_X,OL_Y);
    printf(" Halo size = %d\n",HALOSIZE);
    if (overlap_comms) {
      printf(" Overlap comm/calc = YES\n");
    } else {
      printf(" Overlap comm/calc = NO\n");
    }
    printf("\n");
    printf(" Global domain size [interior]\n");
    printf("    nx x ny      = %d x %d      = %ld\n",nx,ny,nx*ny);
    printf("    nx x ny x nz = %d x %d x %d = %ld\n",nx,ny,NZ,nx*ny*NZ);
    printf(" Blocksizes\n");
    printf("    all       = %d x %d\n",
      BLOCKSIZE_X_ALL,BLOCKSIZE_Y_ALL);
    printf("    center    = %d x %d\n",
      BLOCKSIZE_X_CENTER,BLOCKSIZE_Y_CENTER);
    printf("    bound EW  = %d x %d\n",
      BLOCKSIZE_X_BOUND_EW,BLOCKSIZE_Y_BOUND_EW);
    printf("    bound NS  = %d x %d\n",
      BLOCKSIZE_X_BOUND_NS,BLOCKSIZE_Y_BOUND_NS);
    printf("\n");
  }
  // Entire enterior (ALL)
  setup_subdomain(NX,
                  NY,
                  BLOCKSIZE_X_ALL,
                  BLOCKSIZE_Y_ALL,
                  1,
                  1,
                  'A',
                  all);
  if (overlap_comms_) {
    // CENTER
    setup_subdomain(NX_center,
                    NY_center,
                    BLOCKSIZE_X_CENTER,
                    BLOCKSIZE_Y_CENTER,
                    NX_bound_EW+1,
                    NY_bound_NS+1,
                    'C',
                    center);
    // E
    setup_subdomain(NX_bound_EW,
                    NY_bound_EW,
                    BLOCKSIZE_X_BOUND_EW,
                    BLOCKSIZE_Y_BOUND_EW,
                    NX_bound_EW+NX_center+1,
                    NY_bound_NS+1,
                    'E',
                    boundary[0]);
    // W
    setup_subdomain(NX_bound_EW,
                    NY_bound_EW,
                    BLOCKSIZE_X_BOUND_EW,
                    BLOCKSIZE_Y_BOUND_EW,
                    1,
                    NY_bound_NS+1,
                    'W',
                    boundary[1]);
    // N
    setup_subdomain(NX_bound_NS,
                    NY_bound_NS,
                    BLOCKSIZE_X_BOUND_NS,
                    BLOCKSIZE_Y_BOUND_NS,
                    1,
                    NY_bound_NS+NY_center+1,
                    'N',
                    boundary[2]);
    // S
    setup_subdomain(NX_bound_NS,
                    NY_bound_NS,
                    BLOCKSIZE_X_BOUND_NS,
                    BLOCKSIZE_Y_BOUND_NS,
                    1,
                    1,
                    'S',
                    boundary[3]);
  }
}

/* ********************************************************************* *
 * Set up thread layout in one subdomain
 * ********************************************************************* */
void DomainLayout::setup_subdomain(const int NX,
                     const int NY,
                     const int block_size_x,
                     const int block_size_y,
                     const int offset_x,
                     const int offset_y,
                     const char label,
                     subDomainLayout& subdomain) {
  int pid,nprocs;
  int dx,dy,dz;
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  if (NX < block_size_x) {
    if (pid == 0) {
      printf("Local nx (%d) is smaller than block_size_x (%d) for subdomain %c\n", NX, block_size_x, label);
    }
    MPI_Finalize();
    exit(1);
  }
  if (NX < block_size_x) {
    if (pid == 0) {
      printf("Local nx (%d) is smaller than block_size_x (%d) for subdomain %c\n", NX, block_size_x, label);
    }
    MPI_Finalize();
    exit(1);
  }
  if ( (NX % block_size_x) != 0) {
    if (pid == 0) {
      printf("Local nx (%d) is not a multiple of block_size_x (%d) for subdomain %c\n", NX, block_size_x, label);
    }
    MPI_Finalize();
    exit(1);
  }
  if ( (NY % block_size_y) != 0) {
    if (pid == 0) {
      printf("Local nx (%d) is not a multiple of block_size_x (%d) for subdomain %c\n", NY, block_size_y, label);
    }
    MPI_Finalize();
    exit(1);
  }
  dx=NX/block_size_x;
  dy=NY/block_size_y;
  dz = 1;
  subdomain.NX=NX;
  subdomain.NY=NY;
  subdomain.dimBlock.x=block_size_x;
  subdomain.dimBlock.y=block_size_y;
  subdomain.dimBlock.z=1;
  subdomain.dimGrid.x=dx;
  subdomain.dimGrid.y=dy;
  subdomain.dimGrid.z=dz;
  subdomain.offset_x=offset_x;
  subdomain.offset_y=offset_y;
}


