/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef CUDA_ERRORCHECK_HH
#define CUDA_ERRORCHECK_HH CUDA_ERRORCHECK_HH
#include <iostream>
#include <cuda.h>

/* ********************************************************************* *
 * CUDA error checking routines
 * ********************************************************************* */

#define cuda_safeCall(f) __cuda_safeCall(f,__FILE__,__LINE__)
#define cuda_checkError() __cuda_checkError(__FILE__,__LINE__)

/* ********************************************************************* *
 * Safe call to cuda API routine
 * ********************************************************************* */
inline void __cuda_safeCall(cudaError f,
                            const char *file,
                            const int line ) {
#ifdef CUDA_ERROR_CHECK
  if (cudaSuccess != f) {
    std::cerr << "cuda_safeCall() failed at "
              << file << " line " << line
              << " : " << cudaGetErrorString(f) << std::endl;
    exit(-1);
  }
#endif
  return;
}

/* ********************************************************************* *
 * Check for error after kernel launch
 * ********************************************************************* */
inline void __cuda_checkError(const char *file,
                              const int line ) {
#ifdef CUDA_ERROR_CHECK
  cudaError f = cudaGetLastError();
  if (cudaSuccess != f) {
    std::cerr << "cuda_checkError() failed at "
              << file << " line " << line
              << " : " << cudaGetErrorString(f) << std::endl;
    exit(-1);
  }
#endif
  return;
}


#endif // CUDA_ERRORCHECK_HH
