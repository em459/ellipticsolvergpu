/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "kernel_residual.hh"

/* *********************************************************** *
 * Residual kernel
 * *********************************************************** */
__global__ void gpu_residual(
                         const int NX, const int NY,
                         const int nx, const int ny,
                         const int xp, const int yp,
                         const real* a_vert,
                         const real* b_vert,
                         const real* c_vert,
                         const real* d_vert,
                         const real* b,
      	                 const real* u,
                         real* r,
                         const int offset_x,
                         const int offset_y
                        ) {
  DECLARE_DISCRETISATION_VARIABLES
  int ix, iy, iz;
  ix=blockIdx.x*blockDim.x+threadIdx.x+offset_x;
  iy=blockIdx.y*blockDim.y+threadIdx.y+offset_y;
  CALCULATE_PAR_ALPHA_TIJ(NX,NY,nx,ny,xp,yp,ix,iy)
  // Loop over all levels
  for (iz = 0; iz<NZ; iz++) {
    real b_tmp = b_vert[iz];
    real c_tmp = c_vert[iz];
    real d_tmp = d_vert[iz];
    real diag_tmp = (a_vert[iz]-b_tmp-c_tmp)*Tij-alpha*d_tmp;
    real tmp_r = b[LINIDX_X(NX,NY,ix,iy,iz)];
    tmp_r -= diag_tmp          * u[LINIDX_X(NX,NY,ix  ,iy  ,iz  )];
    tmp_r -= alpha_im1_j*d_tmp * u[LINIDX_X(NX,NY,ix-1,iy  ,iz  )];
    tmp_r -= alpha_ip1_j*d_tmp * u[LINIDX_X(NX,NY,ix+1,iy  ,iz  )];
    tmp_r -= alpha_i_jm1*d_tmp * u[LINIDX_X(NX,NY,ix,  iy-1,iz  )];
    tmp_r -= alpha_i_jp1*d_tmp * u[LINIDX_X(NX,NY,ix,  iy+1,iz  )];
    if (iz < NZ-1)
      tmp_r -= b_tmp*Tij         * u[LINIDX_X(NX,NY,ix,  iy,  iz+1)];
    if (iz > 0)
      tmp_r -= c_tmp*Tij         * u[LINIDX_X(NX,NY,ix,  iy,  iz-1)];
    r[LINIDX_X(NX,NY,ix,iy,iz)] = tmp_r;
  }
}
