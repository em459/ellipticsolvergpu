/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef DEFINITIONS_HH
#define DEFINITIONS_HH DEFINITIONS_HH
/* *********************************************************** *
 * Global definitions file
 * *********************************************************** */

// Overlap in x- and y-direction
#define OL_X 32
#define OL_Y 1
// Halo size (in x- and y- direction)
#define HALOSIZE 1

// Number of vertical levels
#define NZ 128

/* Absolute value function */
#define ABS(a) (((a) < 0) ? -(a) : (a))

/* Macro for mapping local three dimensional index (ix,iy,iz) to
 * linear index in range 0,..., (NX+2*OL_X)*(NY+2*OL_Y)*NZ-1
 *
 * LOCAL index range:
 *   1-OL_X, ...,   1, 2, ..., NX,   ..., NX+OL_X
 *   <- pad -> | <- HALO -> | <- INTERIOR -> | <- HALO -> | <- pad ->
 *
 * GLOBAL index range:
 *   1-OL_X, ..., ! 1, 2, ..., nx, | ..., nx+OL_X
 *   <- pad -> | <- HALO -> | <- INTERIOR -> | <- HALO -> | <- pad ->
 *
 * Note that nx = npx * NX where npx is the number
 * of GPUs in direction x.
 *
 * For LINIDX_Z the vertical index is running fastest, so
 * vertical columns are always kept contiguous in memory,
 * For LINIDX_X the X index is running fastest
 */
#define LINIDX_Z(NX,NY,ix,iy,iz)          \
  (  (NZ)*((NY)+2*(OL_Y))*((ix)-1+(OL_X)) \
   + (NZ)*((iy)-1+(OL_Y))                 \
   + (iz)                                 \
  )
#define LINIDX_X(NX,NY,ix,iy,iz)          \
  (  ((NX)+2*(OL_X))*(NZ)*((iy)-1+(OL_Y)) \
   + ((NX)+2*(OL_X))*(iz)                 \
   + ((ix)-1+(OL_X))                      \
  )

#define LINIDX_ZYX(NX,NY,ix,iy,iz)        \
  (  ((NX)+2*(OL_X))*((NY)+2*(OL_Y))*(iz) \
   + ((NX)+2*(OL_X))*((iy)-1+(OL_Y))      \
   + ((ix)-1+(OL_X))                      \
  )

/* Macro for mapping two dimensional index (ix,iy) to
 * linear index */
#define LINIDX_2D(NX,NY,ix,iy)          \
  ( ((NX)+2*(OL_X))*((iy)-1+(OL_Y))     \
  + ((ix)-1+(OL_X))                     \
  )

/*  Define single/double precision data types
 */
#ifdef DOUBLE__PRECISION
  #define real double
  #define MPI_real MPI_DOUBLE
#else
  #define real float
  #define MPI_real MPI_FLOAT
#endif // DOUBLE__PRECISION

// Thickness of the atmosphere (in units of the earth radius).
#define H_ATMOS 0.01

#endif // DEFINITIONS_HH
