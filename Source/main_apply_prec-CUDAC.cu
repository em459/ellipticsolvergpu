/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<cuda.h>
#include<assert.h>
#include"definitions.hh"
#include"auxilliary.hh"
#include"apply_host.hh"
#include"prec_host.hh"
#include"kernel_apply.hh"
#include"kernel_prec.hh"
#include"multi_gpu.hh"
#include"functions.hh"
#include"function_mapper.hh"
#include"solver.hh"
#include"cuda_errorcheck.hh"

#include <mpi.h>

/* ********************************************************************* *
 * MAIN PROGRAM
 * ********************************************************************* */
int main(int argc, char* argv[]){

  int iiter;
  const int niterGPU=1000;

  int NX;
  int NY;

  // Test parallel communications?
  bool compare_to_host = true;
  bool compare_to_exact=false;

  int verbose = 1;
  int nlevel = 0;
  int ncoarsesmooth=2;

  SolverType solver_type;

  real omega2;
  real lambda2;
  real resreduction;
  int maxiter;
  int i,j,k;
  clock_t start, end;

  int pid;
  int nprocs;
  int xp,yp,nxyp,nx,ny;
  long int nxyz;

  device_binding();
  MPI_Init(&argc, &argv);
  GCL::GCL_Init(argc, argv);

  // Read parameters from disk
  char filename_param[128];
  char filename_domain[128];
  // Setup processor layout
  MPI_Comm CartComm;
  setup_processor_layout(&pid,&nprocs,&nxyp,&xp,&yp,&CartComm);
  if (argc != 3) {
    if (pid == 0) {
      printf("Usage: %s <parameterfile> <domainfile>\n",argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (pid == 0) {
    printf("***********************************************\n");
    printf("***********************************************\n");
    printf("*** Multi-GPU Matrix apply & preconditioner ***\n");
    printf("***********************************************\n");
    printf("***********************************************\n");
    printf("\n");
    printf("Running on %d GPUs.\n",nprocs);
    printf("\n");
    printf(" compiler flags\n");
    printf(" ==============\n");

#ifdef DOUBLE__PRECISION
    printf(" precision = DOUBLE\n");
#else
    printf(" precision = SINGLE\n");
#endif

#ifdef CARTESIAN_GEOMETRY
    printf(" geometry = CARTESIAN\n");
#else
    printf(" geometry = CUBED SPHERE\n");
#endif

#ifdef Z_GRADED
    printf(" z-graded = TRUE\n");
#else
    printf(" z-graded = FALSE\n");
#endif

#ifdef HOSTWORKAROUND
    printf("  HOSTWORKAROUND = TRUE\n");
#else
    printf("  HOSTWORKAROUND = FALSE\n");
#endif
    printf("\n");
  }

  strcpy(filename_param,argv[1]);
  strcpy(filename_domain,argv[2]);
  read_parameters(filename_param,
                  &solver_type,
                  &nlevel,
                  &ncoarsesmooth,
                  &omega2,
                  &lambda2,
                  &resreduction,
                  &maxiter,
                  &verbose,
                  &compare_to_host,
                  &compare_to_exact);

  // Thread layout
  DomainLayout threadlayout(filename_domain);

  NX=threadlayout.all.NX;
  NY=threadlayout.all.NY;
  const int NXYZ=(NX+2*OL_X)*(NY+2*OL_Y)*NZ;


  // Work out global problem size
  nx=NX*nxyp;
  ny=NY*nxyp;
  nxyz = (nx+2*OL_X)*(ny+2*OL_Y)*NZ;

  /* -------------------------------------- *
   *    Setup
   * -------------------------------------- */

  // Arrays for vertical discretisation
  VerticalDiscretisation vert_disc(omega2,lambda2);
  // * host
  real* a_vert = vert_disc.get_a_vert();
  real* b_vert = vert_disc.get_b_vert();
  real* c_vert = vert_disc.get_c_vert();
  real* d_vert = vert_disc.get_d_vert();

  // Set cache size
  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);

  // Allocate vectors for x and y
  real* x_l = (real*) malloc(NXYZ*sizeof(real));
  real* xT_l = (real*) malloc(NXYZ*sizeof(real));
  real* y_GPU_l = (real*) malloc(NXYZ*sizeof(real));
  real* yT_l = (real*) malloc(NXYZ*sizeof(real));
  real *dev_c;
  real *dev_x_l;
  real *dev_y_l;
  real *dev_z_l;

  real* x_seq;
  real* y_seq;
  real* z_seq;

  if (compare_to_host) {
    x_seq = (real*) malloc(nxyz*sizeof(real));
    z_seq = (real*) malloc(nxyz*sizeof(real));
    y_seq = (real*) malloc(nxyz*sizeof(real));
  }

  cuda_safeCall(cudaMalloc((void**)&dev_c,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_x_l,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_y_l,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_z_l,NXYZ*sizeof(real)));

  // Generate local vector x with random numbers
  FunctionRandom f_rand;
  FunctionMapper f_map(NX,NY);
  if (compare_to_host){ /// TESTING...
    f_map.mapGlobal(f_rand,x_seq);
    /* Initialise local vector by copying corresponding part
       of sequential vector */
    for(i=1-OL_X;i<=NX+OL_X;i++){
      for(j=1-OL_Y;j<=NY+OL_Y;j++) {
        for(k=0;k<NZ;k++) {
          x_l[LINIDX_Z(NX,NY,i,j,k)]
            = x_seq[LINIDX_Z(nx,ny,xp*NX+i,yp*NY+j,k)];
        }
      }
    }
  } else {
    f_map.mapLocal(f_rand,x_l);
  }

  /* Transpose array for using it on device */
  transposeZtoX(NX,NY,x_l,xT_l);

  // Copy data from host to device
  MPI_Barrier(MPI_COMM_WORLD);
  cuda_safeCall(cudaMemcpy(dev_x_l,xT_l,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cudaDeviceSynchronize();

  // Create kernels
  PrecKernel prec_kernel(&vert_disc,dev_c,dev_x_l,dev_z_l);
  ApplyKernel apply_kernel(&vert_disc,dev_z_l,dev_y_l);
  MultiGPULauncher multi_gpu_launcher(nx,ny,threadlayout);

  // Apply and preconditioner on device
  MPI_Barrier(MPI_COMM_WORLD);
  start=clock();
  for (iiter = 0; iiter<niterGPU;iiter++) {
    multi_gpu_launcher.invoke(apply_kernel);
  }
  end=clock();
  MPI_Barrier(MPI_COMM_WORLD);
  if (pid == 0) {
    printf(" parallel apply [device] = %12.8e s\n",
           (double)(end-start)/(CLOCKS_PER_SEC*niterGPU));
  }
  MPI_Barrier(MPI_COMM_WORLD);
  start=clock();
  for (iiter = 0; iiter<niterGPU;iiter++) {
    multi_gpu_launcher.invoke(prec_kernel);
  }
  end=clock();
  MPI_Barrier(MPI_COMM_WORLD);
  if (pid == 0) {
    printf(" parallel preconditioner [device] = %12.8e s\n",
           (double)(end-start)/(CLOCKS_PER_SEC*niterGPU));
  }

  // Copy data from device to host
  MPI_Barrier(MPI_COMM_WORLD);
  // Initialise y and z to zero and copy to device
  for (i=0;i<NXYZ;++i) yT_l[i] = 0.0;
  cuda_safeCall(cudaMemcpy(dev_y_l,yT_l,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_z_l,yT_l,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cudaDeviceSynchronize();
  MPI_Barrier(MPI_COMM_WORLD);

  /* Apply on host and device and compare results */
  if (compare_to_host) {
    prec(nx,ny,
         a_vert,
         b_vert,
         c_vert,
         d_vert,
         x_seq,z_seq);
    apply(nx,ny,
          a_vert,
          b_vert,
          c_vert,
          d_vert,
          z_seq,y_seq);
    multi_gpu_launcher.invoke(prec_kernel);
    multi_gpu_launcher.invoke(apply_kernel);
    cuda_safeCall(cudaMemcpy(yT_l,dev_y_l,NXYZ*sizeof(real),
                  cudaMemcpyDeviceToHost));
    cudaDeviceSynchronize();

    /* Transpose solution array back */
    transposeXtoZ(NX,NY,yT_l,y_GPU_l);
    /* Compare */
    real norm_diff;
    real norm_GPU_l;
    real norm_seq;
    real max_rel_diff;
    dnorm_loc_glo(NX, NY,
                  nx, ny,
                  xp, yp,
                  y_GPU_l,
                  y_seq,
                  norm_diff,
                  norm_GPU_l,
                  norm_seq,
                  max_rel_diff);
                  //norm_seq,pid,nprocs);
    if (pid == 0) {
      printf("Comparison C sequential / CUDAC parallel\n");
      printf (" ||y_seq[C]||           = %12.8e\n",norm_seq);
      printf (" ||y_[CUDA]||           = %12.8e\n",norm_GPU_l);
      printf (" ||y_seq[C]-y_[CUDAC]|| = %12.8e\n",norm_diff);
      printf (" maximal rel. diff.     = %12.8e\n",max_rel_diff);
    }
  }
  MPI_Finalize();
  exit(0);
  /* *** Tidy up *** */

  cuda_safeCall(cudaFree(dev_c));
  cuda_safeCall(cudaFree(dev_x_l));
  cuda_safeCall(cudaFree(dev_y_l));
  cuda_safeCall(cudaFree(dev_z_l));
  free(x_l);

  if (compare_to_host) {
    free(x_seq);
    free(z_seq);
    free(y_seq);
  }
  free(x_l);
  free(y_GPU_l);
  free(xT_l);
  free(yT_l);

  MPI_Finalize();

  return(0);
}
