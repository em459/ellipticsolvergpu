/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "kernel_zero.hh"
#include <iostream>

/* ********************************************************************* *
 * Device function for zeroing out a field
 * ********************************************************************* */
__global__ void gpu_zero(int NX,
                         int NY,
                         real* phi) {
  int ix = 1-OL_X+threadIdx.z + blockIdx.z * blockDim.z;
  int iy = 1-OL_Y+threadIdx.y + blockIdx.y * blockDim.y;
  int iz = threadIdx.x + blockIdx.x * blockDim.x;
  phi[LINIDX_Z(NX,NY,ix,iy,iz)] = 0.0;
}

/* ********************************************************************* *
 * Constructor
 * ********************************************************************* */
KernelZero::KernelZero(const int NX_,
                       const int NY_) : NX(NX_), NY(NY_) {
  dimGrid.x = 1;
  dimGrid.y = NY+2*OL_Y;
  dimGrid.z = NX+2*OL_X;
  dimBlock.x = NZ;
  dimBlock.y = 1;
  dimBlock.z = 1;
}

/* ********************************************************************* *
 * Invoke kernel
 * ********************************************************************* */
void KernelZero::invoke(real *phi) {
  gpu_zero<<<dimGrid,dimBlock>>>(NX,NY,phi);
  cuda_checkError();
}
