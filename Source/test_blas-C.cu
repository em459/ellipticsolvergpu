/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "blas_wrapper.h"

#ifdef DOUBLE__PRECISION
  #define real double
#else
  #define real float
#endif

/* ********************************************************************* *
 * Code for measuring BLAS performance
 * ********************************************************************* */

int main(int argc, char* argv[]) {
  unsigned int n=256*256*128;
  unsigned int i;
  int niter = 200;
  clock_t tstart, tfinish;
  int iiter;
  real* x;
  real* y;
  real alpha=0.76;
  x = (real*) malloc(n*sizeof(real));
  y = (real*) malloc(n*sizeof(real));
#ifdef HAVE_BLAS
  printf("Using BLAS\n");
#else
  printf("Using dummy BLAS\n");
#endif
#ifdef DOUBLE__PRECISION
  printf("PRECISION = DOUBLE\n");
#else
  printf("PRECISION = SINGLE\n");
#endif
  printf("CLOCKS_PER_SEC = %d\n",CLOCKS_PER_SEC);
  printf("Vector length = %d\n",n);
  printf("Iterations    = %d\n",niter);
  /* Initialise vector */
  srand(0);
  for (i=0;i<n;++i) {
    x[i] = (rand() % 1000) / 1000.;
    y[i] = (rand() % 1000) / 1000.;
  }
  /* *** copy *** */
  tstart = clock();
  for (iiter=0;iiter < niter;iiter++) {
    BLAS_COPY(n,x,1,y,1);
  }
  tfinish = clock();
  printf("t_iter copy [host] : %12.8e\n",(double)(tfinish-tstart)/(CLOCKS_PER_SEC*niter));
  /* Initialise vector */
  srand(0);
  for (i=0;i<n;++i) {
    x[i] = (rand() % 1000) / 1000.;
    y[i] = (rand() % 1000) / 1000.;
  }
  /* *** axpy *** */
  tstart = clock();
  for (iiter=0;iiter < niter;iiter++) {
    BLAS_AXPY(n,alpha,x,1,y,1);
  }
  tfinish = clock();
  printf("t_iter axpy [host] : %12.8e\n",(double)(tfinish-tstart)/(CLOCKS_PER_SEC*niter));
  /* Initialise vector */
  srand(0);
  for (i=0;i<n;++i) {
    x[i] = (rand() % 1000) / 1000.;
    y[i] = (rand() % 1000) / 1000.;
  }
  /* *** scal *** */
  tstart = clock();
  for (iiter=0;iiter < niter;iiter++) {
    BLAS_SCAL(n,alpha,y,1);
  }
  tfinish = clock();
  printf("t_iter scal [host] : %12.8e\n",(double)(tfinish-tstart)/(CLOCKS_PER_SEC*niter));
  /* Initialise vector */
  srand(0);
  for (i=0;i<n;++i) {
    x[i] = (rand() % 1000) / 1000.;
    y[i] = (rand() % 1000) / 1000.;
  }
  /* *** dotprod *** */
  tstart = clock();
  for (iiter=0;iiter < niter;iiter++) {
    BLAS_DOT(n,x,1,y,1);
  }
  tfinish = clock();
  printf("t_iter dot [host] : %12.8e\n",(double)(tfinish-tstart)/(CLOCKS_PER_SEC*niter));
  /* Initialise vector */
  srand(0);
  for (i=0;i<n;++i) {
    x[i] = (rand() % 1000) / 1000.;
    y[i] = (rand() % 1000) / 1000.;
  }
  /* *** norm *** */
  tstart = clock();
  for (iiter=0;iiter < niter;iiter++) {
    real nrm = BLAS_NRM2(n,x,1);
  }
  tfinish = clock();
  printf("t_iter nrm2 [host] : %12.8e\n",(double)(tfinish-tstart)/(CLOCKS_PER_SEC*niter));
  free(x);
  free(y);
}
