/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef AUXILLIARY_HH
#define AUXILLIARY_HH AUXILLIARY_HH
/* *********************************************************** *
 * Auxilliary routines
 * *********************************************************** */

#include<mpi.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<cuda.h>
#include<sstream>
#include"definitions.hh"
#include"discretisation.hh"
#include"cuda_errorcheck.hh"

// Enum for solver type
enum SolverType {
  SolverTypeCG,
  SolverTypeMultigrid
};

/* *********************************************************** *
 *  Set up processor layout
 * *********************************************************** */
void setup_processor_layout(int* pid,    // OUT: pid = rank
                            int* nprocs, // OUT: #procs
                            int* nxyp,   // OUT: #procs in x/y-direction
                            int* xp,     // OUT: # x-id/rank of proc
                            int* yp,     // OUT: # y-id/rank of proc
                            MPI_Comm *cart_comm // OUT: Cartesian comm
                           );

/* *********************************************************** *
 * Print out local vector (stored in LINIDX_Z ordering) on one processor
 * *********************************************************** */
void print_vec(const int NX,
               const int NY,
               int pid,
               int nprocs,
               real* x);

/* *********************************************************** *
 * Read runtime parameters from file
 * *********************************************************** */
void read_parameters(const char* filename,
                     SolverType* solver_type,
                     int *nlevel,
                     int *ncoarsesmooth,
                     real *omega2,
                     real *lambda2,
                     real *resreduction,
                     int *maxiter,
                     int *verbose,
                     bool *compare_to_host,
                     bool *compare_to_exact);

/* *********************************************************** *
 * Transpose x form Z -> X
 * *********************************************************** */
void transposeZtoX(const int NX,
                   const int NY,
                   const real* x, // vector stored in LINIDX_Z order
                   real* xT);     // vector stored in LINIDX_X order

/* *********************************************************** *
 * Transpose x form X -> Z
 * *********************************************************** */
void transposeXtoZ(const int NX,
                   const int NY,
                   const real* xT, // vector stored in LINIDX_X order
                   real* x);       // vector stored in LINIDX_Z order

/* *********************************************************** *
 * Calculate local part of L2-norm of y_l, y_g and local difference y1-y2
 * *********************************************************** */
void dnorm_loc_glo(const int NX,
                   const int NY,
                   const int nx,
                   const int ny,
                   const int xp, const int yp,
                   const real *y_l,
                   const real* y_g,
                   real& norm_y1my2,
                   real& norm_y1,
                   real& norm_y2,
                   real& max_rel_diff);

void dnorm_loc_loc(const int NX,
                   const int NY,
                   const real *y_1,
                   const real* y_2,
                   real& norm_y1my2,
                   real& norm_y1,
                   real& norm_y2,
                   real& max_rel_diff);

/* *********************************************************** *
 * Calculate L2 norm of a field y, which in each grid cell
 * is given as the average value of this field in
 * the cell.
 *
 * l2norm = \int_{\Omega} dx dy dz |y|^2
 *
 * *********************************************************** */
real l2norm(const int nx,
            const int ny,
            const real *y);

/* *********************************************************** *
 * Save distributed field to vtk file
 * *********************************************************** */
 void savetovtk(int NX,
                int NY,
                std::string filename,
                real* phi,
                bool includeHalos,
                bool devicePointer=false);

#endif // AUXILLIARY_HH
