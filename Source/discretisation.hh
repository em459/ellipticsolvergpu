/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef DISCRETISATION_HH
#define DISCRETISATION_HH DISCRETISATION_HH
/* ********************************************** *
 * 7 point stencil discretisation of elliptic
 * model PDE in Cartesian geometry and on one
 * segment of a gnomonic cubed sphere grid.
 * ********************************************** */
#include<stdlib.h>
#include<cuda.h>
#include"definitions.hh"
#include"math.h"
#include"cuda_errorcheck.hh"

// Graded vertical grid. For the graded grid, the size of
// vertical grid cells increases linearly. For the ungraded
// grid the size of vertical grid cells is constant.
#ifdef CARTESIAN_GEOMETRY
  #ifdef Z_GRADED
    inline real r_vert(int k) {
      return H_ATMOS*((real)k)*((real)k)/(1.0*NZ*NZ);
    }
  #else
    inline real r_vert(int k) {
      return H_ATMOS*((real)k)/(1.0*NZ);
    }
  #endif // Z_GRADED
#else
  #ifdef Z_GRADED
    inline real r_vert(int k) {
      return 1.0 + H_ATMOS*((real)k)*((real)k)/(1.0*NZ*NZ);
    }
  #else
    inline real r_vert(int k) {
      return 1.0 + H_ATMOS*((real)k)/(1.0*NZ);
    }
  #endif // Z_GRADED
#endif // CARTESIAN_GEOMETRY

/* ***************** *
 * *** Cartesian *** *
 * ***************** */

#ifdef CARTESIAN_GEOMETRY
/* ********************************************** *
 * Declare all variables which are needed for
 * the discretisation
 * ********************************************** */
#define DECLARE_DISCRETISATION_VARIABLES                          \
  real alpha_im1_j, alpha_ip1_j, alpha_i_jp1, alpha_i_jm1, alpha; \
  real Tij;                                                       \
  real h;

/* ********************************************** *
 * Calculate alpha and T_ij needed for the
 * horizontal discretisation
 * ********************************************** */
#define CALCULATE_ALPHA_TIJ(nx,ny,ix,iy)                 \
  h = 1.0/(nx);                                          \
  Tij = h*h;                                             \
  if ((ix) == (nx)) {                                    \
    alpha_ip1_j = 2.0;                                   \
  } else {                                               \
    alpha_ip1_j = 1.0;                                   \
  }                                                      \
  if ((ix) == 1) {                                       \
    alpha_im1_j = 2.0;                                   \
  } else {                                               \
    alpha_im1_j = 1.0;                                   \
  }                                                      \
  if ((iy) == (ny)) {                                    \
    alpha_i_jp1 = 2.0;                                   \
  } else {                                               \
    alpha_i_jp1 = 1.0;                                   \
  }                                                      \
  if ((iy) == 1) {                                       \
    alpha_i_jm1 = 2.0;                                   \
  } else {                                               \
    alpha_i_jm1 = 1.0;                                   \
  }                                                      \
  alpha = alpha_ip1_j                                    \
        + alpha_im1_j                                    \
        + alpha_i_jp1                                    \
        + alpha_i_jm1;

/* ********************************************** *
 * Multi-GPU: Calculate alpha and T_ij needed for the
 * horizontal discretisation
 * ********************************************** */
#define CALCULATE_PAR_ALPHA_TIJ(NX,NY,nx,ny,xp,yp,ix,iy)  \
  int gix;                                                \
  int giy;                                                \
  gix = (xp)*(NX)+(ix);                                   \
  giy = (yp)*(NY)+(iy);                                   \
  CALCULATE_ALPHA_TIJ(nx,ny,gix,giy)

/* ********************************************** *
 * Vertical volume factor
 * ********************************************** */
inline real volume_V(int k) {
  return (r_vert(k+1)-r_vert(k));
}

/* ********************************************** *
 * Surface stretch factor
 * ********************************************** */
inline real surface_area(int k) { return 1.0; }

#else // CARTESIAN_GEOMETRY

/* ******************** *
 * *** Cubed sphere *** *
 * ******************** */

/* ********************************************** *
 * Declare all variables which are needed for
 * the discretisation
 * ********************************************** */
#define DECLARE_DISCRETISATION_VARIABLES                          \
  real xi1, xi2;                                                  \
  real alpha_im1_j, alpha_ip1_j, alpha_i_jp1, alpha_i_jm1, alpha; \
  real Tij;                                                       \
  real h;

/* ********************************************** *
 * Calculate alpha and T_ij needed for the
 * horizontal discretisation
 * ********************************************** */
#define CALCULATE_ALPHA_TIJ(nx,ny,ix,iy)                         \
  h = 2.0/(nx);                                                  \
  xi1 = (ix-0.5)*(h) - 1.0;                                      \
  xi2 = (iy-0.5)*(h) - 1.0;                                      \
  Tij = pow(1.0+xi1*xi1+xi2*xi2,-1.5)*h*h;                       \
  if ((ix) == (nx)) {                                            \
    alpha_ip1_j =                                                \
      2.*sqrt((1.0+(xi1+0.25*h)*(xi1+0.25*h))/(1.+xi2*xi2));     \
  } else {                                                       \
    alpha_ip1_j =                                                \
      sqrt((1.0+(xi1+0.5*h)*(xi1+0.5*h))/(1.+xi2*xi2));          \
  }                                                              \
  if ((ix) == 1) {                                               \
    alpha_im1_j =                                                \
      2.*sqrt((1.0+(xi1-0.25*h)*(xi1-0.25*h))/(1.+xi2*xi2));     \
  } else {                                                       \
    alpha_im1_j =                                                \
      sqrt((1.0+(xi1-0.5*h)*(xi1-0.5*h))/(1.+xi2*xi2));          \
  }                                                              \
  if ((iy) == (ny)) {                                            \
    alpha_i_jp1 =                                                \
      2.*sqrt((1.0+(xi2+0.25*h)*(xi2+0.25*h))/(1.+xi1*xi1));     \
  } else {                                                       \
    alpha_i_jp1 =                                                \
      sqrt((1.0+(xi2+0.5*h)*(xi2+0.5*h))/(1.+xi1*xi1));          \
  }                                                              \
  if ((iy) == 1) {                                               \
    alpha_i_jm1 =                                                \
      2.*sqrt((1.0+(xi2-0.25*h)*(xi2-0.25*h))/(1.+xi1*xi1));     \
  } else {                                                       \
    alpha_i_jm1 =                                                \
      sqrt((1.0+(xi2-0.5*h)*(xi2-0.5*h))/(1.+xi1*xi1));          \
  }                                                              \
  alpha = alpha_ip1_j                                            \
        + alpha_im1_j                                            \
        + alpha_i_jp1                                            \
        + alpha_i_jm1;

/* ********************************************** *
 * Multi-GPU Calculate alpha and T_ij needed for the
 * horizontal discretisation
 * ********************************************** */
#define CALCULATE_PAR_ALPHA_TIJ(NX,NY,nx,ny,xp,yp,ix,iy)         \
  int gix;                                                       \
  int giy;                                                       \
  gix = (xp)*(NX)+(ix);                                          \
  giy = (yp)*(NY)+(iy);                                          \
  CALCULATE_ALPHA_TIJ(nx,ny,gix,giy)

/* ********************************************** *
 * Vertical volume factor
 * ********************************************** */
inline real volume_V(int k) {
  real r_vert_k = r_vert(k);
  real r_vert_kp1 = r_vert(k+1);
  return (r_vert_kp1*r_vert_kp1*r_vert_kp1-r_vert_k*r_vert_k*r_vert_k)/3.0;
}

/* ********************************************** *
 * Surface stretch factor
 * ********************************************** */
inline real surface_area(int k) {
  real r_vert_k = r_vert(k);
  return r_vert_k*r_vert_k;
}

#endif // CARTESIAN_GEOMETRY

/* ********************************************** *
 * Calculate diagonal (a_k) coupling, vertical
 * couplings (b_k, c_k) and horizontal coupling
 * (d_k)
 * ********************************************** */
inline void calculate_abcd(const real omega2,
                           const real lambda2,
                           const int k,
                           real* a_k,
                           real* b_k,
                           real* c_k,
                           real* d_k) {
  if (k < (NZ-1)) {
    *b_k = -2.*omega2*lambda2*surface_area(k+1)/(r_vert(k+2)-r_vert(k));
  } else {
    *b_k = 0.0;
  }
  if (k>0) {
    *c_k = -2.*omega2*lambda2*surface_area(k)/(r_vert(k+1)-r_vert(k-1));
  } else {
    *c_k = 0.0;
  }
  *a_k = volume_V(k);
  *d_k = - omega2*(r_vert(k+1)-r_vert(k));
}

/* ********************************************************************* *
 * Class representing vertical discretisation
 * ********************************************************************* */
class VerticalDiscretisation {
 public:
  // Constructor
  VerticalDiscretisation(const real omega2_,
                         const real lambda2_);
  // Destructor
  ~VerticalDiscretisation();
  // Return pointers to host arrays
  real* get_a_vert() const { return a_vert; }
  real* get_b_vert() const { return b_vert; }
  real* get_c_vert() const { return c_vert; }
  real* get_d_vert() const { return d_vert; }
  // Return pointers to device arrays
  real* get_dev_a_vert() const { return dev_a_vert; }
  real* get_dev_b_vert() const { return dev_b_vert; }
  real* get_dev_c_vert() const { return dev_c_vert; }
  real* get_dev_d_vert() const { return dev_d_vert; }
 private:
  // Create vertical matrix
  void create_vertical_matrix();
  double omega2;
  double lambda2;
  // Data arrays
  // * host
  real* a_vert;
  real* b_vert;
  real* c_vert;
  real* d_vert;
  // * device
  real *dev_a_vert;
  real *dev_b_vert;
  real *dev_c_vert;
  real *dev_d_vert;
};


#endif // DISCRETISATION_HH
