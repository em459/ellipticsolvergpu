/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef SOLVER_CG_HH
#define SOLVER_CG_HH SOLVER_CG_HH
/* *********************************************************** *
 * MPI-Parallel implementation of the Preconditioned CG algorithm
 * for solving the system A.x = b using the interleaved kernels.
 * *********************************************************** */

#include <time.h>
#include <halo_exchange.h>
#include <utils/layout_map.h>
#include <GCL.h>
#include <mpi.h>
#include <stdio.h>
#include "definitions.hh"
#include "cublas_wrapper.hh"
#include "kernel_apply.hh"
#include "kernel_ilvd_apply.hh"
#include "kernel_prec.hh"
#include "kernel_ilvd_prec.hh"
#include "kernel_zero.hh"
#include "apply_host.hh"
#include "kernel_transpose.hh"
#include "prec_host.hh"
#include "auxilliary.hh"
#include "multi_gpu.hh"
#include "solver.hh"
#include "cuda_errorcheck.hh"

/* *********************************************************** *
 * PCG solver class
 * *********************************************************** */
class SolverCG : public Solver {
 public:
  SolverCG(DomainLayout* threadlayout_,
           const real omega2_,
           const real lambda2_,
           const int maxiter_=20,
           const real tolerance_=1.0E-5,
           const int verbose_=2);
  // Destructor
  ~SolverCG();
  // Solve using the PCG iteration
  virtual void solve(real* b,
                     real* u);
 private:
  // Problem size and thread layout
  int NX;
  int NY;
  // Device pointers
  real *dev_x,
       *dev_b,
       *dev_r,
       *dev_p,
       *dev_q,
       *dev_z,
       *dev_pq,
       *dev_rsq,
       *dev_rz,
       *dev_one;
  //temporary vector for preconditioners
  real* dev_c;
  // Variables alpha and beta used in the PCG algorithm
  real alpha;
  real beta;
  // GPU Kernels
  ApplyKernel *apply_kernel_xq;
  ApplyKernel *apply_kernel_pq;
  PrecKernel *prec_kernel;
  IlvdPrecKernel *ilvd_prec_kernel;
  IlvdApplyKernel *ilvd_apply_kernel;
  cublasHandle_t cublasHandle;
  // Multi GPU Kernel launcher
  MultiGPULauncher *multi_gpu_launcher;
  // Timers
  Timer* timer_SpMV;
  Timer* timer_Prec;
};

#endif // SOLVER_CG_HH
