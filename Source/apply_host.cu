/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "apply_host.hh"

/* ********************************************************************* *
 * Matrix-free apply method y = A.x [host version]
 * ********************************************************************* */
void apply(const int nx,
           const int ny,
           const real* a_vert,
           const real* b_vert,
           const real* c_vert,
           const real* d_vert,
           const real* x,       // INPUT: x
           real* y) {           // OUTPUT: y
  int ix, iy, iz;
  DECLARE_DISCRETISATION_VARIABLES
  for (ix = 1; ix<=nx; ix++) {
    for (iy = 1; iy<=ny; iy++) {
      CALCULATE_ALPHA_TIJ(nx,ny,ix,iy)
      for (iz = 0; iz<NZ; iz++) {
        // Diagonal and vertical couplings
        real b_tmp = b_vert[iz];
        real c_tmp = c_vert[iz];
        real d_tmp = d_vert[iz];
        real tmp = ((a_vert[iz]-b_tmp-c_tmp)*Tij - alpha*d_tmp)
                 * x[LINIDX_Z(nx,ny,ix,iy,iz)];
        if (iz<NZ-1) {
          tmp+= b_tmp*Tij*x[LINIDX_Z(nx,ny,ix,iy,iz+1)];
        }
        if (iz>0) {
          tmp+= c_tmp*Tij*x[LINIDX_Z(nx,ny,ix,iy,iz-1)];
        }
        // Horizontal couplings
        tmp+= x[LINIDX_Z(nx,ny,ix-1,iy,iz)] * alpha_im1_j * d_tmp;
        tmp+= x[LINIDX_Z(nx,ny,ix+1,iy,iz)] * alpha_ip1_j * d_tmp;
        tmp+= x[LINIDX_Z(nx,ny,ix,iy-1,iz)] * alpha_i_jm1 * d_tmp;
        tmp+= x[LINIDX_Z(nx,ny,ix,iy+1,iz)] * alpha_i_jp1 * d_tmp;
        y[LINIDX_Z(nx,ny,ix,iy,iz)] = tmp;
      }
    }
  }
}

/* *********************************************************** *
 * Interleaved apply
 * *********************************************************** */
void ilvd_apply(const int nx,
                const int ny,
                const real* a_vert,
                const real* b_vert,
                const real* c_vert,
                const real* d_vert,
                const real* z,
                const real alpha_prime,
                const real beta,
                real* u,
                real* p,
                real* q,
                real* pq) {
  int ix, iy, iz;
  real tmp_pq=0.0;
  DECLARE_DISCRETISATION_VARIABLES
  for (ix=1;ix<=nx;++ix) {
    for (iy=1;iy<=ny;++iy) {
      CALCULATE_ALPHA_TIJ(nx,ny,ix,iy)
      for (iz=0;iz<NZ;++iz) {
        real tmp_p = p[LINIDX_Z(nx,ny,ix,iy,iz)];
        real tmp_q = q[LINIDX_Z(nx,ny,ix,iy,iz)];
        real tmp_z = z[LINIDX_Z(nx,ny,ix,iy,iz)];
        u[LINIDX_Z(nx,ny,ix,iy,iz)] += alpha_prime*tmp_p;
        tmp_p *= beta;
        tmp_p += tmp_z;
        p[LINIDX_Z(nx,ny,ix,iy,iz)] = tmp_p;
        // Diagonal and vertical couplings
        tmp_q *= beta;
        real b_tmp = b_vert[iz];
        real c_tmp = c_vert[iz];
        real d_tmp = d_vert[iz];
        real delta_q = ((a_vert[iz]-b_tmp-c_tmp)*Tij-alpha*d_tmp)*tmp_z;
        if (iz<NZ-1) {
          delta_q+= b_tmp*Tij*z[LINIDX_Z(nx,ny,ix,iy,iz+1)];
        }
        if (iz>0) {
          delta_q+= c_tmp*Tij*z[LINIDX_Z(nx,ny,ix,iy,iz-1)];
        }
        // Horizontal couplings
        delta_q+= z[LINIDX_Z(nx,ny,ix-1,iy,iz)] * alpha_im1_j*d_tmp;
        delta_q+= z[LINIDX_Z(nx,ny,ix+1,iy,iz)] * alpha_ip1_j*d_tmp;
        delta_q+= z[LINIDX_Z(nx,ny,ix,iy-1,iz)] * alpha_i_jm1*d_tmp;
        delta_q+= z[LINIDX_Z(nx,ny,ix,iy+1,iz)] * alpha_i_jp1*d_tmp;
        tmp_q += delta_q;
        tmp_pq += tmp_p*tmp_q;
        q[LINIDX_Z(nx,ny,ix,iy,iz)] = tmp_q;
      }
    }
  }
  *pq = tmp_pq;
}

