/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include"auxilliary.hh"

/* *********************************************************** *
 *  Set up processor layout
 * *********************************************************** */
void setup_processor_layout(int* pid,
                            int* nprocs,
                            int* nxyp,
                            int* xp,
                            int* yp,
                            MPI_Comm *cart_comm) {
  MPI_Comm_rank(MPI_COMM_WORLD, pid);
  MPI_Comm_size(MPI_COMM_WORLD, nprocs);
  if ((*nprocs) == 1) {
    (*nxyp) = 1;
    (*xp) = 0;
    (*yp) = 0;
  } else {
    // we expect nprocs be nxyp**2 !!!:
    (*nxyp)=1;
    while ((*nxyp)*(*nxyp)<(*nprocs)) {
      (*nxyp)++;
    }
    if ((*nxyp)*(*nxyp)!=(*nprocs)) {
      if (pid == 0) {
        printf("#### Error: number of processors must be a square of nxyp!!! ##\n");
      }
      exit(0);
    }
  }
  /* Set up processor layout and Cartesian geometry */
  int dims[3] = {(*nxyp),(*nxyp),1};
  if (pid == 0) {
    printf("processor layout = %d x %d x %d = %d\n",
           dims[0],dims[1],dims[2],nprocs);
  }
  int period[3] = {0, 0, 0};
  int coords[3];
  MPI_Cart_create(MPI_COMM_WORLD, 3, dims, period, false, cart_comm);
  MPI_Cart_get(*cart_comm, 3, dims, period, coords);
  (*xp)=coords[0];
  (*yp)=coords[1];
}

/* *********************************************************** *
 * Print out local vector (stored in LINIDX_Z ordering) on one processor
 * *********************************************************** */
void print_vec(const int NX,
               const int NY,
               int pid,
               int nprocs,
               real* x){
  int i,j,k,p;
  for (p=0;p<nprocs;p++){
    MPI_Barrier(MPI_COMM_WORLD);
    if (pid==p){
      printf(" ############################### pid %d (%d,%d):\n",pid,(pid&2)/2,pid&1);
      for(k=0;k<NZ;k++) {
        for(j=1-OL_Y;j<=NY+OL_Y;j++) {
          for(i=1-OL_X;i<=NX+OL_X;i++) {
            printf(" %5.3f",x[LINIDX_Z(NX,NY,i,j,k)]);
          } printf("\n");
        } printf("\n");
      } printf("\n");
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
}

/* *********************************************************** *
 * Read runtime parameters from file
 * *********************************************************** */
void read_parameters(const char* filename,
                     SolverType* solver_type,
                     int *nlevel,
                     int *ncoarsesmooth,
                     real *omega2,
                     real *lambda2,
                     real *resreduction,
                     int *maxiter,
                     int *verbose,
                     bool *compare_to_host,
                     bool *compare_to_exact) {
  FILE *file;
  float omega2_in, lambda2_in, resreduction_in;
  bool compare_to_host_in, compare_to_exact_in;
  int verbose_in,nlevel_in;
  int ncoarsesmooth_in;
  SolverType solver_type_in;
  int maxiter_in;
  int rank;
  char s[128];
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  if (rank == 0) {
    printf("Reading parameters from file \'%s\'",filename);
    char bool_s[128];
    char solver_s[128];
    file = fopen(filename,"r");
    fscanf(file,"%s = %s\n",&s, &solver_s);
    if (!strncmp(solver_s,"cg",2)) {
      solver_type_in = SolverTypeCG;
    } else if (!strncmp(solver_s,"multigrid",9)) {
      solver_type_in = SolverTypeMultigrid;
    } else {
      printf("Unknown solver type: %s\n",solver_s);
      MPI_Finalize();
      exit(1);
    }
    fscanf(file,"%s = %d\n",&s, &nlevel_in);
    fscanf(file,"%s = %d\n",&s, &ncoarsesmooth_in);
    fscanf(file,"%s = %e\n",&s, &omega2_in);
    fscanf(file,"%s = %e\n",&s, &lambda2_in);
    fscanf(file,"%s = %e\n",&s, &resreduction_in);
    fscanf(file,"%s = %d\n",&s, &maxiter_in);
    fscanf(file,"%s = %d\n",&s, &verbose_in);
    fscanf(file,"%s = %s\n",&s, &bool_s);
    if ( (!strncmp(bool_s,"t",1)) ||
         (!strncmp(bool_s,"T",1)) )
      compare_to_host_in=true;
    else
      compare_to_host_in=false;
    fscanf(file,"%s = %s\n",&s, &bool_s);
    if ( (!strncmp(bool_s,"t",1)) ||
         (!strncmp(bool_s,"T",1)) )
      compare_to_exact_in=true;
    else
      compare_to_exact_in=false;
    fclose(file);
  }
  MPI_Bcast(&solver_type_in,sizeof(SolverType),MPI_BYTE,0,MPI_COMM_WORLD);
  MPI_Bcast(&nlevel_in,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&ncoarsesmooth_in,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&omega2_in,1,MPI_FLOAT,0,MPI_COMM_WORLD);
  MPI_Bcast(&lambda2_in,1,MPI_FLOAT,0,MPI_COMM_WORLD);
  MPI_Bcast(&resreduction_in,1,MPI_FLOAT,0,MPI_COMM_WORLD);
  MPI_Bcast(&maxiter_in,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&verbose_in,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&compare_to_host_in,1,MPI_LOGICAL,0,MPI_COMM_WORLD);
  MPI_Bcast(&compare_to_exact_in,1,MPI_LOGICAL,0,MPI_COMM_WORLD);
  *solver_type = solver_type_in;
  *nlevel = nlevel_in;
  *ncoarsesmooth = ncoarsesmooth_in;
  *omega2 = omega2_in;
  *lambda2 = lambda2_in;
  *resreduction = resreduction_in;
  *maxiter = maxiter_in;
  *verbose = verbose_in;
  *compare_to_exact = compare_to_exact_in;
  *compare_to_host = compare_to_host_in;
  if (rank == 0) {
    printf(" parameters\n");
    printf(" ==========\n");
    printf(" solver           = ");
    if (*solver_type == SolverTypeCG) {
      printf("CG");
    } else if (*solver_type == SolverTypeMultigrid) {
      printf("Multigrid");
    } else {
      printf("UNKNOWN");
    }
  printf("\n");
    printf(" # of mg levels   = %d\n",*nlevel);
    printf(" n_coarsesmooth   = %d\n",*ncoarsesmooth);
    printf(" omega2           = %12.6e\n",*omega2);
    printf(" lambda2          = %12.6e\n",*lambda2);
    printf(" resreduction     = %12.6e\n",*resreduction);
    printf(" maxiter          = %d\n",*maxiter);
    printf(" verbose          = %d\n",*verbose);
    printf(" compare to host  = ");
    if (*compare_to_host)
      printf("true");
    else
      printf("false");
    printf("\n");
    printf(" compare to exact = ");
    if (*compare_to_exact)
      printf("true");
    else
      printf("false");
    printf("\n");
    printf("\n");
  }
}

/* *********************************************************** *
 * Transpose field from Z -> X
 * *********************************************************** */
void transposeZtoX(const int NX,
                   const int NY,
                   const real* x,
                   real* xT) {
  int ix;
  int iy;
  int iz;
  for (ix=1-OL_X;ix<=NX+OL_X;++ix)
    for (iy=1-OL_Y;iy<=NY+OL_Y;++iy)
      for (iz=0;iz<NZ;++iz)
        xT[LINIDX_X(NX,NY,ix,iy,iz)] = x[LINIDX_Z(NX,NY,ix,iy,iz)];
}

/* *********************************************************** *
 * Transpose field from X -> Z
 * *********************************************************** */
void transposeXtoZ(const int NX,
                   const int NY,
                   const real* xT,
                   real* x) {
  int ix;
  int iy;
  int iz;
  for (ix=1-OL_X;ix<=NX+OL_X;++ix)
    for (iy=1-OL_Y;iy<=NY+OL_Y;++iy)
      for (iz=0;iz<NZ;++iz)
        x[LINIDX_Z(NX,NY,ix,iy,iz)] = xT[LINIDX_X(NX,NY,ix,iy,iz)];
}

/* *********************************************************** *
 * Calculate L2-norm of y_l, y_seq and local difference y1-y2
 * *********************************************************** */
void dnorm_loc_glo(const int NX,
                   const int NY,
                   const int nx,
                   const int ny,
                   const int xp,
                   const int yp,
                   const real *y_l,
                   const real* y_seq,
                   real& norm_y1my2,
                   real& norm_y1,
                   real& norm_y2,
                   real& max_rel_diff) {
  int i,j,k;
  bool verbose=false;

  real norm_y1my2_loc = 0.0;
  real norm_y1_loc = 0.0;
  real norm_y2_loc = 0.0;

  real max_rel_diff_loc = 0;

  for(i=1;i<=NX;i++){
    for(j=1;j<=NY;j++){
      for(k=0;k<NZ;k++){
        real tmp_1 = y_l[LINIDX_Z(NX,NY,i,j,k)];
        real tmp_2 = y_seq[LINIDX_Z(nx,ny,xp*NX+i,yp*NY+j,k)];
        norm_y1my2_loc += (tmp_1-tmp_2)*(tmp_1-tmp_2);
        norm_y1_loc += tmp_1*tmp_1;
        norm_y2_loc += tmp_2*tmp_2;
        if ( fabs(tmp_1+tmp_2) > 0.0) {
          real rd = fabs((tmp_1-tmp_2)/(0.5*(tmp_1+tmp_2)));
          max_rel_diff_loc = ((max_rel_diff_loc<rd)?rd:max_rel_diff_loc);
          if ( (rd > 1.0) && verbose ) {
            printf("%d %d %d %12.4e %12.4e\n",i,j,k,tmp_1,tmp_2);
          }
        }
      }
    }
  }
  MPI_Allreduce(&norm_y1_loc,&norm_y1,1,MPI_real,
                MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&norm_y2_loc,&norm_y2,1,MPI_real,
                MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&norm_y1my2_loc,&norm_y1my2,1,MPI_real,
                MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&max_rel_diff_loc,&max_rel_diff,1,MPI_real,
                MPI_MAX,MPI_COMM_WORLD);
  norm_y1 = sqrt(norm_y1);
  norm_y2 = sqrt(norm_y2);
  norm_y1my2 = sqrt(norm_y1my2);
}

void dnorm_loc_loc(const int NX,
                   const int NY,
                   const real *y_1,
                   const real* y_2,
                   real& norm_y1my2,
                   real& norm_y1,
                   real& norm_y2,
                   real& max_rel_diff) {
  int i,j,k;
  bool verbose=false;

  real norm_y1my2_loc = 0.0;
  real norm_y1_loc = 0.0;
  real norm_y2_loc = 0.0;

  real max_rel_diff_loc = 0;

  for(i=1;i<=NX;i++){
    for(j=1;j<=NY;j++){
      for(k=0;k<NZ;k++){
        real tmp_1 = y_1[LINIDX_Z(NX,NY,i,j,k)];
        real tmp_2 = y_2[LINIDX_Z(NX,NY,i,j,k)];
        norm_y1my2_loc += (tmp_1-tmp_2)*(tmp_1-tmp_2);
        norm_y1_loc += tmp_1*tmp_1;
        norm_y2_loc += tmp_2*tmp_2;
        if ( fabs(tmp_1+tmp_2) > 0.0) {
          real rd = fabs((tmp_1-tmp_2)/(0.5*(tmp_1+tmp_2)));
          max_rel_diff_loc = ((max_rel_diff_loc<rd)?rd:max_rel_diff_loc);
          if ( (rd > 1.0) && verbose ) {
            printf("%d %d %d %12.4e %12.4e\n",i,j,k,tmp_1,tmp_2);
          }
        }
      }
    }
  }
  MPI_Allreduce(&norm_y1_loc,&norm_y1,1,MPI_real,
                MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&norm_y2_loc,&norm_y2,1,MPI_real,
                MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&norm_y1my2_loc,&norm_y1my2,1,MPI_real,
                MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&max_rel_diff_loc,&max_rel_diff,1,MPI_real,
                MPI_MAX,MPI_COMM_WORLD);
  norm_y1 = sqrt(norm_y1);
  norm_y2 = sqrt(norm_y2);
  norm_y1my2 = sqrt(norm_y1my2);
}

/* *********************************************************** *
 * Calculate L2 norm of a field y, which in each grid cell
 * is given as the average value of this field in the cell.
 * *********************************************************** */
real l2norm(const int nx,
            const int ny,
            const real *y) {
  real h;
  #ifdef CARTESIAN_GEOMETRY
    h = 1.0/nx;
  #else
    h = 2.0/nx;
    real xi1,xi2;
  #endif // CARTESIAN_GEOMETRY
  real volume;
  int ix, iy, iz;
  real tmp = 0.0;
  for (ix=1;ix<=nx;++ix) {
    for (iy=1;iy<=ny;++iy) {
      for (iz=0;iz<NZ;++iz) {
        #ifdef CARTESIAN_GEOMETRY
          volume = h*h*volume_V(iz);
        #else
          xi1 = ((ix)-0.5)*(h) - 1.0;
          xi2 = ((iy)-0.5)*(h) - 1.0;
          volume = pow(1.0+xi1*xi1+xi2*xi2,-1.5)*h*h*volume_V(iz);
        #endif // CARTESIAN_GEOMETRY
        tmp += y[LINIDX_Z(nx,ny,ix,iy,iz)]
             * y[LINIDX_Z(nx,ny,ix,iy,iz)]
             * volume;
      }
    }
  }
  return sqrt(tmp);
}

/* *********************************************************** *
 * Save distributed field to vtk file
 * *********************************************************** */
 void savetovtk(int NX,
                int NY,
                std::string filename,
                real* phi,
                bool includeHalos,
                bool devicePointer) {
  // MPI parameters
  int nprocs, pid, nxyp, xp, yp;
  MPI_Comm CartComm;
  // Setup processor layout
  setup_processor_layout(&pid,&nprocs,&nxyp,&xp,&yp,&CartComm);
  unsigned long nx=nxyp*NX;
  unsigned long ny=nxyp*NY;
  int halosize;
  if (includeHalos) {
    halosize = HALOSIZE;
  } else {
    halosize = 0;
  }
  std::stringstream s;
  s << filename << "_";
  s.fill('0');
  s.width(7);
  s << nprocs;
  s << "_";
  s.fill('0');
  s.width(7);
  s << pid << ".vtk";
  std::ofstream out;
  out.open(s.str().c_str(),std::ios::out);
  out << "# vtk DataFile Version 2.0" << std::endl;
  out << "Cell data" << std::endl;
  out << "ASCII" << std::endl;
  out << "DATASET RECTILINEAR_GRID" << std::endl;
  out << "DIMENSIONS ";
  out << (NX+2*halosize+1) << " ";
  out << (NY+2*halosize+1) << " ";
  out << (NZ+1) << std::endl;
  out << "X_COORDINATES " << (NX+2*halosize+1) << " double" << std::endl;
  for (int ix=0-halosize;ix<=NX+halosize;++ix) {
    double x = (ix+xp*NX)/double(nx);
    out << x << " ";
  }
  out << std::endl;
  out << "Y_COORDINATES " << (NY+2*halosize+1) << " double" << std::endl;
  for (int iy=0-halosize;iy<=NY+halosize;++iy) {
    double y = (iy+yp*NY)/double(ny);
    out << y << " ";
  }
  out << std::endl;
  out << "Z_COORDINATES " << (NZ+1) << " double" << std::endl;
  for (int iz=0;iz<=NZ;++iz) {
    double z_tmp = iz/double(NZ);
    double z = z_tmp*z_tmp;
    out << z << " ";
  }
  out << std::endl;
  out << std::endl;
  unsigned long nlocal = (NX+2*halosize)*(NY+2*halosize)*NZ;
  out << "CELL_DATA " << nlocal << std::endl;
  out << "SCALARS field double 1" << std::endl;
  out << "LOOKUP_TABLE default" << std::endl;
  if (devicePointer) {
    real* phi_tmp = (real*) malloc(nlocal*sizeof(real));
    cuda_safeCall(cudaMemcpy(phi_tmp,phi,nlocal*sizeof(real),cudaMemcpyDeviceToHost));
    for (int iz=0;iz<NZ;++iz) {
      for (int iy=1-halosize;iy<=NY+halosize;++iy) {
        for (int ix=1-halosize;ix<=NX+halosize;++ix) {
          out << double(phi_tmp[LINIDX_X(NX,NY,ix,iy,iz)]) << std::endl;
        }
      }
    }
    free(phi_tmp);
  } else {
    for (int iz=0;iz<NZ;++iz) {
      for (int iy=1-halosize;iy<=NY+halosize;++iy) {
        for (int ix=1-halosize;ix<=NX+halosize;++ix) {
          out << double(phi[LINIDX_Z(NX,NY,ix,iy,iz)]) << std::endl;
        }
      }
    }
  }
  out.close();
}
