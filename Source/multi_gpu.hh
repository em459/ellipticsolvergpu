/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef MULTIGPU_HH
#define MULTIGPU_HH MULTIGPU_HH
/* *********************************************************** *
 * Routines for multi-GPU functionality
 * *********************************************************** */
#include<stdlib.h>
#include<stdio.h>
#include<cuda.h>
#include<mpi.h>
#include<assert.h>
#include <halo_exchange.h>
#include <utils/layout_map.h>
#include <GCL.h>
#include "kernel.hh"
#include "definitions.hh"
#include "auxilliary.hh"
#include "cuda_errorcheck.hh"

/* ********************************************************************* *
 * Use MPI environment variable to bind to correct device.
 * ********************************************************************* */
void device_binding();

// Structure describing the layout of threads in one subdomain
  typedef struct subDomainLayout {
    int NX;
    int NY;
    int offset_x;
    int offset_y;
    dim3 dimBlock;
    dim3 dimGrid;
  } subDomainLayout;

/* ********************************************************************* *
 * Class describing the layout of threads in all five subdomains
 * of the interior
 * ********************************************************************* */
  class DomainLayout {
   public:
    // Constructors
    DomainLayout(const int NX,
                 const int NX_center,
                 const int NY_center,
                 const int BLOCKSIZE_X_ALL,
                 const int BLOCKSIZE_Y_ALL,
                 const int BLOCKSIZE_X_CENTER,
                 const int BLOCKSIZE_Y_CENTER,
                 const int BLOCKSIZE_X_BOUND_EW,
                 const int BLOCKSIZE_Y_BOUND_EW,
                 const int BLOCKSIZE_X_BOUND_NS,
                 const int BLOCKSIZE_Y_BOUND_NS,
                 const bool overlap_comms_,
                 const bool verbose_=true);
    DomainLayout(const char* filename,
                 const bool verbose_=true);
    bool overlap_comms;
    subDomainLayout all;
    subDomainLayout center;
    subDomainLayout boundary[4];
   private:
    bool verbose;
    void setup_all(const int NX,
                   const int NX_center,
                   const int NY_center,
                   const int BLOCKSIZE_X_ALL,
                   const int BLOCKSIZE_Y_ALL,
                   const int BLOCKSIZE_X_CENTER,
                   const int BLOCKSIZE_Y_CENTER,
                   const int BLOCKSIZE_X_BOUND_EW,
                   const int BLOCKSIZE_Y_BOUND_EW,
                   const int BLOCKSIZE_X_BOUND_NS,
                   const int BLOCKSIZE_Y_BOUND_NS,
                   const bool overlap_comms_);
    void setup_subdomain(const int NX,
                         const int NY,
                         const int block_size_x,
                         const int block_size_y,
                         const int offset_x,
                         const int offset_y,
                         const char label,
                         subDomainLayout& subdomain);
  };


/* ********************************************************************* *
 * Class for multi GPU kernel launcher with a specific halo echange pattern
 * ********************************************************************* */
class MultiGPULauncher {
 public:
  // Constructor
  MultiGPULauncher(const int nx_,
                   const int ny_,
                   const DomainLayout& threadlayout_,
                   const int nhalo_max_=1) :
   nx(nx_), ny(ny_), threadlayout(threadlayout_), nhalo_max(nhalo_max_) {
    MPI_Comm cart_comm;
    setup_processor_layout(&pid,&nprocs,&nxyp,&xp,&yp,&cart_comm);
    NX=threadlayout.all.NX;
    NY=threadlayout.all.NY;
    he = new pattern_type(period_type(false,false,false), cart_comm);
    he->add_halo<0>(HALOSIZE, HALOSIZE, OL_X, NX+OL_X-1, NX+2*OL_X);
    he->add_halo<1>(HALOSIZE, HALOSIZE, OL_Y, NY+OL_Y-1, NY+2*OL_Y);
    he->add_halo<2>(0, 0, 0, NZ-1, NZ);
    he->setup(nhalo_max);
    for(int i=0; i<4; i++)
      cudaStreamCreate(&stream[i]);
  }

  // Destructor
  ~MultiGPULauncher() {
    for(int i=0; i<4; i++)
      cudaStreamDestroy(stream[i]);
    delete he;
  }

  // Invoke a specific kernel
template <class Kernel>
inline void invoke(Kernel& kernel) {
  // Extract fields that need to be halo exchanged from kernel
  std::vector<real*> x_vect = kernel.get_halofields();
  int nhalo = x_vect.size();
  assert(nhalo<=nhalo_max);
  if ( (nprocs > 1) && (nhalo > 0) && (threadlayout.overlap_comms) ) {
    // ------ do the work on boundary dofs
    for (int i=0;i<4;++i) {
      kernel.invoke(threadlayout.boundary[i].dimGrid,
                    threadlayout.boundary[i].dimBlock,
                    stream[i],
                    NX,NY,nx,ny,
                    xp,yp,
                    threadlayout.boundary[i].offset_x,
                    threadlayout.boundary[i].offset_y);
    }
    he->pack(x_vect);
    he->start_exchange();
    // ------ do the work on central dofs
    kernel.invoke(threadlayout.center.dimGrid,
                  threadlayout.center.dimBlock,
                  stream[0],
                  NX,NY,nx,ny,
                  xp,yp,
                  threadlayout.center.offset_x,
                  threadlayout.center.offset_y);
    he->wait();
    he->unpack(x_vect);
  } else {
    // ------ do the work on all dofs
    kernel.invoke(threadlayout.all.dimGrid,
                  threadlayout.all.dimBlock,
                  stream[0],
                  NX,NY,nx,ny,
                  xp,yp,
                  threadlayout.all.offset_x,
                  threadlayout.all.offset_y);
    // Only halo exchange if actually needed
    if ( (nhalo > 0) && (nprocs > 1) ) {
      he->pack(x_vect);
      he->exchange();
      he->unpack(x_vect);
    }
  }
  cudaDeviceSynchronize();
}

// Halo exchange a field
void halo_exchange(real* phi) {
  std::vector<real*> x_vect(1);
  x_vect[0] = phi;
  he->pack(x_vect);
  he->exchange();
  he->unpack(x_vect);
  cudaDeviceSynchronize();
}

 private:
  // GCL data types
  typedef GCL::halo_exchange_dynamic_ut<GCL::layout_map<0,2,1>,
                                        GCL::layout_map<0,1,2>,
                                        real,
                                        3,
                                        GCL::gcl_gpu,
                                        GCL::version_manual>
    pattern_type;
  typedef typename pattern_type::grid_type::period_type period_type;
  pattern_type* he;
  // Thread layout in the domain
  const DomainLayout& threadlayout;
  // Problem size
  int NX;
  int NY;
  int nx;
  int ny;
  // Maximal number of halos
  int nhalo_max;
  // Processor layout specific variables
  int nprocs;
  int pid;
  int xp;
  int yp;
  int nxyp;
  cudaStream_t stream[4];
};

#endif // MULTIGPU_HH
