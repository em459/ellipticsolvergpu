/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef PCG_HOST_HH
#define PCG_HOST_HH PCG_HOST_HH
/* *********************************************************** *
 * Host implementation of the Preconditioned CG algorithm
 * for solving the system A.x = b.
 * *********************************************************** */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "definitions.hh"
#include "blas_wrapper.hh"
#include "apply_host.hh"
#include "prec_host.hh"
#include "auxilliary.hh"

/* *********************************************************** *
 * Preconditioned CG solver, solve A.x = b for x
 *
 * Note that the field b has to be given as integrals over the
 * individual cells (i.e. might have to be scaled appropriately with
 * volscale before calling the solver), whereas the field x is
 * returned as cell averages.
 * *********************************************************** */
void pcg_solve(const int nx,         // } problem size
               const int ny,         // }
               const real omega2,    // Model parameter omega2
               const real lambda2,   // Model parameter lambda2
               real resreduction,    // Desired relative residual reduction
               unsigned int maxiter, // Maximal number of iterations
               int verbose,          // Verbosity level, can be 0, 1 or 2
               real* b,              // INPUT: Right hand side b
    	         real* x);             // OUTPUT: Solution x

#endif // PCG_HOST_HH
