/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef FUNCTIONS_HH
#define FUNCTIONS_HH FUNCTIONS_HH
#include "definitions.hh"
#include <iostream>

/* ********************************************************************* *
 * Template function
 * ********************************************************************* */
class Function {
 public:
  // Constructor
  Function() {}
  // Evaluation
  inline real operator()(const real x,
                         const real y,
                         const real z) const { return 0; }
};

/* ********************************************************************* *
 * Random
 * ********************************************************************* */
class FunctionRandom {
 public:
  // Constructor
  FunctionRandom() { srand(1); }
  // Evaluation
  inline real operator()(const real x,
                         const real y,
                         const real z) const {
    return 0.00001*(rand() % 100000);
  }
};

/* ********************************************************************* *
 * Zero function
 * ********************************************************************* */
class FunctionZero {
 public:
  // Constructor
  FunctionZero() { srand(1); }
  // Evaluation
  inline real operator()(const real x,
                         const real y,
                         const real z) const
    { return 0.0; }
};

/* ********************************************************************* *
 * Function that isnonzero only in a certain region
 * ********************************************************************* */
class FunctionIndicator {
 public:
  // Constructor
  FunctionIndicator(const real x_min_ = -0.25,
                    const real x_max_ = +0.25,
                    const real y_min_ = -0.25,
                    const real y_max_ = +0.25,
                    const real z_min_ =  0.25*H_ATMOS,
                    const real z_max_ =  0.75*H_ATMOS) :
    x_min(x_min_),
    x_max(x_max_),
    y_min(y_min_),
    y_max(y_max_),
    z_min(z_min_),
    z_max(z_max_) {}
  // Evaluation
  inline real operator()(const real x,
                         const real y,
                         const real z) const {
    if ( (x > x_min) && (x < x_max) &&
         (y > y_min) && (y < y_max) &&
         (z > z_min) && (z < z_max) ) {
      return 1.0;
    } else {
      return 0.0;
    }
  }
 private:
  real x_min;
  real x_max;
  real y_min;
  real y_max;
  real z_min;
  real z_max;
};


/* ********************************************************************* *
 * Exact solution function
 * ********************************************************************* */
class FunctionExact {
 public:
  // Constructor
  FunctionExact(const real sigma_) : sigma(sigma_) {}
  // Evaluation
  inline real operator()(const real x,
                         const real y,
                         const real z) const {
    real r2 = x*x+y*y;
    return (x-1.0)*(x+1.0)*(y-1.0)*(y+1.0)*exp(-0.5*r2/(sigma*sigma));
  }
 private:
  real sigma;
};

#endif // FUNCTIONS_HH
