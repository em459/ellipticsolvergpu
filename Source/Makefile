### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the EllipticSolverGPU code.
#  
#  (c) The copyright relating to this work is owned jointly 
#  by the Crown, Met Office and NERC [2014]. However, it
#  has been created with the help of the GungHo Consortium,
#  whose members are identified at
#  https://puma.nerc.ac.uk/trac/GungHo/wiki
#  
#  Main Developer: Eike Mueller, University of Bath
#  
#  Contributors:
#    Eero Vainikko (University of Tartu)
#    Sinan Shi (EPCC Edinburgh)
#  
#  EllipticSolverGPU is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  EllipticSolverGPU is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
#  main directory).  If not, see <http://www.gnu.org/licenses/>.
#  
#  The EllipticSolverGPU code uses (i.e. links to and includes header files
#  from) the Generic Communication Library (GCL). The GCL has been developed
#  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
#  CENTRE (CSCS) and it is distributed under the Copyright statement in the
#  file COPYING.GCL.
#
### COPYRIGHT AND LICENSE STATEMENT ###



################################################################
# Makefile for PCG CUDA code
################################################################

# Include user specific options

include Makefile.options

################################################################
# DO NOT EDIT BELOW THIS LINE
################################################################

# Main programs
MAINS= \
	main_apply_prec-CUDAC.x \
	main_solver-CUDAC.x

# C/CUDA compiler
NVCC=nvcc
CC=CC
# Compiler flags
CFLAGS=
# additional compiler flags for nvcc
NVCFLAGS= #-Xptxas -dlcm=cg
COMPUTE_CAPABILITY=sm_20
# Linker flags
LFLAGS=
# additional linker flags for nvcc
NVLFLAGS= -lm -lcublas#-Xptxas -dlcm=cg #-O3 -Wall -pg -lm

ifeq ($(CUDA_ERROR_CHECK),True)
  CFLAGS:=$(CFLAGS) -DCUDA_ERROR_CHECK
endif

ifeq ($(DOUBLE_PRECISION),True)
	CFLAGS:=$(CFLAGS) -DDOUBLE__PRECISION
endif

ifeq ($(CARTESIAN_GEOMETRY),True)
	CFLAGS:=$(CFLAGS) -DCARTESIAN_GEOMETRY
endif

ifeq ($(Z_GRADED),True)
	CFLAGS:=$(CFLAGS) -DZ_GRADED
endif

ifeq ($(SAVE_FIELDS),True)
	CFLAGS:=$(CFLAGS) -DSAVE_FIELDS
endif

# Compiler mode
ifeq ($(MODE),Debugging)
	CFLAGS:=$(CFLAGS) -O0 -g -G -DCUDA_ERROR_CHECK
endif

ifeq ($(MODE),Optimized)
	CFLAGS:=$(CFLAGS)  -O3 -DNDEBUG
endif

ifeq ($(HAVE_BLAS),True)
	CFLAGS:=$(CFLAGS) -DHAVE_BLAS
endif

ifeq ($(USE_TIMERS),True)
	CFLAGS:=$(CFLAGS) -DUSE_TIMERS
endif

# Host specific settings
HOSTNAME=$(shell hostname)
# --- HECToR GPU testbed ---
ifeq ($(HOSTNAME),gpu.hector.ac.uk)
	ifeq ($(HAVE_BLAS),True)
		CFLAGS:=$(CFLAGS) -DATLAS_BLAS -I/usr/local/packages/gnag/ATLAS/ATLAS_3.8.3/include
		LFLAGS:=$(LFLAGS) -L/usr/local/packages/gnag/ATLAS/ATLAS_3.8.3/lib/ -lcblas -latlas
	endif
endif

# --- aquila ---
ifeq ($(findstring gnode,$(HOSTNAME)),gnode)
	CC=mpicxx
	GCL_DIR=/data/em459/Library/GCL/
	ifeq ($(HAVE_BLAS),True)
		ifeq ($(BLAS_IMPLEMENTATION),OPEN)
			CFLAGS:=$(CFLAGS) -DOPEN_BLAS -I/apps/openblas/0.2.5/gcc-4.7.2/include/
			LFLAGS:=$(LFLAGS) -L/apps/openblas/0.2.5/gcc-4.7.2/lib -L/apps/cblas/sandybridge/20130128/openblas/lib64/ -lopenblas -lcblas
		endif
		ifeq ($(BLAS_IMPLEMENTATION),GOTO)
			CFLAGS:=$(CFLAGS) -DGOTO_BLAS -I/apps/cblas/gcc-4.3.4/goto/include/
			LFLAGS:=$(LFLAGS) -L/apps/cblas/gcc-4.3.4/goto/lib64/ -L/apps/gotoblas/penryn/64/1.26 -lcblas -lgoto
		endif
		ifeq ($(BLAS_IMPLEMENTATION),GOTO2)
			CFLAGS:=$(CFLAGS) -DGOTO_BLAS2 -I/apps/cblas/gcc-4.3.4/goto2/include/
			LFLAGS:=$(LFLAGS) -L/apps/cblas/gcc-4.3.4/goto2/lib64/ -L/apps/gotoblas2/penryn/64/gcc/1.13/ -lcblas -lgoto2
		endif
		ifeq ($(BLAS_IMPLEMENTATION),ATLAS)
			CFLAGS:=$(CFLAGS) -DATLAS_BLAS -I/cm/shared/apps/atlas/3.9.23/include/
			LFLAGS:=$(LFLAGS) -L/cm/shared/apps/atlas/3.9.23/lib/ -lcblas -latlas
		endif
	endif
	ifeq ($(MULTIGPU_GCL),True)
		NVCFLAGS:=$(NVCFLAGS) -D_GCL_GPU_ -D_GCL_MPI_ -D__CUDACC__ -ccbin /apps/openmpi/gcc/64/1.6.2/bin/mpicc -m64 -Xcompiler -DNVCC -I/apps/nvidia/cuda/5.0.35/include -I/data/em459/Library/Boost_1.53/include -I/apps/nvidia/cuda/5.0.35/include -I${GCL_DIR}/L3/include -I${GCL_DIR}/L3/src -I${GCL_DIR}/L2/include -I${GCL_DIR}/L2/src -I${GCL_DIR}.  -I/apps/openmpi/gcc/64/1.6.2/include
		LFLAGS:=$(LFLAGS)  -L${GCL_DIR}/lib -lgcl
        endif
	# additional linker flags for nvcc
	NVLFLAGS= -L/apps/nvidia/cuda/5.0.35/lib64 -lcublas
endif

# --- balena ---
ifeq ($(findstring balena,$(HOSTNAME)),balena)
	CC=mpicxx
	GCL_DIR=/beegfs/scratch/user/em459/svn_workspace/gcl/
	BOOST_DIR=/apps/boost/gcc/1.57.0/
	CUDA_DIR=/cm/shared/apps/cuda65/toolkit/6.5.14
	MPI_DIR=/apps/openmpi/gcc-4.8/1.8.4
	ifeq ($(MULTIGPU_GCL),True)
		NVCFLAGS:=$(NVCFLAGS) -ccbin /usr/bin/cc -m64 -Xcompiler ,\"-D_GCL_GPU_\",\"-D_GCL_MPI_\",\"-O3\",\"-DNDEBUG\" -DNVCC -I${CUDA_DIR}/include -I${BOOST_DIR}/include -I${CUDA_DIR}/include -I${GCL_DIR}/L3/include -I${GCL_DIR}/L3/src -I${GCL_DIR}/L2/include -I${GCL_DIR}/L2/src -I${GCL_DIR}/. -I${MPI_DIR}/include
		# additional linker flags for nvcc
		NVLFLAGS:=$(NVLFLAGS) -L${GCL_DIR}/lib -rdynamic ${CUDA_DIR}/lib64/libcudart.so -lgcl -Wl,-rpath,${CUDA_DIR}/lib64 -L${CUDA_DIR}/lib64/
  endif
endif

# --- EMERALD ---
ifeq ($(findstring gpu.rl.ac.uk,$(HOSTNAME)),gpu.rl.ac.uk)
	CC=mpicxx
	ifeq ($(HAVE_BLAS),True)
		ifeq ($(BLAS_IMPLEMENTATION),ATLAS)
			CFLAGS:=$(CFLAGS) -DATLAS_BLAS -I/cm/shared/apps/atlas/3.9.23/include/ -I/usr/include/gsl/
			LFLAGS:=$(LFLAGS) -L/usr/lib64/atlas/ -lcblas -latlas
		endif
	endif
	ifeq ($(MULTIGPU_GCL),True)
		NVCFLAGS:=$(NVCFLAGS) -ccbin /usr/bin/cc -m64 -Xcompiler ,\"-D_GCL_GPU_\",\"-D_GCL_MPI_\",\"-O3\",\"-DNDEBUG\" -DNVCC -I//apps/cuda/5.0.35/cuda/include -I/apps/boost/1.53/include -I//apps/cuda/5.0.35/cuda/include -I//apps/src/libs/gcl/trunk/L3/include -I//apps/src/libs/gcl/trunk/L3/src -I//apps/src/libs/gcl/trunk/L2/include -I//apps/src/libs/gcl/trunk/L2/src -I//apps/src/libs/gcl/trunk/. -I/apps/contrib/mvapich2/1.9a2-gnu/include
		# additional linker flags for nvcc
		NVLFLAGS:=$(NVLFLAGS) -D_GCL_GPU_ -D_GCL_MPI_ -O3 -DNDEBUG -rdynamic //apps/cuda/5.0.35/cuda/lib64/libcudart.so -lgcl -Wl,-rpath,//apps/cuda/5.0.35/cuda/lib64 -L/apps/nvidia/5.0.35/cuda/lib64 -L/apps/libs/gcl/revision_296/lib -L/apps/cuda/5.0.35/cuda/lib64 /apps/cuda/5.0.35/cuda/lib64/libcublas.so
  endif
endif

# --- stampede ---
ifeq ($(findstring stampede,$(HOSTNAME)),stampede)
  COMPUTE_CAPABILITY=sm_35
  CC=mpicxx
	ifeq ($(HAVE_BLAS),True)
		LFAGS:=$(LFLAGS) -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm
		CFLAGS:=$(CFLAGS) # $(MKLROOT)/include
	endif

        ifeq ($(MULTIGPU_GCL),True)
             NVCFLAGS:=$(NVCFLAGS) -D_GCL_GPU_ -D_GCL_MPI_ -I/opt/apps/intel13/boost/1.51.0/include -I/opt/apps/cuda/5.5/include -I/home1/01635/bmuite/GCL/trunk/L3/include -I/home1/01635/bmuite/GCL/trunk/L3/src -I/home1/01635/bmuite/GCL/trunk/L2/include -I/home1/01635/bmuite/GCL/trunk/L2/src -I/home1/01635/bmuite/GCL/trunk/. -I/opt/apps/intel13/mvapich2/1.9/include
             # additional linker flags for nvcc
             NVLFLAGS:= -lm -D_GCL_GPU_ -D_GCL_MPI_  -rdynamic /home1/01635/bmuite/GCL/build/lib/libgcl.a /opt/apps/cuda/5.5/lib64/libcudart.so -lcuda /opt/apps/cuda/5.5/lib64/libcublas.so -Wl,-rpath,/opt/apps/cuda/5.5/lib64
        endif
endif
# --- Titan ---
ifeq ($(findstring titan,$(HOSTNAME)),titan)
	COMPUTE_CAPABILITY=sm_35
	CC=CC
  GCL_DIR=/lustre/widow/proj/csc113/GungHoMuite/GCL/
        ifeq ($(MULTIGPU_GCL),True)
                NVCFLAGS:=$(NVCFLAGS)  -m64 -Xcompiler ,\"-D_GCL_GPU_\",\"-D_GCL_MPI_\",\"-g\",\"-DNDEBUG\" -DNVCC -I/opt/nvidia/cudatoolkit/5.0.35.102/include -I/sw/xk6/boost/1.53.0/cle4.0_gnu4.7.2/include -I/opt/nvidia/cudatoolkit/5.0.35.102/include -I${GCL_DIR}/. -I${GCL_DIR}/L3/include -I${GCL_DIR}/L3/src -I${GCL_DIR}/L2/include -I${GCL_DIR}/L2/src -I${GCL_DIR}/build/lib -I/opt/cray/mpt/5.6.3/gni/mpich2-gnu/47/include

		 # additional linker flags for nvcc
                NVLFLAGS:=$(NVLFLAGS) -D_GCL_GPU_ -D_GCL_MPI_ -O3 -DNDEBUG -I/opt/nvidia/cudatoolkit/5.0.35.102/include -I/sw/xk6/boost/1.53.0/cle4.0_gnu4.7.2/include -I/opt/nvidia/cudatoolkit/5.0.35.102/include -I${GCL_DIR}/L3/include -I${GCL_DIR}/L3/src -I${GCL_DIR}/L2/include -I${GCL_DIR}/L2/src -I/opt/cray/mpt/5.6.3/gni/mpich2-gnu/47/include -I/opt/cray/mpt/5.6.3/gni/mpich2-gnu/47/include  -I/opt/nvidia/cudatoolkit/5.0.35.102/include -I/opt/nvidia/cudatoolkit/5.0.35.102/extras/CUPTI/include -I/opt/nvidia/cudatoolkit/5.0.35.102/extras/Debugger/include -L${GCL_DIR}/lib -L/opt/nvidia/cudatoolkit/5.0.35.102/lib64 -L/opt/nvidia/cudatoolkit/5.0.35.102/extras/CUPTI/lib64 -Wl,--as-needed -Wl,-lcupti -Wl,-lcudart -Wl,--no-as-needed -lcublas -lgcl
	endif
endif

# --- Juur ---
ifeq ($(findstring juur,$(HOSTNAME)),juur)
      COMPUTE_CAPABILITY=sm_35
      CC=mpicxx
      GCL_DIR=/home/hpc_benson/software/GCL
      GCL_BUILD_DIR=/home/hpc_benson/software/GCLbuild
      ifeq ($(MULTIGPU_GCL),True)
           NVCFLAGS:=$(NVCFLAGS) -D_GCL_GPU_ -D_GCL_MPI_ -g -DNDEBUG  -DNVCC -I${MPI_INCLUDE}  -I${GCL_DIR} -I${GCL_DIR}/L3/include -I${GCL_DIR}/L3/src -I${GCL_DIR}/L2/include -I${GCL_DIR}/L2/src -I${GCL_DIR}/build/lib  -I/opt/cuda-5.5/include -I/opt/boost-1.55/include -I/opt/cuda-5.5/include -I/home/hpc_benson/software/GCL/L3/include -I/home/hpc_benson/software/GCL/L3/src -I/home/hpc_benson/software/GCL/L2/include -I/home/hpc_benson/software/GCL/L2/src -I/home/hpc_benson/software/GCL/. -I/usr/include/openmpi-x86_64

           # additional linker flags for nvcc
           NVLFLAGS:=$(NVLFLAGS) -D_GCL_GPU_ -D_GCL_MPI_ -O3 -DNDEBUG -rdynamic -I${MPI_INCLUDE} -I${GCL_DIR}/L3/include -I${GCL_DIR}/L3/src -I${GCL_DIR}/L2/include -I${GCL_DIR}/L2/src -I${GCL_DIR}/build/lib  -I/opt/cuda-5.5/include -I/opt/boost-1.55/include -I/opt/cuda-5.5/include -I/home/hpc_benson/software/GCL/L3/include -I/home/hpc_benson/software/GCL/L3/src -I/home/hpc_benson/software/GCL/L2/include -I/home/hpc_benson/software/GCL/L2/src -I/home/hpc_benson/software/GCL/. -I/usr/include/openmpi-x86_64  -L/opt/cuda-5.5/lib64 -L${GCL_BUILD_DIR}/lib -lgcl -lcublas -lcudart
      endif

endif

# Set compute capability
NVCFLAGS:=$(NVCFLAGS)   -arch $(COMPUTE_CAPABILITY)  -I${MPI_INCLUDE}

# Dependencies
SOURCES=$(wildcard *.cu) $(wildcard *.cc)
HEADERS=$(wildcard *.hh)
DEPENDENCIES=make.dependencies

all: $(MAINS)

include $(DEPENDENCIES)

$(DEPENDENCIES): $(SOURCES) $(HEADERS)
	python makedepend.py $(DEPENDENCIES)

# ------------------------------------------
# Test programs
# ------------------------------------------
#test_cblas.x: test_cblas.cu blas_wrapper.cu
#	$(CC) -o $@ test_cblas.cu blas_wrapper.cu $(CFLAGS) $(LFLAGS)

# ------------------------------------------
# Main executables
# ------------------------------------------
# CUDA-C version
%-CUDAC.x: %-CUDAC.o $(OBJS$@)
		$(CC) -o $@ $(OBJS$@) $< $(LFLAGS) $(NVLFLAGS)

# plain C version
%-C.x: %-C.o $(OBJS$@)
		$(CC) -o $@ $(OBJS$@) $< $(LFLAGS)

# Dependencies for .cu and .o files
%.o: %.cc %.hh
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.cc
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.cu %.hh
	$(NVCC) -c $(CFLAGS) $(NVCFLAGS) -o $@ $<

%.o: %.cu
	$(NVCC) -c $(CFLAGS) $(NVCFLAGS) -o $@ $<

# Install programs
install:
	for MAIN in $(MAINS); do if [ -e $$MAIN ]; then cp $$MAIN $(INSTALL_DIR); fi; done

# Tidy up
clean:
	-rm -rf $(MAINS)
	-rm -rf *.o
	-rm -rf *~
	-rm -rf *.out
	-rm -rf *.preprocessed.cu
	-rm -rf $(DEPENDENCIES)
