/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "solver_multigrid.hh"

/* ********************************************************************* *
 * Constructor
 * ********************************************************************* */
SolverMultigrid::SolverMultigrid(DomainLayout *threadlayout_,
                                 const int Nlevel_,
                                 const int n_coarsesmooth_,
                                 const bool testConvergence_,
                                 const double rho_relax_,
                                 const real  omega2_,
                                 const real lambda2_,
                                 const int maxiter_,
                                 const real tolerance_,
                                 const int verbose_) :

    Solver(threadlayout_,
           omega2_,
           lambda2_,
           maxiter_,
           tolerance_,
           verbose_),
    Nlevel(Nlevel_),
    n_coarsesmooth(n_coarsesmooth_),
    n_presmooth(1),
    n_postsmooth(1),
    n_presmooth_fine(1),
    n_postsmooth_fine(1),
    rho_relax(rho_relax_),
    testConvergence(testConvergence_) {
  // Only print out on processor 0
  if (verbose > 0) {
    std::cout << " Initialising multigrid solver " << std::endl;
    std::cout << "   Nlevel              = " << Nlevel     << std::endl;
    std::cout << "   maxiter             = " << maxiter    << std::endl;
    std::cout << "   tolerance           = " << tolerance  << std::endl;
    std::cout << "   rho_relax           = " << rho_relax  << std::endl;
    std::cout << "   omega2              = " << omega2     << std::endl;
    std::cout << "   lambda2             = " << lambda2    << std::endl;
    std::cout << "   n_presmooth         = " << n_presmooth << std::endl;
    std::cout << "   n_postsmooth        = " << n_postsmooth << std::endl;
    std::cout << "   n_presmooth [fine]  = " << n_presmooth_fine << std::endl;
    std::cout << "   n_postsmooth [fine] = " << n_postsmooth_fine << std::endl;
    std::cout << "   n_coarsesmooth      = " << n_coarsesmooth << std::endl;
    std::cout << std::endl;
    std::cout << "   Grid sizes (LOCAL / global): " << std::endl;
  }
  NX_fine = threadlayout->all.NX;
  NY_fine = threadlayout->all.NY;
  int nx_fine, ny_fine;
  if (NX_fine != NY_fine) {
    if (pid == 0) {
      std::cout << " ERROR: NX_fine has to be equal to NY_fine." << std::endl;
      std::cout << "    NX_fine = " << NX_fine;
      std::cout << "    NY_fine = " << NY_fine << std::endl;
    }
    exit(1);
  }
  if ( ((NX_fine>>(Nlevel-1))<<(Nlevel-1)) != NX_fine) {
    if (pid == 0) {
      std::cout << " ERROR: NX_fine has to be a multiple of 2^{Nlevel-1}" << std::endl;
      std::cout << "    NX_fine = " << NX_fine << " Nlevel = " << Nlevel << std::endl;
    }
    exit(1);
  }
  // Calculate local and global problem sizes on
  // each level and set up storage for fields
  nx_fine = NX_fine*nxyp;
  ny_fine = NY_fine*nxyp;
  nx.resize(Nlevel);
  ny.resize(Nlevel);
  NX.resize(Nlevel);
  NY.resize(Nlevel);
  dev_u_mg.resize(Nlevel);
  dev_b_mg.resize(Nlevel);
  dev_r_mg.resize(Nlevel);
  dev_c_mg.resize(Nlevel);
  timer_Smooth.resize(Nlevel);
  timer_Residual.resize(Nlevel);
  timer_ResSmooth.resize(Nlevel);
  timer_Prolongate.resize(Nlevel);
  threadlayout_mg.resize(Nlevel);
  double gridComplexity=0;
  for (int ell=Nlevel-1;ell>=0;--ell) {
    nx[ell] = nx_fine >> (Nlevel-1-ell);
    ny[ell] = ny_fine >> (Nlevel-1-ell);
    NX[ell] = NX_fine >> (Nlevel-1-ell);
    NY[ell] = NY_fine >> (Nlevel-1-ell);
    unsigned long nlocal = (NX[ell]+2*OL_X)*(NY[ell]+2*OL_Y)*NZ;
    unsigned long nglobal = nx[ell]*ny[ell]*NZ;
    if (verbose > 0) {
      std::cout << "     Level " << ell;
      std::cout << " NX x NY = " << NX[ell] << " x " << NY[ell];
      std::cout << " = " << NX[ell]*NY[ell];
      std::cout << " nx x ny = " << nx[ell] << " x " << ny[ell];
      std::cout << " = " << nx[ell]*ny[ell];
      std::cout << std::endl;
    }
    gridComplexity += nx[ell]*ny[ell];
    cuda_safeCall(cudaMalloc((void**)&dev_u_mg[ell],nlocal*sizeof(real)));
    cuda_safeCall(cudaMalloc((void**)&dev_b_mg[ell],nlocal*sizeof(real)));
    cuda_safeCall(cudaMalloc((void**)&dev_r_mg[ell],nlocal*sizeof(real)));
    cuda_safeCall(cudaMalloc((void**)&dev_c_mg[ell],nlocal*sizeof(real)));
    /* IMPORTANT: Initialise u,b and r to zero */
    real* phiZero = (real*) malloc(nlocal*sizeof(real));
    for (unsigned long i=0;i<nlocal;++i) phiZero[i]=0.0;
    cuda_safeCall(cudaMemcpy(dev_u_mg[ell],
                  phiZero,
                  nlocal*sizeof(real),
                  cudaMemcpyHostToDevice));
    cuda_safeCall(cudaMemcpy(dev_b_mg[ell],
                  phiZero,
                  nlocal*sizeof(real),
                  cudaMemcpyHostToDevice));
    cuda_safeCall(cudaMemcpy(dev_r_mg[ell],
                  phiZero,
                  nlocal*sizeof(real),
                  cudaMemcpyHostToDevice));
    free(phiZero);
    int blocksize_all_x = min(32,NX[ell]);
    int blocksize_all_y = min(2,NY[ell]);
    threadlayout_mg[ell] = new DomainLayout(NX[ell],
                                         1,1,  // center
                                         blocksize_all_x, // }
                                         blocksize_all_y, // } BLOCKSIZE_all
                                         1,1,  // BLOCKSIZE_center
                                         1,1,  // BLOCKSIZE_EW
                                         1,1,  // BLOCKSIZE_NS
                                         false,false);
    std::stringstream ss;
    ss.str("");
    ss <<  "Smooth [" << ell << "]";
    timer_Smooth[ell] = new Timer(ss.str());
    ss.str("");
    ss <<  "Residual [" << ell << "]";
    timer_Residual[ell] = new Timer(ss.str());
    ss.str("");
    ss <<  "ResSmooth [" << ell << "]";
    timer_ResSmooth[ell] = new Timer(ss.str());
    ss.str("");
    ss <<  "Prolongate [" << ell << "]";
    timer_Prolongate[ell] = new Timer(ss.str());
    timers.push_back(timer_Smooth[ell]);
    timers.push_back(timer_Residual[ell]);
    timers.push_back(timer_ResSmooth[ell]);
    timers.push_back(timer_Prolongate[ell]);
  }
  gridComplexity /= nx[Nlevel-1]*ny[Nlevel-1];
  if (verbose > 0) {
    std::cout << "   grid complexity = " << gridComplexity << std::endl;
  }
  // Initialise Multi-GPU launchers and kernels
  multi_gpu_launcher.resize(Nlevel);
  kernel_smooth.resize(Nlevel);
  kernel_restrict_smooth.resize(Nlevel);
  kernel_residual.resize(Nlevel);
  kernel_prolongate.resize(Nlevel);
  for (int ell=0;ell<Nlevel;++ell) {
    // Multi-GPU launcher
    multi_gpu_launcher[ell] =
      new MultiGPULauncher(nx[ell],ny[ell],*threadlayout_mg[ell]);
    // Smoother kernel
    kernel_smooth[ell] =
      new SmoothKernel(vert_disc,
                       dev_c_mg[ell],
                       dev_r_mg[ell],
                       dev_b_mg[ell],
                       dev_u_mg[ell],
                       rho_relax);
    if (ell < Nlevel-1) {
      // Restrict-and-smooth kernel
      kernel_restrict_smooth[ell] =
        new RestrictSmoothKernel(vert_disc,
                         dev_c_mg[ell],
                         dev_r_mg[ell+1],
                         dev_b_mg[ell],
                         dev_u_mg[ell],
                         rho_relax);
    }
    if (ell > 0) {
      // Prolongation kernel
      kernel_prolongate[ell] =
        new ProlongateKernel(vert_disc,
                         dev_u_mg[ell-1],
                         dev_u_mg[ell]);
    }
    // Residual kernel
    kernel_residual[ell] =
      new ResidualKernel(vert_disc,
                       dev_b_mg[ell],
                       dev_u_mg[ell],
                       dev_r_mg[ell]);
  }
  // Initialise cublas Handle
  cublasHandle=0;
  cublasCreate(&cublasHandle);
  if (verbose > 0) {
    std::cout << std::endl;
  }
};

/* ********************************************************************* *
 * Destructor
 * ********************************************************************* */
SolverMultigrid::~SolverMultigrid() {
  for (int ell=0;ell<Nlevel;++ell) {
    cuda_safeCall(cudaFree(dev_u_mg[ell]));
    cuda_safeCall(cudaFree(dev_b_mg[ell]));
    cuda_safeCall(cudaFree(dev_r_mg[ell]));
    cuda_safeCall(cudaFree(dev_c_mg[ell]));
    delete multi_gpu_launcher[ell];
    delete kernel_smooth[ell];
    delete kernel_restrict_smooth[ell];
    if (ell>0)
      delete kernel_prolongate[ell];
    delete kernel_residual[ell];
    delete threadlayout_mg[ell];
    delete timer_Smooth[ell];
    delete timer_ResSmooth[ell];
    delete timer_Residual[ell];
    delete timer_Prolongate[ell];
  }
  cublasDestroy(cublasHandle);
};

/* ********************************************************************* *
 * Solve using a Richardson iteration
 * ********************************************************************* */
void SolverMultigrid::solve(real* b,
                            real* u) {
  KernelZero zero_kernel(NX_fine,NY_fine);
  KernelTranspose transpose_kernel(NX_fine,NY_fine);
  // Set correct cache / L1 configuration
  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);
  // Initialise solution and RHS on finest level
  unsigned long nlocal=(NX_fine+2*OL_X)*(NY_fine+2*OL_Y)*NZ;

  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_prememcpy = MPI_Wtime();
  cuda_safeCall(cudaMemcpy(dev_u_mg[Nlevel-1],
                b,
                nlocal*sizeof(real),
                cudaMemcpyHostToDevice));
  cudaDeviceSynchronize();

  double t_start_transpose = MPI_Wtime();
  transpose_kernel.invoke(dev_u_mg[Nlevel-1],
                          dev_b_mg[Nlevel-1],
                          ZtoX);
  cudaDeviceSynchronize();

  // Set initial solution to zero
  zero_kernel.invoke(dev_u_mg[Nlevel-1]);
  cudaDeviceSynchronize();
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_prememcpy = MPI_Wtime();

  // Calculate initial residual
  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_preloop = MPI_Wtime();
  rnorm0 = residual(Nlevel-1);
  real rnorm_old = rnorm0;
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_preloop = MPI_Wtime();

  if (verbose > 0) {
    std::cout << " *** Multigrid solve ** " << std::endl;
    std::cout << std::endl;
    std::cout << "  Initial residual = " << std::scientific << rnorm0 << std::endl;
  }
  solverconverged = false;
  // Call V-cycle until convergence
  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_loop = MPI_Wtime();
  for (int iter=1;iter<=maxiter;++iter) {
    vcycle(Nlevel-1);
    // Calculate residual and exit condition
    nIter = iter;
    if (testConvergence) {
      rnorm = residual(Nlevel-1);
      if (verbose > 1) {
        printf("    iteration %d ||r||/||r_0|| = %8.3e rho_r = %6.3f\n",iter,rnorm/rnorm0,rnorm/rnorm_old);
      }
      if ((rnorm/rnorm0) < tolerance ) {
        solverconverged = true;
        break;
      }
      rnorm_old = rnorm;
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_loop = MPI_Wtime();

  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_postmemcpy = MPI_Wtime();
  // Copy solution back on finest level
  transpose_kernel.invoke(dev_u_mg[Nlevel-1],
                          dev_r_mg[Nlevel-1],
                          XtoZ);
  cudaDeviceSynchronize();
  cuda_safeCall(cudaMemcpy(u,
                dev_r_mg[Nlevel-1],
                nlocal*sizeof(real),
                cudaMemcpyDeviceToHost));
  cudaDeviceSynchronize();
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_postmemcpy = MPI_Wtime();

  t_preloop = 1000.*(t_finish_preloop-t_start_preloop);
  t_loop = 1000.*(t_finish_loop-t_start_loop);
  t_memcpy_transpose = 1000.*(t_finish_prememcpy-t_start_prememcpy+
                              t_finish_postmemcpy-t_start_postmemcpy);

  if (verbose > 1) {
    std::cout << std::endl;
  }
  if (verbose > 0) {
    if (solverconverged) {
      printf("  Solver converged in %4d iterations.\n",nIter);
    } else {
      printf("  Solver failed to converge after %4d iterations.\n",nIter);
    }
    printf("  Final residual reduction = %8.3e\n",rnorm/rnorm0);
    printf("\n");
  }
};

/* ********************************************************************* *
 * Multigrid V-cycle
 * ********************************************************************* */
void SolverMultigrid::vcycle(int ell) {
  if (ell == 0) {
    // Solve exactly on coarsest level
    coarse_solve();
  } else {
    int n_smooth;
    if (ell == (Nlevel-1)) {
      n_smooth = n_presmooth_fine;
      // On the finest level, first smooth once
      timer_Smooth[ell]->start();
      multi_gpu_launcher[ell]->invoke(*kernel_smooth[ell]);
      timer_Smooth[ell]->stop();
    } else {
      n_smooth = n_presmooth;
      // On all other levels, restrict the solution and smooth
      timer_ResSmooth[ell]->start();
      multi_gpu_launcher[ell]->invoke(*kernel_restrict_smooth[ell]);
      timer_ResSmooth[ell]->stop();
    }
    // then carry out all remaining presmoothing steps
    for (int i_smooth=0;i_smooth<n_smooth-1;++i_smooth) {
      timer_Smooth[ell]->start();
      multi_gpu_launcher[ell]->invoke(*kernel_smooth[ell]);
      timer_Smooth[ell]->stop();
    }
    // Calculate residual
    timer_Residual[ell]->start();
    multi_gpu_launcher[ell]->invoke(*kernel_residual[ell]);
    timer_Residual[ell]->stop();
    // Recursive call to V-cycle
    vcycle(ell-1);
    // Prolongate solution
    timer_Prolongate[ell]->start();
    multi_gpu_launcher[ell]->invoke(*kernel_prolongate[ell]);
    timer_Prolongate[ell]->stop();
    // Postsmooth
    if (ell == Nlevel-1) {
      n_smooth = n_postsmooth_fine;
    } else {
      n_smooth = n_postsmooth;
    }
    for (int i_smooth=0;i_smooth<n_smooth;++i_smooth) {
      timer_Smooth[ell]->start();
      multi_gpu_launcher[ell]->invoke(*kernel_smooth[ell]);
      timer_Smooth[ell]->stop();
    }
  }
};

/* ********************************************************************* *
 * Solve on coarsest level
 * ********************************************************************* */
void SolverMultigrid::coarse_solve() {
  // Restrict the solution and smooth
  int i_min;
  if (Nlevel > 1) {
    timer_ResSmooth[0]->start();
    multi_gpu_launcher[0]->invoke(*kernel_restrict_smooth[0]);
    timer_ResSmooth[0]->stop();
    i_min=1;
  } else {
    i_min=0;
  }
  // then carry out all remaining smoothing steps
  for (int i_smooth=i_min;i_smooth<n_coarsesmooth;++i_smooth) {
    timer_Smooth[0]->start();
    multi_gpu_launcher[0]->invoke(*kernel_smooth[0]);
    timer_Smooth[0]->stop();
  }
}

/* ********************************************************************* *
 * Calculate residual on a specific level
 * ********************************************************************* */
real SolverMultigrid::residual(int ell) {
  real r2_loc_tmp;
  real r2;
  int NXYZ = (NX[ell]+2*OL_X)*(NY[ell]+2*OL_Y)*NZ;
  timer_Residual[ell]->start();
  multi_gpu_launcher[ell]->invoke(*kernel_residual[ell]);
  timer_Residual[ell]->stop();
  CUBLAS_NRM2(cublasHandle,NXYZ,dev_r_mg[ell],1,&r2_loc_tmp);
  real r2_loc = r2_loc_tmp*r2_loc_tmp;
  MPI_Allreduce(&r2_loc,&r2,1,MPI_real,MPI_SUM,MPI_COMM_WORLD);
  return sqrt(r2);
}
