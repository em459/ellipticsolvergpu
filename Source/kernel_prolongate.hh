/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef KERNEL_PROLONGATE_HH
#define KERNEL_PROLONGATE_HH KERNEL_PROLONGATE_HH
/* *********************************************************** *
 * GPU Implementation of the prolongation kernel
 * *********************************************************** */
#include<cuda.h>
#include"definitions.hh"
#include"discretisation.hh"
#include"kernel.hh"
#include"cuda_errorcheck.hh"

/* *********************************************************** *
* Kernel for prolongation u <- u + P.u_c
* *********************************************************** */
__global__ void gpu_prolongate(const int NX, const int NY,
                               const int nx, const int ny,
                               const int xp, const int yp,
                               const real* a_vert,
                               const real* b_vert,
                               const real* c_vert,
                               const real* d_vert,
                               const real* u_c,
    	                         real* u,
                               const int offset_x,
                               const int offset_y
                              );

/* ********************************************************************* *
 * Wrapper class for smoother kernel
 * ********************************************************************* */
class ProlongateKernel : public Kernel {
 public:
  // Constructor
  ProlongateKernel (const VerticalDiscretisation* vert_disc,
                real* u_c_,
                real* u_) :
  Kernel(vert_disc),
  u_c(u_c_), u(u_) {
    halo_fields.push_back(u);
  }
  // Launch kernel on a particular subdomain
  inline void invoke(dim3 dimGrid,
              dim3 dimBlock,
              cudaStream_t stream,
              int NX,
              int NY,
              int nx,
              int ny,
              int xp,
              int yp,
              int offset_x,
              int offset_y) const {
    gpu_prolongate<<<dimGrid,dimBlock>>>
      (NX,NY,nx,ny,
       xp,yp,
       a_vert,b_vert,c_vert,d_vert,
       u_c,u,
       offset_x,
       offset_y);
    cuda_checkError();
  }
 private:
  real* u_c;
  real* u;
};

#endif // KERNEL_PROLONGATE_HH
