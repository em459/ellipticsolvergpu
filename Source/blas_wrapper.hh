/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef BLAS_WRAPPER_H
#define BLAS_WRAPPER_H BLAS_WRAPPER_H
/* ************************************************************ *
 * If HAVE_BLAS is defined, this module ensures that, depending
 * on whether we use double precision or not, the correct
 * CBLAS routines are called. If HAVE_BLAS is not set, the
 * dummy implementation below is used instead of the system
 * BLAS implementation. This should only be used for testing.
 * ************************************************************ */

#include "definitions.hh"

#ifdef HAVE_BLAS
#ifdef __cplusplus
extern "C" {
#endif
  #include <cblas.h>
#ifdef __cplusplus
}
#endif
  #ifdef DOUBLE__PRECISION
    #define BLAS_COPY cblas_dcopy
    #define BLAS_AXPY cblas_daxpy
    #define BLAS_SCAL cblas_dscal
    #define BLAS_DOT  cblas_ddot
    #define BLAS_NRM2 cblas_dnrm2
  #else // DOUBLE__PRECISION
    #define BLAS_COPY cblas_scopy
    #define BLAS_AXPY cblas_saxpy
    #define BLAS_DOT  cblas_sdot
    #define BLAS_SCAL cblas_sscal
    #define BLAS_NRM2 cblas_snrm2
  #endif // DOUBLE__PRECISION
#else // HAVE_BLAS
  #define BLAS_COPY dummyblas_copy
  #define BLAS_AXPY dummyblas_axpy
  #define BLAS_SCAL dummyblas_scal
  #define BLAS_DOT  dummyblas_dot
  #define BLAS_NRM2 dummyblas_nrm2
#endif // HAVE_BLAS

/* ************************************************************ *
 * Copy
 * ************************************************************ */
void dummyblas_copy(const int n,
                    const real* x,
                    const int incx,
                    real* y,
                    const int inxy);

/* ************************************************************ *
 * axpy
 * ************************************************************ */
void dummyblas_axpy(const int n,
                    const real alpha,
                    const real* x,
                    const int incx,
                    real* y,
                    const int inxy);

/* ************************************************************ *
 * scal
 * ************************************************************ */
void dummyblas_scal(const int n,
                    const real alpha,
                    real* x,
                    const int incx);

/* ************************************************************ *
 * dot
 * ************************************************************ */
real dummyblas_dot(const int n,
                   real* x,
                   const int incx,
                   real* y,
                   const int incy);

/* ************************************************************ *
 * L2 onrm
 * ************************************************************ */
real dummyblas_nrm2(const int n,
                    real* x,
                    const int incx);

#endif // BLAS_WRAPPER_H
