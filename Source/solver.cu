/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "solver.hh"

/* *********************************************************** *
 * Print out timing- and other results
 * *********************************************************** */
void Solver::showResults() {
  if (verbose > 0) {
    printf("  ----------------------------------------\n");
    printf("  Solver converged        = ");
    if (solverconverged)
      printf("YES\n");
    else
      printf("NO\n");
    printf("  Number of iterations    = %4d\n",nIter);
    printf("  Preloop time            = %6.3e ms\n",t_preloop);
    printf("  Loop time               = %6.3e ms\n",t_loop);
    printf("  Memcpy & transpose time = %6.3e ms\n",t_memcpy_transpose);
    printf("  Time per iteration      = %6.3e ms\n",t_loop/nIter);
    printf("  ||r||/||r_0||           = %9.4e\n",rnorm/rnorm0);
    printf("  rho_{avg}               = %6.3f\n",
      pow(rnorm/rnorm0,(real)1./nIter));
    printf("  ----------------------------------------\n");
    printf("\n");
  }
  #ifdef USE_TIMERS
  if (pid == 0) {
    std::ofstream out;
    out.open("timing.txt");
    for(std::vector<Timer*>::iterator it = timers.begin();
        it != timers.end(); ++it) {
      out << *(*it) << std::endl;
    }
    out.close();
  }
  #endif // USE_TIMERS
}
