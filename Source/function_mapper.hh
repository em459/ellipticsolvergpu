/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef FUNCTION_MAPPER_HH
#define FUNCTION_MAPPER_HH FUNCTION_MAPPER_HH
#include "mpi.h"
#include "definitions.hh"
#include "auxilliary.hh"

/* ********************************************************************* *
 * Class for mapping continuous functions onto grid functions
 * ********************************************************************* */
class FunctionMapper {
 public:
  // Constructor
  FunctionMapper(const int NX_,
                 const int NY_) :
    NX(NX_), NY(NY_) {
    MPI_Comm CartComm;
    setup_processor_layout(&pid,&nprocs,&nxyp,&xp,&yp,&CartComm);
    nx = nxyp*NX;
    ny = nxyp*NY;
  }

  template <class F>
  // Map a function onto a GLOBAL field
  void mapGlobal(F& f, real* u,
                 const bool volscale=false) {
    real h = 2.0/nx;
    real volume;
    for(int ix=1-OL_X;ix<=nx+OL_X;ix++) {
      for(int iy=1-OL_Y;iy<=ny+OL_Y;iy++) {
        for(int iz=0;iz<NZ;iz++) {
          real x = (ix-0.5)*h-1.0;
          real y = (iy-0.5)*h-1.0;
          real tmp_z = (1.0*iz/(1.0*(NZ-1.)));
          real z = 1.0*H_ATMOS*tmp_z*tmp_z;
          if (volscale) {
            volume = pow(1.0+x*x+y*y,-1.5)*h*h*volume_V(iz);
          } else {
            volume = 1.0;
          }
          if ( (ix >= 1) && (ix <= nx) &&
               (iy >= 1) && (iy <= ny) ) {
              u[LINIDX_Z(nx,ny,ix,iy,iz)] = volume*f(x,y,z);
          } else {
            u[LINIDX_Z(nx,ny,ix,iy,iz)] = 0.0;
          }
        }
      }
    }
  }

  // Map a function onto a LOCAL field
  template <class F>
  void mapLocal(F& f,
                real* u,
                const bool volscale=false) {
    real h = 2.0/nx;
    real volume;
    for(int ix=1-OL_X;ix<=NX+OL_X;ix++) {
      for(int iy=1-OL_Y;iy<=NY+OL_Y;iy++) {
        for(int iz=0;iz<NZ;iz++) {
          int ix_g = ix+xp*NX;
          int iy_g = iy+yp*NY;
          real x = (ix_g-0.5)*h-1.0;
          real y = (iy_g-0.5)*h-1.0;
          real tmp_z = (1.0*iz/(1.0*(NZ-1.)));
          real z = 1.0*H_ATMOS*tmp_z*tmp_z;
          if (volscale) {
            volume = pow(1.0+x*x+y*y,-1.5)*h*h*volume_V(iz);
          } else {
            volume = 1.0;
          }
          if ( (ix_g >= 1) && (ix_g <= nx) &&
               (iy_g >= 1) && (iy_g <= ny) ) {
              u[LINIDX_Z(NX,NY,ix,iy,iz)] = volume*f(x,y,z);
          } else {
            u[LINIDX_Z(NX,NY,ix,iy,iz)] = 0.0;
          }
        }
      }
    }
  }
 private:
  // Local problem size
  int NX;
  int NY;
  // Processor layout
  int nprocs, pid, nx, ny, nxyp, xp, yp;
};

#endif // FUNCTION_MAPPER_HH
