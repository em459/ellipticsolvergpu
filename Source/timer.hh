/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef TIMER_HH
#define TIMER_HH TIMER_HH

#include <iostream>
#include <iomanip>
#include <string>
#include "mpi.h"

/* ********************************************************************* *
 * Timer class
 * ********************************************************************* */
class Timer {
 public:
  // Constructor
  Timer(const std::string label_) :
   label(label_),
   ncall(0),
   telapsed(0.0),
   isrunning(false),
   tStart(0.0) {}
  // Return timer information
  std::string getLabel() const { return label; }
  int nCall() const { return ncall; }
  double tElapsed() const { return telapsed; }
  double tCall() const {
    if (ncall > 0)
      return telapsed/double(ncall);
    else
      return 0.0;
  }
  // Start timer
  inline void start() {
#ifdef USE_TIMERS
    if (isrunning) timerWarning("Timer already running.");
    MPI_Barrier(MPI_COMM_WORLD);
    tStart = MPI_Wtime();
    isrunning=true;
#endif // USE_TIMERS
  }
  // Stop timer
  inline void stop() {
#ifdef USE_TIMERS
    if (!isrunning) timerWarning("Timer not running.");
    MPI_Barrier(MPI_COMM_WORLD);
    double tFinish = MPI_Wtime();
    isrunning=false;
    telapsed += (tFinish-tStart);
    ncall += 1;
#endif // USE_TIMERS
  }
  // Reset timer
  inline void reset() {
#ifdef USE_TIMERS
    isrunning = false;
    telapsed = 0.0;
    ncall = 0;
#endif // USE_TIMERS
  }
 private:
  // Print out warning message
  void timerWarning (const std::string msg);
  // Label
  std::string label;
  // elapsed time
  double telapsed;
  // Number of calls
  int ncall;
  bool isrunning;
  // start time
  double tStart;
};

/* ********************************************************************* *
 * Output operator for timer class
 * ********************************************************************* */
std::ostream& operator<<(std::ostream& out,
                         const Timer& timer);

#endif // TIMER_HH
