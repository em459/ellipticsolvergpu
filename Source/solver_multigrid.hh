/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef SOLVER_MULTIGRID_HH
#define SOLVER_MULTIGRID_HH SOLVER_MULTIGRID_HH
#include <vector>
#include <iostream>
#include <sstream>
#include <mpi.h>
#include <GCL.h>
#include "cublas_wrapper.hh"
#include <halo_exchange.h>
#include <utils/layout_map.h>
#include "auxilliary.hh"
#include "definitions.hh"
#include "discretisation.hh"
#include "multi_gpu.hh"
#include "kernel_smooth.hh"
#include "kernel_restrict_smooth.hh"
#include "kernel_prolongate.hh"
#include "kernel_residual.hh"
#include "kernel_zero.hh"
#include "kernel_transpose.hh"
#include "solver.hh"
#include "cuda_errorcheck.hh"


/* ********************************************************************* *
 * Multigrid class
 * ********************************************************************* */
class SolverMultigrid : public Solver {
 public:
  // Constructor
  SolverMultigrid(DomainLayout *threadlayout_,
                  const int Nlevel_,
                  const int n_coarsesmooth_,
                  const bool testConvergence_,
                  const double rho_relax_,
                  const real  omega2_,
                  const real lambda2_,
                  const int maxiter_,
                  const real tolerance_,
                  const int verbose_);
  // Destructor
  ~SolverMultigrid();
  // Solve using a Richardson iteration
  virtual void solve(real* b,
                     real* u);
  // Multigrid V-cycle
  void vcycle(int ell);
 private:
  // Solve on coarsest level
  void coarse_solve();
  real residual(int ell);
  // Multigrid parameters
  int n_coarsesmooth;
  int n_presmooth;
  int n_postsmooth;
  int n_presmooth_fine;
  int n_postsmooth_fine;
  // Smoother parameters
  real rho_relax;
  // Number of levels
  int Nlevel;
  // Fine grid problem size
  int NX_fine;
  int NY_fine;
  int nx_fine;
  int ny_fine;
  // Local problem size on different multigrid levels
  std::vector<int> NX;
  std::vector<int> NY;
  // Global problem size on different multigrid level
  std::vector<int> nx;
  std::vector<int> ny;
  // RHS, solution and residual
  std::vector<real*> dev_b_mg;
  std::vector<real*> dev_u_mg;
  std::vector<real*> dev_r_mg;
  // Temporary work space for Thomas algorithm and residual calculation
  std::vector<real*> dev_c_mg;
  // Multi-GPU launchers on all levels
  std::vector<MultiGPULauncher*> multi_gpu_launcher;
  // Kernels on all levels
  std::vector<SmoothKernel*> kernel_smooth;
  std::vector<RestrictSmoothKernel*> kernel_restrict_smooth;
  std::vector<ProlongateKernel*> kernel_prolongate;
  std::vector<ResidualKernel*> kernel_residual;
  std::vector<DomainLayout*> threadlayout_mg;
  // cublas Handle
  cublasHandle_t cublasHandle;
  // Do we test the convergence in the iterative solver?
  bool testConvergence;
  // Timers
  std::vector<Timer*> timer_Smooth;
  std::vector<Timer*> timer_Residual;
  std::vector<Timer*> timer_ResSmooth;
  std::vector<Timer*> timer_Prolongate;
};

#endif // SOLVER_MULTIGRID_HH
