/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "prec_host.hh"

/* *********************************************************** *
 * Serial Block-Jacobi Preconditioner for test
 * y = M^{-1}.b
 * *********************************************************** */
void prec(const int NX,
          const int NY,
          const real* a_vert,
          const real* b_vert,
          const real* c_vert,
          const real* d_vert,
          const real* b,
      	  real* y) {
  DECLARE_DISCRETISATION_VARIABLES
  real c[NZ];
  int ix, iy, iz;
  for (ix = 1; ix<=NX; ix++) {
    for (iy = 1; iy<=NY; iy++) {
      CALCULATE_ALPHA_TIJ(NX,NY,ix,iy)
      // Do a tridiagonal solve in the vertical direction
      // STEP 1: Calculate modified coefficients
      real diag_tmp = ((a_vert[0]-b_vert[0]-c_vert[0])*Tij-alpha*d_vert[0]);
      c[0] = b_vert[0]*Tij/diag_tmp;
      y[LINIDX_Z(NX,NY,ix,iy,0)] = b[LINIDX_Z(NX,NY,ix,iy,0)] / diag_tmp;
      for (iz = 1; iz<NZ; iz++) {
        real b_tmp = b_vert[iz];
        real c_tmp = c_vert[iz];
        real d_tmp = d_vert[iz];
        real tmp = ((a_vert[iz]-b_tmp-c_tmp)*Tij-alpha*d_tmp)
                 - c[iz-1]*(c_tmp*Tij);
        c[iz] = (b_tmp*Tij) / tmp;
        y[LINIDX_Z(NX,NY,ix,iy,iz)] = (b[LINIDX_Z(NX,NY,ix,iy,iz)]
                                    - y[LINIDX_Z(NX,NY,ix,iy,iz-1)]*c_tmp*Tij)
                                    / tmp;
      }
      // STEP 2: Back-substitution.
      for (iz = NZ-2; iz>=0; iz--) {
        y[LINIDX_Z(NX,NY,ix,iy,iz)] -= c[iz]*y[LINIDX_Z(NX,NY,ix,iy,iz+1)];
      }
    }
  }
}

/* *********************************************************** *
 * Interleaved preconditioner kernel
 * *********************************************************** */
void ilvd_prec(const int NX,
               const int NY,
               const real* a_vert,
               const real* b_vert,
               const real* c_vert,
               const real* d_vert,
               const real* q,
               const real alpha_prime,
               real* r,
               real* z,
               real* rsq,
               real* rz) {
  DECLARE_DISCRETISATION_VARIABLES
  int ix, iy, iz;
  real tmp_r;
  real tmp_z;
  real tmp_rsq=0.0;
  real tmp_rz=0.0;
  real c[NZ];
  for (ix=1;ix<=NX;++ix) {
    for (iy=1;iy<=NY;++iy) {
      CALCULATE_ALPHA_TIJ(NX,NY,ix,iy)
      // Do a tridiagonal solve in the vertical direction
      // STEP 1: Calculate modified coefficients
      // Vertical level 0
      real b_tmp = b_vert[0];
      real c_tmp = c_vert[0];
      real d_tmp = d_vert[0];
      real diag_tmp = (a_vert[0]-b_tmp-c_tmp)*Tij-alpha*d_tmp;
      c[0] = (b_tmp*Tij)/diag_tmp;
      tmp_r = r[LINIDX_Z(NX,NY,ix,iy,0)];
      tmp_r -= alpha_prime*q[LINIDX_Z(NX,NY,ix,iy,0)];
      z[LINIDX_Z(NX,NY,ix,iy,0)] = tmp_r/diag_tmp;
      tmp_rsq += tmp_r*tmp_r;
      r[LINIDX_Z(NX,NY,ix,iy,0)] = tmp_r;
      // Vertical level > 0
      for (iz = 1; iz<NZ; iz++) {
        b_tmp = b_vert[iz];
        c_tmp = c_vert[iz];
        d_tmp = d_vert[iz];
        real tmp = (((a_vert[iz]-b_tmp-c_tmp)*Tij-alpha*d_tmp)-c[iz-1]*c_tmp*Tij);
        c[iz] = (b_tmp*Tij) / tmp;
        tmp_r = r[LINIDX_Z(NX,NY,ix,iy,iz)];
        tmp_r -= alpha_prime*q[LINIDX_Z(NX,NY,ix,iy,iz)];
        z[LINIDX_Z(NX,NY,ix,iy,iz)] = (tmp_r
                                    - z[LINIDX_Z(NX,NY,ix,iy,iz-1)]
                                        * c_tmp*Tij)/tmp;
        tmp_rsq += tmp_r*tmp_r;
        r[LINIDX_Z(NX,NY,ix,iy,iz)] = tmp_r;
      }
      tmp_rz += z[LINIDX_Z(NX,NY,ix,iz,NZ-1)]*r[LINIDX_Z(NX,NY,ix,iy,NZ-1)];
      // STEP 2: Back-substitution.
      for (iz = NZ-2; iz>=0; iz--) {
        tmp_z = z[LINIDX_Z(NX,NY,ix,iy,iz)];
        tmp_z -= c[iz]*z[LINIDX_Z(NX,NY,ix,iy,iz+1)];
        z[LINIDX_Z(NX,NY,ix,iy,iz)] = tmp_z;
        tmp_rz += tmp_z*r[LINIDX_Z(NX,NY,ix,iy,iz)];
      }
    }
  }
  *rsq = tmp_rsq;
  *rz = tmp_rz;
}

