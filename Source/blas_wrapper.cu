/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "math.h"
#include "definitions.h"
#include "blas_wrapper.h"

/* ************************************************************ *
 * Copy
 * ************************************************************ */
void dummyblas_copy(const int n,
                    const real* x,
                    const int incx,
                    real* y,
                    const int incy) {
  int i,ix,iy;
  for (ix=0,iy=0,i=0;i<n;++i,ix+=incx,iy+=incy) {
    y[iy] = x[ix];
  }
}

/* ************************************************************ *
 * axpy
 * ************************************************************ */
void dummyblas_axpy(const int n,
                    const real alpha,
                    const real* x,
                    const int incx,
                    real* y,
                    const int incy) {
  int i,ix,iy;
  for (ix=0,iy=0,i=0;i<n;++i,ix+=incx,iy+=incy) {
    y[iy] += alpha*x[ix];
  }
}

/* ************************************************************ *
 * scal
 * ************************************************************ */
void dummyblas_scal(const int n,
                    const real alpha,
                    real* x,
                    const int incx) {
  int i,ix;
  for (ix=0,i=0;i<n;++i,ix+=incx) {
    x[ix]*=alpha;
  }
}

/* ************************************************************ *
 * dot
 * ************************************************************ */
real dummyblas_dot(const int n,
                   real* x,
                   const int incx,
                   real* y,
                   const int incy) {
  int i,ix,iy;
  real tmp = 0.0;
  for (ix=0,iy=0,i=0;i<n;++i,ix+=incx,iy+=incy) {
    tmp += y[iy]*x[ix];
  }
  return tmp;
}

/* ************************************************************ *
 * L2 onrm
 * ************************************************************ */
real dummyblas_nrm2(const int n,
                    real* x,
                    const int incx) {
  int i,ix;
  real tmp = 0.0;
  for (ix=0,i=0;i<n;++i,ix+=incx) {
    tmp += x[ix]*x[ix];
  }
  return sqrt(tmp);
}
