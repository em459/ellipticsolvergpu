/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#ifndef PREC_HOST_HH
#define PREC_HOST_HH PREC_HOST_HH
/* *********************************************************** *
 * CPU Implementation of the Block-Jacobi preconditioner
 * This file contains a matrix-free implementation using the
 * Thomas algorithm and a method for calculation the LU
 * decompostion and solving the system LU.x = b via
 * backsubstitution.
 * *********************************************************** */
#include<math.h>
#include "definitions.hh"
#include "discretisation.hh"

/* *********************************************************** *
* Serial Block-Jacobi Preconditioner
* y = M^{-1}.b
* *********************************************************** */
void prec(const int NX,
          const int NY,
          const real* a_vert,
          const real* b_vert,
          const real* c_vert,
          const real* d_vert,
          const real* b,
      	  real* y);

/* *********************************************************** *
 * Interleaved preconditioner
 *
 * Simultaneously calculate:
 *   r <- r + alpha*q
 *   rsq <- <r,r>
 *   z <- M^{-1}.r
 *   rz <- <r,z>
 *
 * *********************************************************** */
void ilvd_prec(const int NX,
               const int NY,
               const real* a_vert,
               const real* b_vert,
               const real* c_vert,
               const real* d_vert,
               const real* q,
               const real alpha,
               real* r,
               real* z,
               real* rsq,
               real* rz);

#endif // PREC_HOST_HH
