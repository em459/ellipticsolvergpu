/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the EllipticSolverGPU code.
 *  
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2014]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  Main Developer: Eike Mueller, University of Bath
 *  
 *  Contributors:
 *    Eero Vainikko (University of Tartu)
 *    Sinan Shi (EPCC Edinburgh)
 *  
 *  EllipticSolverGPU is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  EllipticSolverGPU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
 *  main directory).  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  The EllipticSolverGPU code uses (i.e. links to and includes header files
 *  from) the Generic Communication Library (GCL). The GCL has been developed
 *  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
 *  CENTRE (CSCS) and it is distributed under the Copyright statement in the
 *  file COPYING.GCL.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/



#include "solver_cg.hh"

/* *********************************************************** *
 * Constructor
 * *********************************************************** */
SolverCG::SolverCG(DomainLayout* thread_layout_,
                   const real omega2_,
                   const real lambda2_,
                   const int maxiter_,
                   const real tolerance_,
                   const int verbose_) :
  Solver(thread_layout_,
         omega2_,
         lambda2_,
         maxiter_,
         tolerance_,
         verbose_) {
  if (verbose > 0) {
    std::cout << " Initialising conjugate gradient solver " << std::endl;
    std::cout << "   maxiter      = " << maxiter    << std::endl;
    std::cout << "   tolerance    = " << tolerance  << std::endl;
    std::cout << "   omega2       = " << omega2     << std::endl;
    std::cout << "   lambda2      = " << lambda2    << std::endl;
    std::cout << std::endl;
  }

  NX = threadlayout->all.NX;
  NY = threadlayout->all.NY;

  const int NXYZ=(NX+2*OL_X)*(NY+2*OL_Y)*NZ;
  const int NXY=(NX+2*OL_X)*(NY+2*OL_Y);
  // Set up vertical discretisation
  vert_disc = new VerticalDiscretisation(omega2, lambda2);
  //Allocate CG vectors on GPU
  cuda_safeCall(cudaMalloc((void**)&dev_x,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_b,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_r,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_p,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_q,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_c,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_z,NXYZ*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_pq,NXY*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_rsq,NXY*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_rz,NXY*sizeof(real)));
  cuda_safeCall(cudaMalloc((void**)&dev_one,NXY*sizeof(real)));
  // Loop variables, counts iterations

  // Only print to screen on master processor

  // Initialise vectors with 1's inside the interior
  // used for dot products
  real* oneXY;
  oneXY=(real*)malloc(NXY*sizeof(real));
  for (int ix=1-OL_X;ix<=NX+OL_X;++ix) {
    for (int iy=1-OL_Y;iy<=NY+OL_Y;++iy) {
      if ( (ix>=1) && (iy>=1) &&
           (ix<=NX) && (iy<=NY) ) {
        oneXY[LINIDX_2D(NX,NY,ix,iy)]=1.0;
      } else {
        oneXY[LINIDX_2D(NX,NY,ix,iy)]=0.0;
      }
    }
  }
  cuda_safeCall(cudaMemcpy(dev_one,oneXY,NXY*sizeof(real),
                cudaMemcpyHostToDevice));
  cudaDeviceSynchronize();
  free(oneXY);

  // Initialise auxilliary vectors to zero
  real* zeroXYZ;
  zeroXYZ=(real*)malloc(NXYZ*sizeof(real));
  for (unsigned long i=0;i<NXYZ;++i) zeroXYZ[i]=0.0;
  cuda_safeCall(cudaMemcpy(dev_b,zeroXYZ,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_r,zeroXYZ,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_p,zeroXYZ,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_q,zeroXYZ,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_z,zeroXYZ,NXYZ*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_rsq,zeroXYZ,NXY*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_rz,zeroXYZ,NXY*sizeof(real),
                cudaMemcpyHostToDevice));
  cuda_safeCall(cudaMemcpy(dev_pq,zeroXYZ,NXY*sizeof(real),
                cudaMemcpyHostToDevice));
  cudaDeviceSynchronize();
  free(zeroXYZ);

  // Create kernels and multi-GPU launcher

  apply_kernel_xq = new ApplyKernel(vert_disc,dev_x,dev_q);
  apply_kernel_pq = new ApplyKernel(vert_disc,dev_p,dev_q);
  prec_kernel = new PrecKernel(vert_disc,dev_c,dev_r,dev_z);
  ilvd_prec_kernel = new IlvdPrecKernel(vert_disc,dev_c,
                                        dev_q,
                                        &alpha,
                                        dev_r,
                                        dev_z,
                                        dev_rsq,
                                        dev_rz);
  ilvd_apply_kernel = new IlvdApplyKernel(vert_disc,
                                          dev_z,
                                          &alpha,
                                          &beta,
                                          dev_x,
                                          dev_p,
                                          dev_q,
                                          dev_pq);

  int nx=NX*nxyp;
  int ny=NY*nxyp;

  multi_gpu_launcher = new MultiGPULauncher(nx,ny,*threadlayout);

  //cublas Handle
  cublasHandle=0;
  cublasCreate(&cublasHandle);
  timer_SpMV = new Timer("SpMV");
  timer_Prec = new Timer("Preconditioner");
  timers.push_back(timer_SpMV);
  timers.push_back(timer_Prec);
}

/* *********************************************************** *
 * Destructor
 * *********************************************************** */
SolverCG::~SolverCG() {
  // Free device memory
  cuda_safeCall(cudaFree(dev_x));
  cuda_safeCall(cudaFree(dev_b));
  cuda_safeCall(cudaFree(dev_r));
  cuda_safeCall(cudaFree(dev_p));
  cuda_safeCall(cudaFree(dev_q));
  cuda_safeCall(cudaFree(dev_z));
  cuda_safeCall(cudaFree(dev_c));
  cuda_safeCall(cudaFree(dev_pq));
  cuda_safeCall(cudaFree(dev_rsq));
  cuda_safeCall(cudaFree(dev_rz));
  cuda_safeCall(cudaFree(dev_one));
  // Destroy cublas handle
  cublasDestroy(cublasHandle);
  // Free kernel launcher and GPU kernels
  delete apply_kernel_xq;
  delete apply_kernel_pq;
  delete prec_kernel;
  delete ilvd_prec_kernel;
  delete ilvd_apply_kernel;
  delete multi_gpu_launcher;
  // Free vertical discretisation
  delete vert_disc;
  // Free timers
  delete timer_SpMV;
  delete timer_Prec;
}

/* *********************************************************** *
 * Iterative solver
 * *********************************************************** */
void SolverCG::solve(real* b,
                     real* u) {
  KernelZero zero_kernel(NX,NY);
  KernelTranspose transpose_kernel(NX,NY);
  // Set correct cache / L1 configuration
  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);
  // Initialise solution
  unsigned long nlocal=(NX+2*OL_X)*(NY+2*OL_Y)*NZ;
  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_prememcpy = MPI_Wtime();
  cuda_safeCall(cudaMemcpy(dev_r,
                b,
                nlocal*sizeof(real),
                cudaMemcpyHostToDevice));
  cudaDeviceSynchronize();
  transpose_kernel.invoke(dev_r,dev_b,ZtoX);
  cudaDeviceSynchronize();
  zero_kernel.invoke(dev_x);
  cudaDeviceSynchronize();
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_prememcpy = MPI_Wtime();

  const int NXYZ=(NX+2*OL_X)*(NY+2*OL_Y)*NZ;
  const int NXY=(NX+2*OL_X)*(NY+2*OL_Y);

  // Loop variables, counts iterations
  unsigned int k;

  // Norms and scalar products
  real rnorm0_loc, rnorm_old;
  real rz, rz_loc, rznew,pq, rnorm_loc, pq_loc, rznew_loc;

  solverconverged = false;

  // Number '-1' for CUBLAS calls
  real negone=-1.0;

  // Initialise
  // *** b -> r_0
  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_preloop = MPI_Wtime();
  CUBLAS_COPY(cublasHandle,NXYZ,dev_b,1,dev_r,1);

  // *** A.x_0 -> q_0
  multi_gpu_launcher->invoke(*apply_kernel_xq);

  // *** r_0 - q_0 = r_0 - A.x_0 -> r_0
  CUBLAS_AXPY(cublasHandle, NXYZ,&negone,dev_q,1,dev_r,1);

  // *** M^{-1}.r_0 -> z_0
  multi_gpu_launcher->invoke(*prec_kernel);

  // *** z_0 -> p_0
  CUBLAS_COPY(cublasHandle,NXYZ,dev_z,1,dev_p,1);
  // *** rz -> <r,z>
  CUBLAS_DOT(cublasHandle,NXYZ,dev_r,1,dev_z,1,&rz_loc);
  MPI_Allreduce(&rz_loc,&rz,1,MPI_real,MPI_SUM,MPI_COMM_WORLD);
  // *** ||r_{0}||^2
  CUBLAS_NRM2(cublasHandle,NXYZ,dev_r,1,&rnorm0_loc);
  rnorm0_loc = rnorm0_loc*rnorm0_loc;
  MPI_Allreduce(&rnorm0_loc,&rnorm0,1,MPI_real,MPI_SUM,MPI_COMM_WORLD);
  rnorm0 = sqrt(rnorm0);
  rnorm_old = rnorm0;

  // *** A.p -> q
  multi_gpu_launcher->invoke(*apply_kernel_pq);

  // *** <p,q> -> pq
  CUBLAS_DOT(cublasHandle,NXYZ,dev_p,1,dev_q,1,&pq_loc);
  MPI_Allreduce(&pq_loc,&pq,1,MPI_real,MPI_SUM,MPI_COMM_WORLD);
  alpha = rz/pq;
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_preloop = MPI_Wtime();

  if (verbose > 0) {
    std::cout << " *** CG solve ** " << std::endl;
    std::cout << std::endl;
    std::cout << "  Initial residual = " << std::scientific << rnorm0 << std::endl;
  }

  // Main loop over iterations
  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_loop = MPI_Wtime();
  for (k=1; k<=maxiter; k++) {
    // Interleaved PRECONDITIONER KERNEL
    /* Simultaneously calculate:
     *   r <- r + alpha*q
     *   rsq <- <r,r>
     *   z <- M^{-1}.r
     */
    timer_Prec->start();
    multi_gpu_launcher->invoke(*ilvd_prec_kernel);
    timer_Prec->stop();

    // Calculate global rsq and rz
    CUBLAS_DOT(cublasHandle,NXY,dev_rsq,1,dev_one,1,&rnorm_loc);
    MPI_Allreduce(&rnorm_loc,&rnorm,1,MPI_real,MPI_SUM,MPI_COMM_WORLD);
    CUBLAS_DOT(cublasHandle,NXY,dev_rz,1,dev_one,1,&rznew_loc);
    MPI_Allreduce(&rznew_loc,&rznew,1,MPI_real,MPI_SUM,MPI_COMM_WORLD);
    rnorm = sqrt(rnorm);
    // Exit condition
    nIter = k;
    if (verbose > 1) {
      printf("    iteration %d ||r||/||r_0|| = %8.3e rho_r = %6.3f\n",k,rnorm/rnorm0,rnorm/rnorm_old);
    }
    if (rnorm/rnorm0 < tolerance) {
      solverconverged = true;
      break;
    }
    rnorm_old=rnorm;
    beta = rznew/rz;
    rz = rznew;
    // Interleaved APPLY KERNEL
    /*
     * Simultaneously calculate:
     *   u <- u + alpha*p
     *   p <- z + beta*p
     *   q <- A.p
     *   pq <- <p,q>
     */
    timer_SpMV->start();
    multi_gpu_launcher->invoke(*ilvd_apply_kernel);
    timer_SpMV->stop();
    CUBLAS_DOT(cublasHandle,NXY,dev_pq,1,dev_one,1,&pq_loc);
    MPI_Allreduce(&pq_loc,&pq,1,MPI_real,MPI_SUM,MPI_COMM_WORLD);
    alpha = rz/pq;
  }
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_loop = MPI_Wtime();

  // Transpose and copy solution back
  MPI_Barrier(MPI_COMM_WORLD);
  double t_start_postmemcpy = MPI_Wtime();
  transpose_kernel.invoke(dev_x,dev_r,XtoZ);
  cudaDeviceSynchronize();
  cuda_safeCall(cudaMemcpy(u,
                dev_r,
                nlocal*sizeof(real),
                cudaMemcpyDeviceToHost));
  cudaDeviceSynchronize();
  MPI_Barrier(MPI_COMM_WORLD);
  double t_finish_postmemcpy = MPI_Wtime();
  t_preloop = 1000.*(t_finish_preloop-t_start_preloop);
  t_loop = 1000.*(t_finish_loop-t_start_loop);
  t_memcpy_transpose = 1000.*(t_finish_prememcpy-t_start_prememcpy+
                              t_finish_postmemcpy-t_start_postmemcpy);
  if (verbose > 1) {
    std::cout << std::endl;
  }
  if (verbose > 0) {
    if (solverconverged) {
      printf("  Solver converged in %4d iterations.\n",nIter);
    } else {
      printf("  Solver failed to converge after %4d iterations.\n",nIter);
    }
    printf("  Final residual reduction = %8.3e\n",rnorm/rnorm0);
    printf("\n");
  }
}
