### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the EllipticSolverGPU code.
#  
#  (c) The copyright relating to this work is owned jointly 
#  by the Crown, Met Office and NERC [2014]. However, it
#  has been created with the help of the GungHo Consortium,
#  whose members are identified at
#  https://puma.nerc.ac.uk/trac/GungHo/wiki
#  
#  Main Developer: Eike Mueller, University of Bath
#  
#  Contributors:
#    Eero Vainikko (University of Tartu)
#    Sinan Shi (EPCC Edinburgh)
#  
#  EllipticSolverGPU is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  EllipticSolverGPU is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
#  main directory).  If not, see <http://www.gnu.org/licenses/>.
#  
#  The EllipticSolverGPU code uses (i.e. links to and includes header files
#  from) the Generic Communication Library (GCL). The GCL has been developed
#  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
#  CENTRE (CSCS) and it is distributed under the Copyright statement in the
#  file COPYING.GCL.
#
### COPYRIGHT AND LICENSE STATEMENT ###



import sys
import os
import re
import math
import socket
# Number of vertical levels
nz = 128

########################################################
#  User defined options
########################################################

## Local resolutions
nxList = {}
nxList['cg'] = (128,256,512,768)
nxList['multigrid'] = (128,256,512)

## numbers of GPUs
nprocsList = (1,4)

## GPU Direct
UseGPUDirect = True

## Use debug queue on Titan?
DebugQueue = False

## Number of multigrid levels
nlevel = 5

## Number of GPUs per node.
# Can be 1, 2 or -1 (=any)
GPUsPerNode=2

## Solver names [do not edit]
Solvers = ('cg','multigrid')

########################################################
# Generate parameter files
########################################################
def GenerateParameterFiles(RunDir,solver,nx,nprocs):
  inputDir = RunDir+'/input_'+str(nprocs)+'GPUs/'
  if (not (os.path.exists(inputDir))):
    os.mkdir(inputDir)
  parameterFilename = inputDir+'parameters_'+solver+'_'+str(nx)+'x'+str(nx)+'x'+str(nz)+'_'+str(nprocs)+'GPUs.in'
  domainFilename = inputDir+'domain_'+str(nx)+'x'+str(nx)+'x'+str(nz)+'_'+str(nprocs)+'GPUs.in'
  # Parameter file
  nx_Global = nx*math.sqrt(nprocs)
  R_earth = 6371.*1000.
  c_s = 550.0
  N = 0.018
  dx = 2.*math.pi*R_earth / (4.*nx_Global)
  dt = 60.*10*(256./nx_Global)
  alphadt = 0.5*dt
  omega2 = (c_s*alphadt/R_earth)**2
  lambda2 = 1./(1+(alphadt*N)**2)
  File = open(parameterFilename,'w')
  print >> File, 'solver = '+str(solver)
  print >> File, 'nlevel = '+str(nlevel)
  print >> File, 'omega2 = '+str(omega2)
  print >> File, 'lambda2 = '+str(lambda2)
  print >> File, 'resreduction = 1.0E-5'
  print >> File, 'maxiter = 500'
  print >> File, 'verbose = 1'
  print >> File, 'compare_to_host = false'
  print >> File, 'compare_to_exact = false'
  File.close()
  File = open(domainFilename,'w')
  print >> File, 'NX = '+str(nx)
  print >> File, 'NX_center             = 32'
  print >> File, 'NY_center             = 32'
  print >> File, 'BLOCKSIZE_NX_all      = 64'
  print >> File, 'BLOCKSIZE_NY_all      = 2'
  print >> File, 'BLOCKSIZE_NX_center   = 32'
  print >> File, 'BLOCKSIZE_NY_center   = 4'
  print >> File, 'BLOCKSIZE_NX_bound_EW = 16'
  print >> File, 'BLOCKSIZE_NY_bound_EW = 4'
  print >> File, 'BLOCKSIZE_NX_bound_NS = 64'
  print >> File, 'BLOCKSIZE_NY_bound_NS = 2'
  print >> File, 'overlap_comms         = false'
  File.close()

########################################################
# Generate a job script and parameter file in
# a specific directory
########################################################
def GenerateJobTitan(RunDir,ExecutableDir,nprocs):

  jobFilename = 'jobscript_'+str(nprocs)+'GPUs.bash'

  # Job script
  File = open(RunDir+'/'+jobFilename,'w')
  print >> File, '#!/bin/bash'
  print >> File, '#PBS -A CSC113'
  print >> File, '#PBS -N PCG'
  print >> File, '#PBS -j oe'
  if (DebugQueue):
    print >> File, '#PBS -q debug'
  print >> File, '#PBS -l walltime=0:15:00,nodes='+str(nprocs)
  print >> File, '#PBS -l gres=atlas1%atlas2'
  print >> File, ''

  print >> File, ''
  if (UseGPUDirect):
    print >> File, 'module switch cray-mpich2 cray-mpich2/5.6.4'
    print >> File, 'export LD_LIBRARY_PATH=$CRAY_LD_LIBRARY_PATH:$LD_LIBRARY_PATH'
    print >> File, 'export MPICH_RDMA_ENABLED_CUDA=1'
  print >> File, ''
  print >> File, 'echo \"WORKDIR=$PBS_O_WORKDIR\"'
  print >> File, 'export NPROCS='+str(nprocs)
  print >> File, 'echo \"Number of GPUs: $NPROCS\"'
  print >> File, 'export Jobname=\"'+str(nprocs)+'GPUs\"'
  print >> File, ''
  for solver in Solvers:
    for nx in nxList[solver]:
      inputDir = 'input_'+str(nprocs)+'GPUs/'
      parameterFilename = 'parameters_'+solver+'_'+str(nx)+'x'+str(nx)+'x'+str(nz)+'_'+str(nprocs)+'GPUs.in'
      domainFilename = 'domain_'+str(nx)+'x'+str(nx)+'x'+str(nz)+'_'+str(nprocs)+'GPUs.in'
      print >> File, '################ NX = '+str(nx)+' ##################'
      print >> File, ''
      print >> File, 'cd $PBS_O_WORKDIR'
      print >> File, 'export PARAMETERFILE=\"'+parameterFilename+'\"'
      print >> File, 'export DOMAINFILE=\"'+domainFilename+'\"'
      print >> File, 'export EXECUTABLE=\"'+str(ExecutableDir)+'/main_solver-CUDAC.x\"'
      print >> File, 'export WORKDIR=$PBS_O_WORKDIR/${Jobname}_${PBS_JOBID}'
      print >> File, 'export INPUTDIR=$PBS_O_WORKDIR/'+inputDir
      print >> File, 'mkdir -p $WORKDIR'
      print >> File, 'export Domainname=\"'+str(nx)+'x'+str(nx)+'x'+str(nz)+'\"'
      print >> File, 'export WORKSUBDIR=$PBS_O_WORKDIR/${Jobname}_${PBS_JOBID}/${Domainname}'+'_'+solver
      print >> File, 'mkdir ${WORKSUBDIR}'
      print >> File, 'cp $0 $WORKDIR/jobscript.bash'
      print >> File, 'cp ${INPUTDIR}/${PARAMETERFILE} $WORKSUBDIR'
      print >> File, 'cp ${INPUTDIR}/${DOMAINFILE} $WORKSUBDIR'
      print >> File, 'cd $WORKSUBDIR'
      print >> File, 'time (aprun -n '+str(nprocs)+' -N1 -S1 -r1 $EXECUTABLE $WORKSUBDIR/$PARAMETERFILE $WORKSUBDIR/$DOMAINFILE > Output.dat) 2> Time.txt'
      print >> File, ''
  File.close()

########################################################
# Generate a job script and parameter file in
# a specific directory
########################################################
def GenerateJobEMERALD(RunDir,ExecutableDir,nprocs):

  jobFilename = 'jobscript_'+str(nprocs)+'GPUs.bash'

  # Job script
  File = open(RunDir+'/'+jobFilename,'w')
  print >> File, '#!/bin/bash'
  print >> File, '#BSUB -o '+RunDir+'/%J.log'
  print >> File, '#BSUB -e '+RunDir+'/%J.err'
  print >> File, '#BSUB -W 0:10'
  print >> File, '#BSUB -n '+str(nprocs)
  if (GPUsPerNode == 1):
    print >> File, '#BSUB -R \"span[ptile=1]\"'
  elif (GPUsPerNode == 2):
    print >> File, '#BSUB -R \"span[ptile=2]\"'
    print >> File, '#BSUB -m emerald3g'
  print >> File, '#BSUB -x'
  print >> File, ''

  print >> File, 'export Jobname=\"Solver_'+str(nprocs)+'GPUs\"'
  print >> File, 'export EXECUTABLE=\"'+str(ExecutableDir)+'/main_solver-CUDAC.x\"'
  print >> File, 'echo \"WORKDIR=${LS_SUBCWD}\"'
  print >> File, 'export MV2_USE_SHARED_MEM=1'
  print >> File, 'export MV2_ENABLE_AFFINITY=1'
  print >> File, 'echo \"Number of processors: '+str(nprocs)+'\"'
  print >> File, 'echo \"Running on hosts: ${LSB_HOSTS}\"'
  print >> File, 'echo \"MV2_USE_SHARED_MEM = ${MV2_USE_SHARED_MEM}\"'
  print >> File, 'echo \"MV2_ENABLE_AFFINITY = ${MV2_ENABLE_AFFINITY}\"'
  print >> File, 'export LD_LIBRARY_PATH=/usr/lib/lib64:${LD_LIBRARY_PATH}'
  print >> File, 'export LD_LIBRARY_PATH=/apps/cuda/5.0.35/cuda/lib64:/apps/cuda/5.0.35/cuda/lib:/opt/vdt/1.8.1/globus/lib:/opt/vdt/1.8.1/glite/lib:/apps/cuda/4.2.9/cuda/lib64/'
  TaskSet = ''
  if (GPUsPerNode == 2):
    print >> File, 'export CUDA_VISIBLE_DEVICES=0,1'
    TaskSet = 'taskset 0x3'
  print >> File, ''
  for solver in Solvers:
    for nx in nxList[solver]:
      inputDir = 'input_'+str(nprocs)+'GPUs/'
      parameterFilename = 'parameters_'+solver+'_'+str(nx)+'x'+str(nx)+'x'+str(nz)+'_'+str(nprocs)+'GPUs.in'
      domainFilename = 'domain_'+str(nx)+'x'+str(nx)+'x'+str(nz)+'_'+str(nprocs)+'GPUs.in'
      print >> File, '################ NX = '+str(nx)+' ##################'
      print >> File, ''
      print >> File, 'cd ${LS_SUBCWD}'
      print >> File, 'export PARAMETERFILE=\"'+parameterFilename+'\"'
      print >> File, 'export DOMAINFILE=\"'+domainFilename+'\"'
      print >> File, 'export WORKDIR=${LS_SUBCWD}/${Jobname}_${LSB_JOBID}'
      print >> File, 'export INPUTDIR=${LS_SUBCWD}/'+inputDir
      print >> File, 'mkdir -p $WORKDIR'
      print >> File, 'export Domainname=\"'+str(nx)+'x'+str(nx)+'x'+str(nz)+'\"'
      print >> File, 'export WORKSUBDIR=${LS_SUBCWD}/${Jobname}_${LSB_JOBID}/${Domainname}_'+str(solver)
      print >> File, 'mkdir ${WORKSUBDIR}'
      print >> File, 'cp $0 $WORKDIR/jobscript.bash'
      print >> File, 'cp ${INPUTDIR}/${PARAMETERFILE} $WORKSUBDIR'
      print >> File, 'cp ${INPUTDIR}/${DOMAINFILE} $WORKSUBDIR'
      print >> File, 'cd $WORKSUBDIR'
      print >> File, 'time (mpirun --hostfile ${LSB_DJOB_HOSTFILE} -np '+str(nprocs)+' '+TaskSet+' $EXECUTABLE $WORKSUBDIR/$PARAMETERFILE $WORKSUBDIR/$DOMAINFILE > Output.dat) 2> Time.txt'
      print >> File, ''
  File.close()

########################################################
# Generate a set of scaling runs
########################################################
def ScalingRuns(nprocsList,RunDir,ExecutableDir):
  hostname = socket.gethostname()
  validHost = False
  m = re.search('gpu.rl.ac.uk',hostname)
  if m:
    host = 'EMERALD'
    validHost = True
  m = re.search('titan',hostname)
  if m:
    host = 'Titan'
    validHost = True
  if (not validHost):
    print 'ERROR: Running script on unknown host \"'+hostname+'\"'
    sys.exit(-1)
  for nprocs in nprocsList:
    # Create parameter files
    for solver in Solvers:
      for nx in nxList[solver]:
        GenerateParameterFiles(RunDir,solver,nx,nprocs)
    # Create submissions scripts
    if (host == 'EMERALD'):
      GenerateJobEMERALD(RunDir,ExecutableDir,nprocs)
    if (host == 'Titan'):
      GenerateJobTitan(RunDir,ExecutableDir,nprocs)


########################################################
# M A I N
########################################################
if (__name__ == '__main__'):

  if (len(sys.argv) != 3):
    print 'Usage: python '+sys.argv[0]+' <rundir> <executabledir>'
    sys.exit(1)

  ## Directory to run in
  RunDir=sys.argv[1]

  ## Directory containing the executable
  ExecutableDir=sys.argv[2]

  ScalingRuns(nprocsList,RunDir,ExecutableDir)
