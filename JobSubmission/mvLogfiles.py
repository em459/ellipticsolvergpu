### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the EllipticSolverGPU code.
#  
#  (c) The copyright relating to this work is owned jointly 
#  by the Crown, Met Office and NERC [2014]. However, it
#  has been created with the help of the GungHo Consortium,
#  whose members are identified at
#  https://puma.nerc.ac.uk/trac/GungHo/wiki
#  
#  Main Developer: Eike Mueller, University of Bath
#  
#  Contributors:
#    Eero Vainikko (University of Tartu)
#    Sinan Shi (EPCC Edinburgh)
#  
#  EllipticSolverGPU is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  EllipticSolverGPU is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with EllipticSolverGPU (see files COPYING and COPYING.LESSER in the 
#  main directory).  If not, see <http://www.gnu.org/licenses/>.
#  
#  The EllipticSolverGPU code uses (i.e. links to and includes header files
#  from) the Generic Communication Library (GCL). The GCL has been developed
#  by Mauro Bianco and Ugo Varetto at the SWISS NATIONAL SUPERCOMPUTING
#  CENTRE (CSCS) and it is distributed under the Copyright statement in the
#  file COPYING.GCL.
#
### COPYRIGHT AND LICENSE STATEMENT ###



import sys
import os
import re
import shutil
import socket

verbose = False

##############################################################
##############################################################
def Main(Directory):
  hostname = socket.gethostname()
  validHost = False
  m = re.search('titan',hostname)
  if m:
    print 'Host = Titan'
    pattern = '.*\.[o|e]([0-9]+)'
    validHost = True
  m = re.search('gpu.rl.ac.uk',hostname)
  if m:
    print 'Host = EMERALD'
    pattern = '([0-9]+)\.[log|err]'
    validHost = True
  if (not validHost):
    print 'ERROR: Running on unknown host'
    sys.exit(-1)
  # Find all log files
  LogFiles = {}
  dirList = os.listdir(Directory)
  for Item in dirList:
    if os.path.isfile(Item):
      m = re.search(pattern,Item)
      if m:
        jobId = m.group(1)
        for Item2 in dirList:
          if os.path.isdir(Item2):
            m2 = re.search(jobId+'$',Item2)
            if m2:
              if verbose:
                print str(Item)+' -> '+str(Item2)
              shutil.copyfile(Item,Item2+'/'+Item)
              os.remove(Item)

##############################################################
# M A I N
##############################################################
if (__name__ == '__main__'):
  Directory = os.getcwd()
  Main(Directory)
